<?php

class M_NextGen_Pro_Lightbox extends C_Base_Module
{
    public $object;
    function define($id = 'pope-module', $name = 'Pope Module', $description = '', $version = '', $uri = '', $author = '', $author_uri = '', $context = false)
    {
        parent::define('photocrati-nextgen_pro_lightbox', 'NextGEN Pro Lightbox', 'Provides a lightbox with integrated commenting, social sharing, and e-commerce functionality', NGG_PRO_LIGHTBOX_VERSION, 'https://www.imagely.com/wordpress-gallery-plugin/nextgen-pro/', 'Imagely', 'https://www.imagely.com', $context);
    }
    function _register_adapters()
    {
        if (\Imagely\NGG\IGW\ATPManager::is_atp_url() || is_admin()) {
            // Add additional settings to each supported display type
            $registry = C_Component_Registry::get_instance();
            $registry->add_adapter('I_Form', 'A_NextGen_Pro_Lightbox_Triggers_Form', NGG_BASIC_THUMBNAILS);
            $registry->add_adapter('I_Form', 'A_NextGen_Pro_Lightbox_Triggers_Form', NGG_BASIC_SLIDESHOW);
            $registry->add_adapter('I_Form', 'A_NextGen_Pro_Lightbox_Triggers_Form', NGG_BASIC_IMAGEBROWSER);
            $registry->add_adapter('I_Form', 'A_NextGen_Pro_Lightbox_Triggers_Form', NGG_BASIC_SINGLEPIC);
            $registry->add_adapter('I_Form', 'A_NextGen_Pro_Lightbox_Triggers_Form', NGG_PRO_SLIDESHOW);
            $registry->add_adapter('I_Form', 'A_NextGen_Pro_Lightbox_Triggers_Form', NGG_PRO_HORIZONTAL_FILMSTRIP);
            $registry->add_adapter('I_Form', 'A_NextGen_Pro_Lightbox_Triggers_Form', NGG_PRO_THUMBNAIL_GRID);
            $registry->add_adapter('I_Form', 'A_NextGen_Pro_Lightbox_Triggers_Form', NGG_PRO_BLOG_GALLERY);
            $registry->add_adapter('I_Form', 'A_NextGen_Pro_Lightbox_Triggers_Form', NGG_PRO_FILM);
            $registry->add_adapter('I_Form', 'A_NextGen_Pro_Lightbox_Triggers_Form', NGG_PRO_MASONRY);
            $registry->add_adapter('I_Form', 'A_NextGen_Pro_Lightbox_Album_Form', NGG_PRO_GRID_ALBUM);
            $registry->add_adapter('I_Form', 'A_NextGen_Pro_Lightbox_Album_Form', NGG_PRO_LIST_ALBUM);
            $registry->add_adapter('I_Form', 'A_NextGen_Pro_Lightbox_Album_Form', NGG_BASIC_COMPACT_ALBUM);
            $registry->add_adapter('I_Form', 'A_NextGen_Pro_Lightbox_Album_Form', NGG_BASIC_EXTENDED_ALBUM);
            $registry->add_adapter('I_Form', 'A_NextGen_Pro_Lightbox_Form', NGG_PRO_LIGHTBOX);
        }
    }
    function _register_hooks()
    {
        add_action('admin_init', function () {
            $forms = \Imagely\NGG\Admin\FormManager::get_instance();
            $forms->add_form(NGG_LIGHTBOX_OPTIONS_SLUG, NGG_PRO_LIGHTBOX);
        });
    }
    function get_type_list()
    {
        return ['A_NextGen_Pro_Lightbox_Form' => 'adapter.nextgen_pro_lightbox_form.php', 'A_NextGen_Pro_Lightbox_Triggers_Form' => 'adapter.nextgen_pro_lightbox_triggers_form.php', 'A_NextGen_Pro_Lightbox_Album_Form' => 'adapter.nextgen_pro_lightbox_album_form.php'];
    }
}
new M_NextGen_Pro_Lightbox();