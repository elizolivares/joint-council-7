jQuery(function($) {
    $('input[name="image_animation[animate_images_enable]"]')
        .nextgen_radio_toggle_tr('1', $('#tr_image_animation_animate_images_style'))
        .nextgen_radio_toggle_tr('1', $('#tr_image_animation_animate_images_duration'))
        .nextgen_radio_toggle_tr('1', $('#tr_image_animation_animate_images_delay'));

    $('input[name="image_animation[animate_pagination_enable]"]')
        .nextgen_radio_toggle_tr('1', $('#tr_image_animation_animate_pagination_style'))
        .nextgen_radio_toggle_tr('1', $('#tr_image_animation_animate_pagination_duration'))
        .nextgen_radio_toggle_tr('1', $('#tr_image_animation_animate_pagination_delay'));

    // TODO: remove this when the minimum supported version of NextGEN is 3.58 or higher.
    const parent = document.querySelector('div.ngg_page_content_menu');
    const animationAnchor = document.querySelector('a[data-id=image_animation]');
    const watermarkAnchor = document.querySelector('a[data-id=watermarks]');
    if (animationAnchor && watermarkAnchor) {
        parent.insertBefore(animationAnchor, watermarkAnchor);
    }
});