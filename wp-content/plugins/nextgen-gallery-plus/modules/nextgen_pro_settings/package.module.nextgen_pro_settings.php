<?php
/**
 * Provides form fields for animation settings.
 *
 * @package NextGEN Pro
 * @remove-for-nextgen-starter
 */
/**
 * Adds form fields and processes animation settings.
 *
 * @property \C_Form $object
 */
class A_Animation_Form extends Mixin
{
    /**
     * Returns an instance of C_Settings_Model. Necessary for the POPE form structure.
     *
     * @return C_Settings_Model
     */
    public function get_model()
    {
        return C_Settings_Model::get_instance();
    }
    /**
     * Returns the title of this form.
     *
     * @return string
     */
    public function get_title()
    {
        return __('Image Animations', 'nextgen-gallery-pro');
    }
    /**
     * Enqueues javascript resources used by this form.
     *
     * @return void
     */
    public function enqueue_static_resources()
    {
        wp_enqueue_script('nextgen_pro_animations_settings_style', $this->object->get_static_url('photocrati-nextgen_pro_settings#animation.js'), [], \Imagely\NGGPro\Bootloader::$plugin_version);
    }
    /**
     * Returns a list of fields to render. Matches $this->method_name_field($settings).
     *
     * @return string[]
     */
    public function _get_field_names()
    {
        return ['nextgen_pro_image_animation_enable', 'nextgen_pro_image_animation_style', 'nextgen_pro_image_animation_duration', 'nextgen_pro_image_animation_delay', 'nextgen_pro_pagination_animation_enable', 'nextgen_pro_pagination_animation_style', 'nextgen_pro_pagination_animation_duration', 'nextgen_pro_pagination_animation_delay'];
    }
    /**
     * Returns the rendered HTML of the 'enable image animations' field.
     *
     * @param array $settings Existing setting value.
     * @return string
     */
    public function _render_nextgen_pro_image_animation_enable_field($settings)
    {
        $model = new \stdClass();
        $model->name = 'image_animation';
        return $this->object->_render_radio_field($model, 'animate_images_enable', __('Animate images', 'nextgen-gallery-pro'), \Imagely\NGG\Settings\Settings::get_instance()->get('animate_images_enable'));
    }
    public function _render_nextgen_pro_image_animation_style_field($settings)
    {
        $model = new \stdClass();
        $model->name = 'image_animation';
        return $this->object->_render_select_field($model, 'animate_images_style', __('Animation style', 'nextgen-gallery-pro'), \Imagely\NGGPro\Display\Animations\Manager::get_styles(), \Imagely\NGG\Settings\Settings::get_instance()->get('animate_images_style'), '', (bool) (!\Imagely\NGG\Settings\Settings::get_instance()->get('animate_images_enable')));
    }
    public function _render_nextgen_pro_image_animation_duration_field($settings)
    {
        $model = new \stdClass();
        $model->name = 'image_animation';
        return $this->object->_render_number_field($model, 'animate_images_duration', __('Animation duration', 'nextgen-gallery-pro'), (int) \Imagely\NGG\Settings\Settings::get_instance()->get('animate_images_duration'), __('Measured in milliseconds', 'nextgen-gallery-pro'), (bool) (!\Imagely\NGG\Settings\Settings::get_instance()->get('animate_images_enable')), '', 0);
    }
    public function _render_nextgen_pro_image_animation_delay_field($settings)
    {
        $model = new \stdClass();
        $model->name = 'image_animation';
        return $this->object->_render_number_field($model, 'animate_images_delay', __('Animation delay', 'nextgen-gallery-pro'), (int) \Imagely\NGG\Settings\Settings::get_instance()->get('animate_images_delay'), __('Measured in milliseconds', 'nextgen-gallery-pro'), (bool) (!\Imagely\NGG\Settings\Settings::get_instance()->get('animate_images_enable')), '', 0);
    }
    /**
     * Returns the rendered HTML of the 'enable pagination animations' field.
     *
     * @param array $settings Existing setting value.
     * @return string
     */
    public function _render_nextgen_pro_pagination_animation_enable_field($settings)
    {
        $model = new \stdClass();
        $model->name = 'image_animation';
        return $this->object->_render_radio_field($model, 'animate_pagination_enable', __('Animate paginations', 'nextgen-gallery-pro'), \Imagely\NGG\Settings\Settings::get_instance()->get('animate_pagination_enable'));
    }
    public function _render_nextgen_pro_pagination_animation_style_field($settings)
    {
        $model = new \stdClass();
        $model->name = 'image_animation';
        return $this->object->_render_select_field($model, 'animate_pagination_style', __('Animation style', 'nextgen-gallery-pro'), \Imagely\NGGPro\Display\Animations\Manager::get_styles(), \Imagely\NGG\Settings\Settings::get_instance()->get('animate_pagination_style'), '', (bool) (!\Imagely\NGG\Settings\Settings::get_instance()->get('animate_pagination_enable')));
    }
    public function _render_nextgen_pro_pagination_animation_duration_field($settings)
    {
        $model = new \stdClass();
        $model->name = 'image_animation';
        return $this->object->_render_number_field($model, 'animate_pagination_duration', __('Animation duration', 'nextgen-gallery-pro'), \Imagely\NGG\Settings\Settings::get_instance()->get('animate_pagination_duration'), __('Measured in milliseconds', 'nextgen-gallery-pro'), (bool) (!\Imagely\NGG\Settings\Settings::get_instance()->get('animate_pagination_enable')), '', 0);
    }
    public function _render_nextgen_pro_pagination_animation_delay_field($settings)
    {
        $model = new \stdClass();
        $model->name = 'image_animation';
        return $this->object->_render_number_field($model, 'animate_pagination_delay', __('Animation delay', 'nextgen-gallery-pro'), \Imagely\NGG\Settings\Settings::get_instance()->get('animate_pagination_delay'), __('Measured in milliseconds', 'nextgen-gallery-pro'), (bool) (!\Imagely\NGG\Settings\Settings::get_instance()->get('animate_pagination_enable')), '', 0);
    }
    /**
     * Process the submitted values and persist them to the database.
     *
     * @param array $options Form submission data.
     * @return void
     */
    public function save_action($options)
    {
        if (!empty($options)) {
            $settings = \Imagely\NGG\Settings\Settings::get_instance();
            // Perform some sanity checking: only allow certain values for these settings.
            $styles = \Imagely\NGGPro\Display\Animations\Manager::get_styles();
            // First handle the first-image-of-the-gallery options.
            if (in_array($options['animate_images_enable'], ['1', '0'], true)) {
                $settings->set('animate_images_enable', $options['animate_images_enable']);
            }
            if (array_key_exists($options['animate_images_style'], $styles)) {
                $settings->set('animate_images_style', $options['animate_images_style']);
            }
            $settings->set('animate_images_duration', (int) $options['animate_images_duration']);
            $settings->set('animate_images_delay', (int) $options['animate_images_delay']);
            // And then handle the pagination options.
            if (in_array($options['animate_pagination_enable'], ['1', '0'], true)) {
                $settings->set('animate_pagination_enable', $options['animate_pagination_enable']);
            }
            if (array_key_exists($options['animate_pagination_style'], $styles)) {
                $settings->set('animate_pagination_style', $options['animate_pagination_style']);
            }
            $settings->set('animate_pagination_duration', (int) $options['animate_pagination_duration']);
            $settings->set('animate_pagination_delay', (int) $options['animate_pagination_delay']);
            $settings->save();
        }
    }
}
/**
 * @remove-for-nextgen-starter
 */
class A_Image_Protection_Form extends Mixin
{
    function get_model()
    {
        return C_Settings_Model::get_instance();
    }
    function get_title()
    {
        return __('Image Protection', 'nextgen-gallery-pro');
    }
    function _get_field_names()
    {
        return ['nextgen_pro_image_protection_enable', 'nextgen_pro_image_protection_global'];
    }
    function enqueue_static_resources()
    {
        wp_enqueue_style('nextgen_pro_image_protection_admin_settings_style', $this->object->get_static_url('photocrati-nextgen_pro_settings#image_protection.css'));
        wp_enqueue_script('nextgen_pro_image_protection_admin_settings_script', $this->get_static_url('photocrati-nextgen_pro_settings#image_protection.js'), ['jquery.nextgen_radio_toggle']);
    }
    function _render_nextgen_pro_image_protection_enable_field($settings)
    {
        $model = new stdClass();
        $model->name = 'image_protection';
        $field = $this->object->_render_radio_field($model, 'protect_images', __('Protect images', 'nextgen-gallery-pro'), \Imagely\NGG\Settings\Settings::get_instance()->get('protect_images'), __('Protect images from being downloaded both by right click or drag &amp; drop', 'nextgen-gallery-pro'));
        return $field;
    }
    function _render_nextgen_pro_image_protection_global_field($settings)
    {
        $model = new stdClass();
        $model->name = 'image_protection';
        $field = $this->object->_render_radio_field($model, 'protect_images_globally', __('Disable right click menu completely', 'nextgen-gallery-pro'), \Imagely\NGG\Settings\Settings::get_instance()->get('protect_images_globally'), __('By default the right click menu is only disabled for NextGEN images. Enable this to disable the right click menu on the whole page.', 'nextgen-gallery-pro'), empty(\Imagely\NGG\Settings\Settings::get_instance()->get('protect_images')));
        return $field;
    }
    function save_action($options)
    {
        if (!empty($options)) {
            $settings = \Imagely\NGG\Settings\Settings::get_instance();
            $settings->set('protect_images', $options['protect_images']);
            $settings->set('protect_images_globally', $options['protect_images_globally']);
            $settings->save();
        }
    }
}
/**
 * @property C_Form $object
 * @mixin C_Form
 */
class A_Licensing_API_Key_Form extends Mixin
{
    function get_model()
    {
        return C_Settings_Model::get_instance();
    }
    function get_title()
    {
        $current = \Imagely\NGGPro\License\Manager::get_current_product();
        switch ($current) {
            case 'photocrati-nextgen-pro':
                return __('Pro license key', 'nextgen-gallery-pro');
            case 'photocrati-nextgen-plus':
                return __('Plus license key', 'nextgen-gallery-pro');
            case 'photocrati-nextgen-starter':
                return __('Starter license key', 'nextgen-gallery-pro');
        }
    }
    function _get_field_names()
    {
        return ['nextgen_pro_license_key'];
    }
    function enqueue_static_resources()
    {
        wp_enqueue_style('nextgen_pro_api_key_form', $this->object->get_static_url('photocrati-nextgen_pro_settings#license.css'));
    }
    function _render_nextgen_pro_license_key_field($settings)
    {
        $licensing = new \Imagely\NGGPro\License\Manager();
        $model = new stdClass();
        $model->name = 'imagely_license_key';
        $field = $this->object->_render_text_field($model, 'imagely_license_key', __('Licensing key', 'nextgen-gallery-pro'), $licensing->get_license($licensing::get_current_product()), __('Used to validate your permissions to download future updates and ecommerce functions.', 'nextgen-gallery-pro'));
        return $field;
    }
    function save_action($options)
    {
        if (!empty($options)) {
            $settings = \Imagely\NGG\Settings\Settings::get_instance();
            $licensing = new \Imagely\NGGPro\License\Manager();
            $current_product = $licensing::get_current_product();
            $license = $licensing->get_license($current_product);
            // The actual setting is kept in a separate WP option; the entry in $settings is just used to determine if
            // we need to call set_license() and clear the update check transient.
            if ($options['imagely_license_key'] !== $license) {
                // Set the license as both for this product and as the 'default' license
                $licensing->set_license($options['imagely_license_key'], null);
                $licensing->set_license($options['imagely_license_key'], $current_product);
                delete_transient('nextgen_gallery_pro_license_check');
                $licensing::is_valid_license();
                $settings->set('imagely_license_key', $options['imagely_license_key']);
                $settings->save();
            }
        }
    }
}