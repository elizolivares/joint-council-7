<?php

class M_NextGen_Pro_Settings extends C_Base_Module
{
    public $object;
    function define($id = 'pope-module', $name = 'Pope Module', $description = '', $version = '', $uri = '', $author = '', $author_uri = '', $context = false)
    {
        parent::define('photocrati-nextgen_pro_settings', 'Pro Options', 'Provides additional settings management features', '3.0.19', 'https://www.imagely.com/wordpress-gallery-plugin/nextgen-pro/', 'Imagely', 'https://www.imagely.com');
    }
    function _register_hooks()
    {
        add_action('admin_init', [$this, 'register_forms']);
    }
    function _register_adapters()
    {
        /**
         * @remove-for-nextgen-starter
         */
        if ('starter' !== \Imagely\NGGPro\Bootloader::$plugin_id && is_admin()) {
            $registry = C_Component_Registry::get_instance();
            $registry->add_adapter('I_Form', 'A_Image_Protection_Form', 'image_protection');
            $registry->add_adapter('I_Form', 'A_Animation_Form', 'image_animation');
        }
    }
    function register_forms()
    {
        $forms = \Imagely\NGG\Admin\FormManager::get_instance();
        /** @remove-for-nextgen-starter */
        if ('starter' !== \Imagely\NGGPro\Bootloader::$plugin_id) {
            $forms->add_form(NGG_OTHER_OPTIONS_SLUG, 'image_protection');
            $forms->add_form(NGG_OTHER_OPTIONS_SLUG, 'image_animation');
            // This method was not added to NextGEN until after 3.57.
            if (method_exists($forms, 'move_form_to_follow_other_form')) {
                $forms->move_form_to_follow_other_form(NGG_OTHER_OPTIONS_SLUG, 'image_animation', 'lightbox_effects');
            }
        }
        $display_license_form = apply_filters('ngg_pro_display_license_form', true);
        if ($display_license_form && (!is_multisite() || is_super_admin())) {
            $registry = C_Component_Registry::get_instance();
            $registry->add_adapter('I_Form', 'A_Licensing_API_Key_Form', 'imagely_license_key');
            $forms->add_form(NGG_OTHER_OPTIONS_SLUG, 'imagely_license_key');
        }
    }
    function get_type_list()
    {
        return ['A_Animation_Form' => 'adapter.animations.php', 'A_Image_Protection_Form' => 'adapter.image_protection.php', 'A_Licensing_API_Key_Form' => 'adapter.license.php', 'A_NextGen_Pro_Settings_Reset_Form' => 'adapter.settings_reset.php'];
    }
}
new M_NextGen_Pro_Settings();