<?php

class P_Photocrati_NextGen_Pro extends C_Base_Product
{
    public $object;
    /**
     * @remove-for-nextgen-pro
     * @remove-for-nextgen-starter
     * @return string[]
     */
    public function get_plus_modules() : array
    {
        return ['imagely-pro_displaytype_admin', 'photocrati-nextgen_pro_lightbox', 'photocrati-nextgen_pro_marketing', 'photocrati-nextgen_pro_settings'];
    }
    public function get_modules() : array
    {
        $retval = [];
        if ('pro' === \Imagely\NGGPro\Bootloader::$plugin_id) {
            $retval = $this->get_pro_modules();
        } elseif ('plus' === \Imagely\NGGPro\Bootloader::$plugin_id) {
            $retval = $this->get_plus_modules();
        } elseif ('starter' === \Imagely\NGGPro\Bootloader::$plugin_id) {
            $retval = $this->get_starter_modules();
        }
        return apply_filters('ngg_pro_get_modules_to_load', $retval);
    }
    function define($id = 'pope-product', $name = 'Pope Product', $description = '', $version = '', $uri = '', $author = '', $author_uri = '', $context = false)
    {
        parent::define('photocrati-nextgen-pro', 'NextGEN Pro', 'NextGEN Pro', \Imagely\NGGPro\Bootloader::$plugin_version, 'https://www.nextgen-gallery.com', 'Imagely', 'https://www.imagely.com');
        $this->get_registry()->set_product_module_path($this->module_id, __DIR__);
    }
    function load()
    {
        $registry = $this->get_registry();
        foreach ($this->get_modules() as $module_name) {
            $registry->load_module($module_name);
        }
        parent::load();
    }
}
new P_Photocrati_NextGen_Pro();