jQuery(function($) {

    window.nextgen_pro_animations_display_types_images.forEach(function(display_type) {
        $(`input[name="${display_type}[animate_images_enable]"`)
            .nextgen_radio_toggle_tr('1', $(`#tr_${display_type}_animate_images_style`))
            .nextgen_radio_toggle_tr('1', $(`#tr_${display_type}_animate_images_duration`))
            .nextgen_radio_toggle_tr('1', $(`#tr_${display_type}_animate_images_delay`));
    });

    window.nextgen_pro_animations_display_types_pagination.forEach(function(display_type) {
        $(`input[name="${display_type}[animate_pagination_enable]"`)
            .nextgen_radio_toggle_tr('1', $(`#tr_${display_type}_animate_pagination_style`))
            .nextgen_radio_toggle_tr('1', $(`#tr_${display_type}_animate_pagination_duration`))
            .nextgen_radio_toggle_tr('1', $(`#tr_${display_type}_animate_pagination_delay`));
    });
});