<?php
/**
 * @remove-for-nextgen-starter
 */
class A_NextGen_Pro_Album_Form extends Mixin_Display_Type_Form
{
    /**
     * Enqueues static resources required by this form
     */
    function enqueue_static_resources()
    {
        $this->call_parent('enqueue_static_resources');
        wp_enqueue_script('nextgen_pro_albums_settings_script', $this->object->get_static_url('imagely-pro_displaytype_admin#album_settings.js'), ['jquery.nextgen_radio_toggle']);
    }
    /**
     * Returns a list of fields to render on the settings page
     */
    function _get_field_names()
    {
        return ['thumbnail_override_settings', 'nextgen_pro_albums_display_type', 'nextgen_pro_albums_enable_breadcrumbs', 'nextgen_pro_albums_caption_color', 'nextgen_pro_albums_caption_size', 'nextgen_pro_albums_border_color', 'nextgen_pro_albums_border_size', 'nextgen_pro_albums_background_color', 'nextgen_pro_albums_padding', 'nextgen_pro_albums_spacing', 'nextgen_pro_albums_child_descriptions', 'display_type_view'];
    }
    function _render_nextgen_pro_albums_child_descriptions_field($display_type)
    {
        return $this->_render_radio_field($display_type, 'enable_descriptions', __('Display album and gallery descriptions', 'nextgen-gallery-pro'), $display_type->settings['enable_descriptions']);
    }
    /*
     * Let users choose which display type galleries inside albums use
     */
    function _render_nextgen_pro_albums_display_type_field($display_type)
    {
        $mapper = \Imagely\NGGPro\DataMappers\DisplayType::get_instance();
        $types = [];
        foreach ($mapper->find_by_entity_type('image') as $dt) {
            $types[$dt->name] = $dt->title;
        }
        return $this->_render_select_field($display_type, 'gallery_display_type', __('Display galleries as', 'nextgen-gallery-pro'), $types, $display_type->settings['gallery_display_type'], __('How would you like galleries to be displayed?', 'nextgen-gallery-pro'));
    }
    function _render_nextgen_pro_albums_enable_breadcrumbs_field($display_type)
    {
        return $this->_render_radio_field($display_type, 'enable_breadcrumbs', __('Enable breadcrumbs', 'nextgen-gallery-pro'), isset($display_type->settings['enable_breadcrumbs']) ? $display_type->settings['enable_breadcrumbs'] : false);
    }
    function _render_nextgen_pro_albums_caption_color_field($display_type)
    {
        return $this->_render_color_field($display_type, 'caption_color', __('Caption color', 'nextgen-gallery-pro'), $display_type->settings['caption_color']);
    }
    function _render_nextgen_pro_albums_caption_size_field($display_type)
    {
        return $this->_render_number_field($display_type, 'caption_size', __('Caption size', 'nextgen-gallery-pro'), $display_type->settings['caption_size'], '', false, '', 0);
    }
    function _render_nextgen_pro_albums_border_color_field($display_type)
    {
        return $this->_render_color_field($display_type, 'border_color', __('Border color', 'nextgen-gallery-pro'), $display_type->settings['border_color']);
    }
    function _render_nextgen_pro_albums_border_size_field($display_type)
    {
        return $this->_render_number_field($display_type, 'border_size', __('Border size', 'nextgen-gallery-pro'), $display_type->settings['border_size'], '', false, '', 0);
    }
    function _render_nextgen_pro_albums_background_color_field($display_type)
    {
        return $this->_render_color_field($display_type, 'background_color', __('Background color', 'nextgen-gallery-pro'), $display_type->settings['background_color']);
    }
    function _render_nextgen_pro_albums_padding_field($display_type)
    {
        return $this->_render_number_field($display_type, 'padding', __('Padding', 'nextgen-gallery-pro'), $display_type->settings['padding'], '', false, '', 0);
    }
    function _render_nextgen_pro_albums_spacing_field($display_type)
    {
        return $this->_render_number_field($display_type, 'spacing', __('Spacing', 'nextgen-gallery-pro'), $display_type->settings['spacing'], '', false, '', 0);
    }
}
/**
 * Adds animations related settings to supporting display types.
 *
 * @package NextGEN Pro
 */
/**
 * Remove this class when building Plus and Starter.
 *
 * @remove-for-nextgen-plus
 * @remove-for-nextgen-starter
 * @property C_Form $object
 */
class A_NextGen_Pro_Animations_Form extends Mixin
{
    function _get_field_names()
    {
        $fields = $this->call_parent('_get_field_names');
        $fields[] = 'nextgen_pro_image_animation_enable';
        $fields[] = 'nextgen_pro_image_animation_style';
        $fields[] = 'nextgen_pro_image_animation_duration';
        $fields[] = 'nextgen_pro_image_animation_delay';
        $fields[] = 'nextgen_pro_pagination_animation_enable';
        $fields[] = 'nextgen_pro_pagination_animation_style';
        $fields[] = 'nextgen_pro_pagination_animation_duration';
        $fields[] = 'nextgen_pro_pagination_animation_delay';
        return $fields;
    }
    /**
     * Enqueues javascript resources used by this form.
     *
     * @return void
     */
    public function enqueue_static_resources()
    {
        $this->call_parent('enqueue_static_resources');
        wp_enqueue_script('nextgen_pro_animations_settings_style', $this->object->get_static_url('imagely-pro_displaytype_admin#animation.js'), ['jquery.nextgen_radio_toggle'], \Imagely\NGGPro\Bootloader::$plugin_version);
        wp_localize_script('nextgen_pro_animations_settings_style', 'nextgen_pro_animations_display_types_images', \Imagely\NGGPro\Display\Animations\Manager::get_supported_image_types());
        wp_localize_script('nextgen_pro_animations_settings_style', 'nextgen_pro_animations_display_types_pagination', \Imagely\NGGPro\Display\Animations\Manager::get_supported_pagination_display_types());
    }
    /**
     * Returns the rendered HTML of the 'enable image animations' field.
     *
     * @param \Imagely\NGG\DataTypes\DisplayType $display_type The display type this setting applies to.
     * @return string
     */
    public function _render_nextgen_pro_image_animation_enable_field($display_type)
    {
        return $this->object->_render_radio_field($display_type, 'animate_images_enable', __('Animate images', 'nextgen-gallery-pro'), $display_type->settings['animate_images_enable'] ?? false);
    }
    /**
     * Returns the rendered HTML of the 'animation style' field.
     *
     * @param \Imagely\NGG\DataTypes\DisplayType $display_type The display type this setting applies to.
     * @return string
     */
    public function _render_nextgen_pro_image_animation_style_field($display_type)
    {
        return $this->object->_render_select_field($display_type, 'animate_images_style', __('Animation style', 'nextgen-gallery-pro'), \Imagely\NGGPro\Display\Animations\Manager::get_styles(), $display_type->settings['animate_images_style'] ?? 'wobble', '', !($display_type->settings['animate_images_enable'] ?? false));
    }
    /**
     * Returns the rendered HTML of the 'animation duration' field.
     *
     * @param \Imagely\NGG\DataTypes\DisplayType $display_type The display type this setting applies to.
     * @return string
     */
    public function _render_nextgen_pro_image_animation_duration_field($display_type)
    {
        return $this->object->_render_number_field($display_type, 'animate_images_duration', __('Animation duration', 'nextgen-gallery-pro'), $display_type->settings['animate_images_duration'] ?? 1500, __('Measured in milliseconds', 'nextgen-gallery-pro'), !($display_type->settings['animate_images_enable'] ?? false), '', 0);
    }
    /**
     * Returns the rendered HTML of the 'animation delay' field.
     *
     * @param \Imagely\NGG\DataTypes\DisplayType $display_type The display type this setting applies to.
     * @return string
     */
    public function _render_nextgen_pro_image_animation_delay_field($display_type)
    {
        return $this->object->_render_number_field($display_type, 'animate_images_delay', __('Animation delay', 'nextgen-gallery-pro'), $display_type->settings['animate_images_delay'] ?? 250, __('Measured in milliseconds', 'nextgen-gallery-pro'), !($display_type->settings['animate_images_enable'] ?? false), '', 0);
    }
    /**
     * Returns the rendered HTML of the 'enable pagination animations' field.
     *
     * @param \Imagely\NGG\DataTypes\DisplayType $display_type The display type this setting applies to.
     * @return string
     */
    public function _render_nextgen_pro_pagination_animation_enable_field($display_type)
    {
        if (!in_array($display_type->name, \Imagely\NGGPro\Display\Animations\Manager::get_supported_pagination_display_types(), true)) {
            return '';
        }
        return $this->object->_render_radio_field($display_type, 'animate_pagination_enable', __('Animate pagination', 'nextgen-gallery-pro'), $display_type->settings['animate_pagination_enable'] ?? false);
    }
    /**
     * Returns the rendered HTML of the 'animation style' field.
     *
     * @param \Imagely\NGG\DataTypes\DisplayType $display_type The display type this setting applies to.
     * @return string
     */
    public function _render_nextgen_pro_pagination_animation_style_field($display_type)
    {
        if (!in_array($display_type->name, \Imagely\NGGPro\Display\Animations\Manager::get_supported_pagination_display_types(), true)) {
            return '';
        }
        return $this->object->_render_select_field($display_type, 'animate_pagination_style', __('Animation style', 'nextgen-gallery-pro'), \Imagely\NGGPro\Display\Animations\Manager::get_styles(), $display_type->settings['animate_pagination_style'] ?? 'flipInX', '', !($display_type->settings['animate_pagination_enable'] ?? false));
    }
    /**
     * Returns the rendered HTML of the 'animation duration' field.
     *
     * @param \Imagely\NGG\DataTypes\DisplayType $display_type The display type this setting applies to.
     * @return string
     */
    public function _render_nextgen_pro_pagination_animation_duration_field($display_type)
    {
        if (!in_array($display_type->name, \Imagely\NGGPro\Display\Animations\Manager::get_supported_pagination_display_types(), true)) {
            return '';
        }
        return $this->object->_render_number_field($display_type, 'animate_pagination_duration', __('Animation duration', 'nextgen-gallery-pro'), $display_type->settings['animate_pagination_duration'] ?? 1500, __('Measured in milliseconds', 'nextgen-gallery-pro'), !($display_type->settings['animate_pagination_enable'] ?? false), '', 0);
    }
    /**
     * Returns the rendered HTML of the 'animation delay' field.
     *
     * @param \Imagely\NGG\DataTypes\DisplayType $display_type The display type this setting applies to.
     * @return string
     */
    public function _render_nextgen_pro_pagination_animation_delay_field($display_type)
    {
        if (!in_array($display_type->name, \Imagely\NGGPro\Display\Animations\Manager::get_supported_pagination_display_types(), true)) {
            return '';
        }
        return $this->object->_render_number_field($display_type, 'animate_pagination_delay', __('Animation delay', 'nextgen-gallery-pro'), $display_type->settings['animate_pagination_delay'] ?? 250, __('Measured in milliseconds', 'nextgen-gallery-pro'), !($display_type->settings['animate_pagination_enable'] ?? false), '', 0);
    }
}
/**
 * @remove-for-nextgen-starter
 */
class A_NextGen_Pro_Blog_Form extends Mixin_Display_Type_Form
{
    function get_display_type_name()
    {
        return NGG_PRO_BLOG_GALLERY;
    }
    function enqueue_static_resources()
    {
        $this->call_parent('enqueue_static_resources');
        wp_enqueue_script($this->object->get_display_type_name() . '-js', $this->get_static_url('imagely-pro_displaytype_admin#blog_gallery_settings.js'), ['jquery.nextgen_radio_toggle']);
    }
    /**
     * Returns a list of fields to render on the settings page
     */
    function _get_field_names()
    {
        return ['image_override_settings', 'nextgen_pro_blog_gallery_image_display_size', 'nextgen_pro_blog_gallery_image_max_height', 'nextgen_pro_blog_gallery_spacing', 'nextgen_pro_blog_gallery_border_size', 'nextgen_pro_blog_gallery_border_color', 'nextgen_pro_blog_gallery_display_captions', 'nextgen_pro_blog_gallery_caption_location', 'display_type_view'];
    }
    function _render_nextgen_pro_blog_gallery_border_size_field($display_type)
    {
        return $this->_render_number_field($display_type, 'border_size', __('Border size', 'nextgen-gallery-pro'), $display_type->settings['border_size'], '', false, '', 0);
    }
    function _render_nextgen_pro_blog_gallery_border_color_field($display_type)
    {
        return $this->_render_color_field($display_type, 'border_color', __('Border color', 'nextgen-gallery-pro'), $display_type->settings['border_color']);
    }
    function _render_nextgen_pro_blog_gallery_image_display_size_field($display_type)
    {
        return $this->_render_number_field($display_type, 'image_display_size', __('Image display size', 'nextgen-gallery-pro'), $display_type->settings['image_display_size'], __('Measured in pixels', 'nextgen-gallery-pro'), false, __('image width', 'nextgen-gallery-pro'), 0);
    }
    function _render_nextgen_pro_blog_gallery_image_max_height_field($display_type)
    {
        return $this->_render_number_field($display_type, 'image_max_height', __('Image maximum height', 'nextgen-gallery-pro'), $display_type->settings['image_max_height'], __('Measured in pixels. Empty or 0 will not impose a limit.', 'nextgen-gallery-pro'), false, '', 0);
    }
    function _render_nextgen_pro_blog_gallery_spacing_field($display_type)
    {
        return $this->_render_number_field($display_type, 'spacing', __('Image spacing', 'nextgen-gallery-pro'), $display_type->settings['spacing'], __('Measured in pixels', 'nextgen-gallery-pro'), false, '', 0);
    }
    function _render_nextgen_pro_blog_gallery_display_captions_field($display_type)
    {
        return $this->_render_radio_field($display_type, 'display_captions', __('Display captions', 'nextgen-gallery-pro'), $display_type->settings['display_captions']);
    }
    function _render_nextgen_pro_blog_gallery_caption_location_field($display_type)
    {
        return $this->_render_select_field($display_type, 'caption_location', __('Caption location', 'nextgen-gallery-pro'), ['above' => __('Above', 'nextgen-gallery-pro'), 'below' => __('Below', 'nextgen-gallery-pro')], $display_type->settings['caption_location'], '', !empty($display_type->settings['display_captions']) ? false : true);
    }
}
/**
 * @mixin C_Form
 * @property C_Form|Mixin_Display_Type_Form $object
 */
class A_NextGen_Pro_Film_Form extends Mixin_Display_Type_Form
{
    public function get_display_type_name()
    {
        return NGG_PRO_FILM;
    }
    public function enqueue_static_resources()
    {
        $this->call_parent('enqueue_static_resources');
        $name = $this->object->get_display_type_name() . '-js';
        wp_enqueue_script($name, $this->object->get_static_url('imagely-pro_displaytype_admin#film_settings.js'), ['jquery.nextgen_radio_toggle']);
    }
    /**
     * @return array
     */
    public function _get_field_names()
    {
        return ['thumbnail_override_settings', 'nextgen_pro_film_images_per_page', 'nextgen_pro_film_image_spacing', 'nextgen_pro_film_border_size', 'nextgen_pro_film_frame_size', 'nextgen_pro_film_border_color', 'nextgen_pro_film_frame_color', 'nextgen_pro_film_alttext_display', 'nextgen_pro_film_alttext_font_color', 'nextgen_pro_film_alttext_font_size', 'nextgen_pro_film_description_display', 'nextgen_pro_film_description_font_color', 'nextgen_pro_film_description_font_size', 'display_type_view'];
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_pro_film_alttext_display_field($display_type)
    {
        return $this->object->_render_radio_field($display_type, 'alttext_display', __('Display image title', 'nextgen-gallery-pro'), $display_type->settings['alttext_display']);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_pro_film_alttext_font_color_field($display_type)
    {
        return $this->object->_render_color_field($display_type, 'alttext_font_color', __('Title font color', 'nextgen-gallery-pro'), $display_type->settings['alttext_font_color'], __('An empty color setting will use your theme colors', 'nextgen-gallery-pro'), empty($display_type->settings['alttext_display']) ? true : false);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_pro_film_alttext_font_size_field($display_type)
    {
        return $this->object->_render_number_field($display_type, 'alttext_font_size', __('Title font size', 'nextgen-gallery-pro'), $display_type->settings['alttext_font_size'], __('Measured in pixels. An empty or zero setting will use your theme font size', 'nextgen-gallery-pro'), empty($display_type->settings['alttext_display']) ? true : false, __('# of pixels', 'nextgen-gallery-pro'), 0);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_pro_film_description_display_field($display_type)
    {
        return $this->object->_render_radio_field($display_type, 'description_display', __('Display image description', 'nextgen-gallery-pro'), $display_type->settings['description_display']);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_pro_film_description_font_color_field($display_type)
    {
        return $this->object->_render_color_field($display_type, 'description_font_color', __('Description font color', 'nextgen-gallery-pro'), $display_type->settings['description_font_color'], __('An empty color setting will use your theme colors', 'nextgen-gallery-pro'), empty($display_type->settings['description_display']) ? true : false);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_pro_film_description_font_size_field($display_type)
    {
        return $this->object->_render_number_field($display_type, 'description_font_size', __('Description font size', 'nextgen-gallery-pro'), $display_type->settings['description_font_size'], __('Measured in pixels. An empty or zero setting will use your theme font size', 'nextgen-gallery-pro'), empty($display_type->settings['description_display']) ? true : false, __('# of pixels', 'nextgen-gallery-pro'), 0);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_pro_film_images_per_page_field($display_type)
    {
        return $this->object->_render_number_field($display_type, 'images_per_page', __('Images per page', 'nextgen-gallery-pro'), $display_type->settings['images_per_page'], __('"0" will display all images at once', 'nextgen-gallery-pro'), false, __('# of images', 'nextgen-gallery-pro'), 0);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_pro_film_border_size_field($display_type)
    {
        return $this->object->_render_number_field($display_type, 'border_size', __('Border size', 'nextgen-gallery-pro'), $display_type->settings['border_size'], '', false, '', 0);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_pro_film_border_color_field($display_type)
    {
        return $this->object->_render_color_field($display_type, 'border_color', __('Border color', 'nextgen-gallery-pro'), $display_type->settings['border_color']);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_pro_film_frame_size_field($display_type)
    {
        return $this->object->_render_number_field($display_type, 'frame_size', __('Frame size', 'nextgen-gallery-pro'), $display_type->settings['frame_size'], '', false, '', 0);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_pro_film_frame_color_field($display_type)
    {
        return $this->object->_render_color_field($display_type, 'frame_color', __('Frame color', 'nextgen-gallery-pro'), $display_type->settings['frame_color']);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_pro_film_image_spacing_field($display_type)
    {
        return $this->object->_render_number_field($display_type, 'image_spacing', __('Image spacing', 'nextgen-gallery-pro'), $display_type->settings['image_spacing'], '', false, '', 0);
    }
}
/**
 * @remove-for-nextgen-starter
 */
class A_NextGen_Pro_Grid_Album_Form extends A_NextGen_Pro_Album_Form
{
    function get_display_type_name()
    {
        return NGG_PRO_GRID_ALBUM;
    }
}
/**
 * @remove-for-nextgen-starter
 */
class A_NextGen_Pro_Slideshow_Form extends Mixin_Display_Type_Form
{
    function get_display_type_name()
    {
        return NGG_PRO_SLIDESHOW;
    }
    function enqueue_static_resources()
    {
        $this->call_parent('enqueue_static_resources');
        wp_enqueue_script($this->get_display_type_name() . '-js', $this->get_static_url('imagely-pro_displaytype_admin#slideshow_settings.js'), ['jquery.nextgen_radio_toggle']);
    }
    /**
     * Returns a list of fields to render on the settings page
     */
    function _get_field_names()
    {
        return ['nextgen_pro_slideshow_image_crop', 'nextgen_pro_slideshow_image_pan', 'nextgen_pro_slideshow_show_playback_controls', 'nextgen_pro_slideshow_show_captions', 'nextgen_pro_slideshow_caption_class', 'nextgen_pro_slideshow_caption_height', 'nextgen_pro_slideshow_aspect_ratio', 'nextgen_pro_slideshow_width_and_unit', 'nextgen_pro_slideshow_transition', 'nextgen_pro_slideshow_transition_speed', 'nextgen_pro_slideshow_slideshow_speed', 'nextgen_pro_slideshow_border_size', 'nextgen_pro_slideshow_border_color'];
    }
    /**
     * A similiar function is available in photocrati-nextgen_admin but has an inappropriate tooltip
     */
    function _render_nextgen_pro_slideshow_width_and_unit_field($display_type)
    {
        return $this->object->render_partial('photocrati-nextgen_admin#field_generator/nextgen_settings_field_width_and_unit', ['display_type_name' => $display_type->name, 'name' => 'width', 'label' => __('Gallery width', 'nextgen-gallery-pro'), 'value' => $display_type->settings['width'], 'text' => '', 'placeholder' => '', 'unit_name' => 'width_unit', 'unit_value' => $display_type->settings['width_unit'], 'options' => ['px' => __('Pixels', 'nextgen-gallery-pro'), '%' => __('Percent', 'nextgen-gallery-pro')]], true);
    }
    function _render_nextgen_pro_slideshow_image_crop_field($display_type)
    {
        return $this->_render_radio_field($display_type, 'image_crop', __('Crop images', 'nextgen-gallery-pro'), $display_type->settings['image_crop']);
    }
    function _render_nextgen_pro_slideshow_image_pan_field($display_type)
    {
        return $this->_render_radio_field($display_type, 'image_pan', __('Pan images', 'nextgen-gallery-pro'), $display_type->settings['image_pan'], '', empty($display_type->settings['image_crop']) ? true : false);
    }
    function _render_nextgen_pro_slideshow_show_captions_field($display_type)
    {
        return $this->_render_radio_field($display_type, 'show_captions', __('Show captions', 'nextgen-gallery-pro'), $display_type->settings['show_captions']);
    }
    function _render_nextgen_pro_slideshow_caption_class_field($display_type)
    {
        return $this->_render_select_field($display_type, 'caption_class', __('Caption location', 'nextgen-gallery-pro'), ['caption_above_stage' => __('Top', 'nextgen-gallery-pro'), 'caption_below_stage' => __('Bottom', 'nextgen-gallery-pro'), 'caption_overlay_top' => __('Top (Overlay)', 'nextgen-gallery-pro'), 'caption_overlay_bottom' => __('Bottom (Overlay)', 'nextgen-gallery-pro')], $display_type->settings['caption_class'], '', empty($display_type->settings['show_captions']) ? true : false);
    }
    function _render_nextgen_pro_slideshow_caption_height_field($display_type)
    {
        return $this->_render_number_field($display_type, 'caption_height', __('Caption height', 'nextgen-gallery-pro'), $display_type->settings['caption_height'], __('Measured in pixels', 'nextgen-gallery-pro'), empty($display_type->settings['show_captions']) ? true : false, __('pixels', 'nextgen-gallery-pro'), 1);
    }
    function _render_nextgen_pro_slideshow_slideshow_speed_field($display_type)
    {
        return $this->_render_number_field($display_type, 'slideshow_speed', __('Slideshow speed', 'nextgen-gallery-pro'), $display_type->settings['slideshow_speed'], __('Measured in seconds', 'nextgen-gallery-pro'), false, __('seconds', 'nextgen-gallery-pro'), 0);
    }
    function _render_nextgen_pro_slideshow_transition_field($display_type)
    {
        return $this->_render_select_field($display_type, 'transition', __('Transition effect', 'nextgen-gallery-pro'), ['fade' => __('Crossfade between images', 'nextgen-gallery-pro'), 'flash' => __('Fades into background color between images', 'nextgen-gallery-pro'), 'pulse' => __('Quickly move the image into the background color, then fade into the next image', 'nextgen-gallery-pro'), 'slide' => __('Slide images depending on image position', 'nextgen-gallery-pro'), 'fadeslide' => __('Fade between images and slide slightly at the same time', 'nextgen-gallery-pro')], $display_type->settings['transition'], '', false);
    }
    function _render_nextgen_pro_slideshow_transition_speed_field($display_type)
    {
        return $this->_render_number_field($display_type, 'transition_speed', __('Transition speed', 'nextgen-gallery-pro'), $display_type->settings['transition_speed'], __('Measured in seconds', 'nextgen-gallery-pro'), false, __('seconds', 'nextgen-gallery-pro'), 0);
    }
    function _render_nextgen_pro_slideshow_border_size_field($display_type)
    {
        return $this->_render_number_field($display_type, 'border_size', __('Border size', 'nextgen-gallery-pro'), $display_type->settings['border_size'], __('Borders will not be applied if "Crop Images" is enabled', 'nextgen-gallery-pro'), !empty($display_type->settings['image_crop']) ? true : false, '', 0);
    }
    function _render_nextgen_pro_slideshow_border_color_field($display_type)
    {
        return $this->_render_color_field($display_type, 'border_color', __('Border color', 'nextgen-gallery-pro'), $display_type->settings['border_color'], '', !empty($display_type->settings['image_crop']) ? true : false);
    }
    function _render_nextgen_pro_slideshow_aspect_ratio_field($display_type)
    {
        return $this->_render_select_field($display_type, 'aspect_ratio', __('Stage aspect ratio', 'nextgen-gallery-pro'), $this->_get_aspect_ratio_options(), $display_type->settings['aspect_ratio'], '', false);
    }
    function _render_nextgen_pro_slideshow_show_playback_controls_field($display_type)
    {
        return $this->_render_radio_field($display_type, 'show_playback_controls', __('Show play controls', 'nextgen-gallery-pro'), $display_type->settings['show_playback_controls']);
    }
}
/**
 * @remove-for-nextgen-starter
 */
class A_NextGen_Pro_Captions_Form extends Mixin
{
    function _get_field_names()
    {
        $fields = $this->call_parent('_get_field_names');
        $fields[] = 'nextgen_pro_captions_enabled';
        $fields[] = 'nextgen_pro_captions_display_sharing';
        $fields[] = 'nextgen_pro_captions_display_title';
        $fields[] = 'nextgen_pro_captions_display_description';
        $fields[] = 'nextgen_pro_captions_animation';
        return $fields;
    }
    function enqueue_static_resources()
    {
        $this->call_parent('enqueue_static_resources');
        wp_enqueue_script('photocrati-nextgen_pro_captions_settings-js', $this->get_static_url('imagely-pro_displaytype_admin#hover_captions_settings.js'), ['jquery.nextgen_radio_toggle']);
    }
    function _render_nextgen_pro_captions_enabled_field($display_type)
    {
        return $this->_render_radio_field($display_type, 'captions_enabled', __('Enable caption overlay', 'nextgen-gallery-pro'), isset($display_type->settings['captions_enabled']) ? $display_type->settings['captions_enabled'] : false);
    }
    function _render_nextgen_pro_captions_display_sharing_field($display_type)
    {
        return $this->_render_radio_field($display_type, 'captions_display_sharing', __('Display share icons', 'nextgen-gallery-pro'), isset($display_type->settings['captions_display_sharing']) ? $display_type->settings['captions_display_sharing'] : true, '', empty($display_type->settings['captions_enabled']) ? true : false);
    }
    function _render_nextgen_pro_captions_display_title_field($display_type)
    {
        return $this->_render_radio_field($display_type, 'captions_display_title', __('Display image title', 'nextgen-gallery-pro'), isset($display_type->settings['captions_display_title']) ? $display_type->settings['captions_display_title'] : true, '', empty($display_type->settings['captions_enabled']) ? true : false);
    }
    function _render_nextgen_pro_captions_display_description_field($display_type)
    {
        return $this->_render_radio_field($display_type, 'captions_display_description', __('Display image description', 'nextgen-gallery-pro'), isset($display_type->settings['captions_display_description']) ? $display_type->settings['captions_display_description'] : true, '', empty($display_type->settings['captions_enabled']) ? true : false);
    }
    function _render_nextgen_pro_captions_animation_field($display_type)
    {
        return $this->_render_select_field($display_type, 'captions_animation', __('Animation type', 'nextgen-gallery-pro'), ['fade' => __('Fade in', 'nextgen-gallery-pro'), 'slideup' => __('Slide up', 'nextgen-gallery-pro'), 'slidedown' => __('Slide down', 'nextgen-gallery-pro'), 'slideleft' => __('Slide left', 'nextgen-gallery-pro'), 'titlebar' => __('Titlebar', 'nextgen-gallery-pro'), 'plain' => __('Plain', 'nextgen-gallery-pro')], isset($display_type->settings['captions_animation']) ? $display_type->settings['captions_animation'] : 'slideup', '', empty($display_type->settings['captions_enabled']) ? true : false);
    }
}
/**
 * @remove-for-nextgen-starter
 */
class A_NextGen_Pro_ImageBrowser_Form extends Mixin_Display_Type_Form
{
    function get_display_type_name()
    {
        return NGG_PRO_IMAGEBROWSER;
    }
    /**
     * Returns a list of fields to render on the settings page
     */
    function _get_field_names()
    {
        return ['ajax_pagination', 'display_type_view'];
    }
}
/**
 * @remove-for-nextgen-starter
 */
class A_NextGen_Pro_List_Album_Form extends A_NextGen_Pro_Album_Form
{
    function get_display_type_name()
    {
        return NGG_PRO_LIST_ALBUM;
    }
    /**
     * Adds pro-list-album specific fields to the defaults provided in A_NextGen_Pro_ALbums_Form
     */
    function _get_field_names()
    {
        $fields = parent::_get_field_names();
        $fields[] = 'nextgen_pro_list_album_description_color';
        $fields[] = 'nextgen_pro_list_album_description_size';
        return $fields;
    }
    function _render_nextgen_pro_list_album_description_color_field($display_type)
    {
        return $this->_render_color_field($display_type, 'description_color', 'Description color', $display_type->settings['description_color']);
    }
    function _render_nextgen_pro_list_album_description_size_field($display_type)
    {
        return $this->_render_number_field($display_type, 'description_size', 'Description size', $display_type->settings['description_size'], '', false, '', 0);
    }
}
/**
 * @remove-for-nextgen-starter
 */
class A_NextGen_Pro_Masonry_Form extends Mixin_Display_Type_Form
{
    function get_display_type_name()
    {
        return NGG_PRO_MASONRY;
    }
    /**
     * Returns a list of fields to render on the settings page
     */
    function _get_field_names()
    {
        return ['nextgen_pro_masonry_size', 'nextgen_pro_masonry_padding', 'nextgen_pro_masonry_center_gallery', 'display_type_view'];
    }
    function _render_nextgen_pro_masonry_size_field($display_type)
    {
        return $this->object->_render_number_field($display_type, 'size', __('Maximum image width', 'nextgen-gallery-pro'), $display_type->settings['size'], __('Measured in pixels', 'nextgen-gallery-pro'));
    }
    function _render_nextgen_pro_masonry_padding_field($display_type)
    {
        return $this->object->_render_number_field($display_type, 'padding', __('Image padding', 'nextgen-gallery-pro'), $display_type->settings['padding'], __('Measured in pixels', 'nextgen-gallery-pro'));
    }
    function _render_nextgen_pro_masonry_center_gallery_field($display_type)
    {
        return $this->object->_render_radio_field($display_type, 'center_gallery', __('Center the gallery', 'nextgen-gallery-pro'), $display_type->settings['center_gallery']);
    }
}
/**
 * Class A_Mosaic_Form
 *
 * @mixin C_Form
 * @adapts I_Form using "photocrati-nextgen_pro_mosaic" context
 */
class A_Mosaic_Form extends Mixin_Display_Type_Form
{
    function get_display_type_name()
    {
        return NGG_PRO_MOSAIC;
    }
    function enqueue_static_resources()
    {
        $this->call_parent('enqueue_static_resources');
        wp_enqueue_script(NGG_PRO_MOSAIC . '_admin_settings_js', $this->object->get_static_url('imagely-pro_displaytype_admin#mosaic_settings.js'), ['jquery.nextgen_radio_toggle'], '3.16.0');
    }
    function _get_field_names()
    {
        return ['display_type_view', 'mosaic_last_row', 'mosaic_lazy_load_batch', 'mosaic_lazy_load_enable', 'mosaic_lazy_load_initial', 'mosaic_margins', 'mosaic_row_height'];
    }
    function _render_mosaic_row_height_field($display_type)
    {
        $settings = $display_type->settings;
        return $this->_render_number_field($display_type, 'row_height', __('Row height', 'nextgen-gallery-pro'), $settings['row_height'], '', false, '', 6);
    }
    function _render_mosaic_margins_field($display_type)
    {
        $settings = $display_type->settings;
        return $this->_render_number_field($display_type, 'margins', __('Margins', 'nextgen-gallery-pro'), $settings['margins'], '', false, '', 0);
    }
    function _render_mosaic_last_row_field($display_type)
    {
        $settings = $display_type->settings;
        return $this->_render_select_field($display_type, 'last_row', __('Justify last row', 'nextgen-gallery-pro'), ['justify' => __('Justify', 'nextgen-gallery-pro'), 'nojustify' => __('Do not justify', 'nextgen-gallery-pro'), 'hide' => __('Hide', 'nextgen-gallery-pro')], $settings['last_row'], __('When aligning the last row some images may appear cropped. Select "Do not justify" to allow the last row to appear flush but "unfinished". "Hide" will omit any images that can not be justified without cropping.', 'nextgen-gallery-pro'));
    }
    function _render_mosaic_lazy_load_enable_field($display_type)
    {
        // _render_radio_field($display_type, $name, $label, $value, $text = '', $hidden = FALSE)
        $settings = $display_type->settings;
        return $this->_render_radio_field($display_type, 'lazy_load_enable', __('Enable "lazy" image loading', 'nextgen-gallery-pro'), $settings['lazy_load_enable']);
    }
    function _render_mosaic_lazy_load_initial_field($display_type)
    {
        $settings = $display_type->settings;
        return $this->_render_number_field($display_type, 'lazy_load_initial', __('Images to display at start', 'nextgen-gallery-pro'), $settings['lazy_load_initial'], '', empty($settings['lazy_load_enable']) ? true : false);
    }
    function _render_mosaic_lazy_load_batch_field($display_type)
    {
        $settings = $display_type->settings;
        return $this->_render_number_field($display_type, 'lazy_load_batch', __('Images to load when scrolling', 'nextgen-gallery-pro'), $settings['lazy_load_batch'], '', empty($settings['lazy_load_enable']) ? true : false);
    }
}
/**
 * @remove-for-nextgen-starter
 */
class A_Search_Form extends Mixin_Display_Type_Form
{
    public function get_display_type_name()
    {
        return NGG_PRO_SEARCH;
    }
    /**
     * Enqueues static resources required by this form
     */
    public function enqueue_static_resources()
    {
        $this->call_parent('enqueue_static_resources');
        $this->object->enqueue_script('nextgen_image_search_admin_form_js', $this->object->get_static_url('imagely-pro_displaytype_admin#search_settings.js'));
    }
    /**
     * Returns a list of fields to render on the settings page
     */
    public function _get_field_names()
    {
        return ['nextgen_frontend_search_gallery_display_type', 'nextgen_frontend_search_enable_tag_filter', 'nextgen_frontend_search_search_alttext', 'nextgen_frontend_search_search_description', 'nextgen_frontend_search_search_tags', 'nextgen_frontend_search_search_mode', 'nextgen_frontend_search_minimum_relevance', 'nextgen_frontend_search_limit', 'nextgen_frontend_search_order_by_relevance', 'nextgen_frontend_search_order_by', 'nextgen_frontend_search_order_direction'];
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_frontend_search_enable_tag_filter_field($display_type)
    {
        return $this->object->_render_radio_field($display_type, 'enable_tag_filter', __('Enable filtering results by tag', 'nextgen-gallery-pro'), $display_type->settings['enable_tag_filter']);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_frontend_search_search_alttext_field($display_type)
    {
        return $this->object->_render_radio_field($display_type, 'search_alttext', __('Search image alttext', 'nextgen-gallery-pro'), $display_type->settings['search_alttext']);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_frontend_search_search_description_field($display_type)
    {
        return $this->object->_render_radio_field($display_type, 'search_description', __('Search image description', 'nextgen-gallery-pro'), $display_type->settings['search_description']);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_frontend_search_search_tags_field($display_type)
    {
        return $this->object->_render_radio_field($display_type, 'search_tags', __('Search image tags', 'nextgen-gallery-pro'), $display_type->settings['search_tags']);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_frontend_search_search_mode_field($display_type)
    {
        $options = ['natural' => __('Natural language', 'nextgen-gallery-pro'), 'boolean' => __('Boolean', 'nextgen-gallery-pro')];
        return $this->object->_render_select_field($display_type, 'search_mode', __('Database search mode', 'nextgen-gallery-pro'), $options, $display_type->settings['search_mode'], __('A natural language search treats the requested string as a phrase in text without any operators except for quotation marks. A boolean search uses special rules and operators such as the plus and minus symbols.', 'nextgen-gallery-pro'));
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_frontend_search_minimum_relevance_field($display_type)
    {
        return $this->object->_render_number_field($display_type, 'minimum_relevance', __('Minimum relevance', 'nextgen-gallery-pro'), $display_type->settings['minimum_relevance'], __('The database server assigns a relevance score to each possible image based on a number of factors with zero being not at all relevant. Users with smaller databases or images whose alttext or description only holds a few words will need a lower number here; possibly as low as 0.05. It is unlikely many users will need to raise this beyond one.', 'nextgen-gallery-pro'), false, '', 0);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_frontend_search_limit_field($display_type)
    {
        return $this->object->_render_number_field($display_type, 'limit', __('Limit search results', 'nextgen-gallery-pro'), $display_type->settings['limit'], __('Limit search results to this amount. A setting of zero means no limitations are applied', 'nextgen-gallery-pro'), false, '', 0);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_frontend_search_order_by_relevance_field($display_type)
    {
        return $this->object->_render_radio_field($display_type, 'order_by_relevance', __('Order by relevance first', 'nextgen-gallery-pro'), $display_type->settings['order_by_relevance'], __('When enabled search results will be ordered by their relevance first, then by the secondary order setting'));
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_frontend_search_order_by_field($display_type)
    {
        $options = ['pid' => __('Image ID', 'nextgen-gallery-pro'), 'galleryid' => __('Gallery ID', 'nextgen-gallery-pro'), 'filename' => __('Image filename', 'nextgen-gallery-pro'), 'imagedate' => __('Image date (EXIF or time of upload)', 'nextgen-gallery-pro')];
        return $this->object->_render_select_field($display_type, 'order_by', __('Order search results by', 'nextgen-gallery-pro'), $options, $display_type->settings['order_by']);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_frontend_search_order_direction_field($display_type)
    {
        $options = ['ASC' => __('Ascending', 'nextgen-gallery-pro'), 'DESC' => __('Descending', 'nextgen-gallery-pro')];
        return $this->object->_render_select_field($display_type, 'order_direction', __('Order direction of search results', 'nextgen-gallery-pro'), $options, $display_type->settings['order_direction']);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string
     */
    public function _render_nextgen_frontend_search_gallery_display_type_field($display_type)
    {
        $options = [];
        $types = \Imagely\NGGPro\DataMappers\DisplayType::get_instance()->find_by_entity_type('image');
        foreach ($types as $type) {
            if (!empty($type->hidden_from_ui) && $type->hidden_from_ui) {
                continue;
            }
            if ($type->name === NGG_PRO_SEARCH) {
                continue;
            }
            $options[$type->name] = $type->title;
        }
        return $this->object->_render_select_field($display_type, 'gallery_display_type', __('Display results as', 'nextgen-gallery-pro'), $options, $display_type->settings['gallery_display_type']);
    }
}
/** @remove-for-nextgen-starter */
class A_NextGen_Pro_Sidescroll_Form extends Mixin_Display_Type_Form
{
    function get_display_type_name()
    {
        return NGG_PRO_SIDESCROLL;
    }
    /**
     * Returns a list of fields to render on the settings page
     */
    function _get_field_names()
    {
        return ['nextgen_pro_sidescroll_height', 'display_type_view'];
    }
    /**
     * Renders the images_per_page settings field
     *
     * @param \Imagely\NGG\DataTypes\DisplayType $display_type
     * @return string
     */
    function _render_nextgen_pro_sidescroll_height_field($display_type)
    {
        return $this->_render_number_field($display_type, 'height', __('Gallery Height', 'nextgen-gallery-pro'), $display_type->settings['height'], __('Provide desired gallery height in pixels.', 'nextgen-gallery-pro'), false, '', 0);
    }
}
/**
 * @remove-for-nextgen-starter
 */
class A_NextGen_Pro_Horizontal_Filmstrip_Form extends A_NextGen_Pro_Slideshow_Form
{
    function get_display_type_name()
    {
        return NGG_PRO_HORIZONTAL_FILMSTRIP;
    }
    function enqueue_static_resources()
    {
        $this->call_parent('enqueue_static_resources');
        wp_enqueue_script($this->object->get_display_type_name() . '-js', $this->get_static_url('imagely-pro_displaytype_admin#horizontal_filmstrip_settings.js'), ['jquery.nextgen_radio_toggle']);
    }
    /**
     * Returns a list of fields to render on the settings page
     */
    function _get_field_names()
    {
        $fields = parent::_get_field_names();
        $fields[] = 'thumbnail_override_settings';
        return $fields;
    }
}
/**
 * @remove-for-nextgen-starter
 */
class A_NextGen_Pro_Thumbnail_Grid_Form extends Mixin_Display_Type_Form
{
    function get_display_type_name()
    {
        return NGG_PRO_THUMBNAIL_GRID;
    }
    function enqueue_static_resources()
    {
        $this->call_parent('enqueue_static_resources');
        wp_enqueue_script($this->object->get_display_type_name() . '-js', $this->object->get_static_url('imagely-pro_displaytype_admin#thumbnail_grid_settings.js'), ['jquery.nextgen_radio_toggle']);
    }
    /**
     * Returns a list of fields to render on the settings page
     */
    function _get_field_names()
    {
        return ['thumbnail_override_settings', 'nextgen_pro_thumbnail_grid_images_per_page', 'nextgen_pro_thumbnail_grid_border_size', 'nextgen_pro_thumbnail_grid_border_color', 'nextgen_pro_thumbnail_grid_spacing', 'nextgen_pro_thumbnail_grid_number_of_columns', 'display_type_view'];
    }
    /**
     * Renders the images_per_page settings field
     *
     * @param \Imagely\NGG\DataTypes\DisplayType $display_type
     * @return string
     */
    function _render_nextgen_pro_thumbnail_grid_images_per_page_field($display_type)
    {
        return $this->_render_number_field($display_type, 'images_per_page', __('Images per page', 'nextgen-gallery-pro'), $display_type->settings['images_per_page'], __('"0" will display all images at once', 'nextgen-gallery-pro'), false, __('# of images', 'nextgen-gallery-pro'), 0);
    }
    /**
     * @param \Imagely\NGG\DataTypes\DisplayType $display_type
     * @return string
     */
    function _render_nextgen_pro_thumbnail_grid_border_size_field($display_type)
    {
        return $this->_render_number_field($display_type, 'border_size', __('Border size', 'nextgen-gallery-pro'), $display_type->settings['border_size'], '', false, '', 0);
    }
    /**
     * @param \Imagely\NGG\DataTypes\DisplayType $display_type
     * @return string
     */
    function _render_nextgen_pro_thumbnail_grid_spacing_field($display_type)
    {
        return $this->_render_number_field($display_type, 'spacing', __('Spacing', 'nextgen-gallery-pro'), $display_type->settings['spacing']);
    }
    /**
     * @param \Imagely\NGG\DataTypes\DisplayType $display_type
     * @return string
     */
    function _render_nextgen_pro_thumbnail_grid_number_of_columns_field($display_type)
    {
        return $this->_render_number_field($display_type, 'number_of_columns', __('Number of columns to display', 'nextgen-gallery-pro'), $display_type->settings['number_of_columns'], __('An empty or zero in this field will use a responsive layout', 'nextgen-gallery-pro'));
    }
    /**
     * @param \Imagely\NGG\DataTypes\DisplayType $display_type
     * @return string
     */
    function _render_nextgen_pro_thumbnail_grid_border_color_field($display_type)
    {
        return $this->_render_color_field($display_type, 'border_color', __('Border color', 'nextgen-gallery-pro'), $display_type->settings['border_color']);
    }
}
/**
 * @remove-for-nextgen-starter
 */
class A_NextGen_Pro_Tile_Form extends Mixin_Display_Type_Form
{
    function get_display_type_name()
    {
        return NGG_PRO_TILE;
    }
    function enqueue_static_resources()
    {
        $this->call_parent('enqueue_static_resources');
        wp_enqueue_script($this->get_display_type_name() . '-js', $this->get_static_url('imagely-pro_displaytype_admin#tile_settings.js'), ['jquery.nextgen_radio_toggle']);
    }
    /**
     * Returns a list of fields to render on the settings page
     */
    function _get_field_names()
    {
        return ['nextgen_pro_tile_override_maximum_width', 'nextgen_pro_tile_maximum_width'];
    }
    /**
     * @param C_Display_Type $display_type
     * @return string Rendered HTML
     */
    function _render_nextgen_pro_tile_override_maximum_width_field($display_type)
    {
        return $this->_render_radio_field($display_type, 'override_maximum_width', __('Override maximum gallery width', 'nextgen-gallery-pro'), $display_type->settings['override_maximum_width'], __("Gallery width is set to your theme's content width but this can be overridden to create smaller galleries. If your theme does not provide the \$content_width feature the default will fallback to 2000px.", 'nextgen-gallery-pro'));
    }
    /**
     * @param C_Display_Type $display_type
     * @return string Rendered HTML
     */
    function _render_nextgen_pro_tile_maximum_width_field($display_type)
    {
        return $this->_render_number_field($display_type, 'maximum_width', __('Maximum gallery width', 'nextgen-gallery-pro'), $display_type->settings['maximum_width'], __('Measured in pixels', 'nextgen-gallery-pro'), empty($display_type->settings['override_maximum_width']) ? true : false, '', 100);
    }
    /**
     * @param C_Display_Type $display_type
     * @return string Rendered HTML
     */
    function _render_nextgen_pro_tile_margin_field($display_type)
    {
        return $this->_render_number_field($display_type, 'margin', __('Margin between blocks of images', 'nextgen-gallery-pro'), $display_type->settings['margin'], __('Measured in pixels', 'nextgen-gallery-pro'), false, 0, 0);
    }
}