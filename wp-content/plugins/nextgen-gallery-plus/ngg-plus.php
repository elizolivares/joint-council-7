<?php

/**
 * Plugin Name: NextGEN Plus
 * Description: A premium add-on for NextGEN Gallery with beautiful new gallery displays and a fullscreen, responsive Pro Lightbox with social sharing and commenting.
 * Version: 1.16.2
 * Plugin URI: http://www.nextgen-gallery.com
 * Author: Imagely
 * Author URI: https://www.imagely.com
 * License: GPLv3
 * Requires at least: 5.5.0
 * Requires PHP: 7.0
 * Text Domain: nextgen-gallery-pro
 * Domain Path: /static/I18N/
 *
 * @package NextGEN Gallery
 *
 * @remove-for-nextgen-pro
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro;

if (!class_exists('\\Imagely\\NGGPro\\Bootloader')) {
    require_once 'src' . DIRECTORY_SEPARATOR . 'Bootloader.php';
    new Bootloader('1.16.2', __FILE__, 'plus');
}
// Disable other NextGEN Gallery addons if they are already activated.
\add_action('activate_' . \plugin_basename(__FILE__), function () {
    require_once 'src' . DIRECTORY_SEPARATOR . 'PreventDuplicateActivations.php';
    prevent_duplicate_nextgen_extensions(__FILE__);
});