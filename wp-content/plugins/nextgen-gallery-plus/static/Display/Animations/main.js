const animateNextGENImage = function (node, animation) {
    const prefix = 'animate__';
    return new Promise((resolve, reject) => {
        const animationName = `${prefix}${animation}`;

        // Images are given opacity: 0 to prevent their display before the animation effects start.
        // So we drop the class that hides the image and immediately assign the animation class.
        node.classList.remove('nextgen-pro-animate-images-loading');
        node.classList.add(`${prefix}animated`, animationName);

        // Remove the animation class when the effect has finished.
        function handleAnimationEnd(event) {
            event.stopPropagation();
            node.classList.remove(`${prefix}animated`, animationName);
            resolve('Animation ended');
        }

        node.addEventListener('animationend', handleAnimationEnd, {once: true});
    });
};

const processNextGENImageAnimations = function(styleName) {
    const containers = document.querySelectorAll('.nextgen-pro-animate-images-gallery-container');
    containers.forEach(function (container) {
        // Not all display types support XHR pagination; if the data-animate-all=1 is set then we force
        // a workaround here and apply the animation to all images in the gallery, not just the first.
        if (1 === parseInt(container.dataset.animateAll)) {
            styleName = 'pagination';
        }

        const style              = container.dataset[`${styleName}Style`];
        const duration = parseInt(container.dataset[`${styleName}Duration`]);
        const delay    = parseInt(container.dataset[`${styleName}Delay`]);

        container.querySelectorAll('.nextgen-pro-animate-images-image-container').forEach(
            function(image, index) {
                image.style.setProperty('--animate-duration', `${duration}ms`);
                setTimeout(() => {
                    animateNextGENImage(image, style);
                }, (0 === index ? 0 : index * delay));
            }
        );
    });
};

document.addEventListener('nextgen_page_refreshed', function() {
    processNextGENImageAnimations('pagination');
});

processNextGENImageAnimations('firstload');
