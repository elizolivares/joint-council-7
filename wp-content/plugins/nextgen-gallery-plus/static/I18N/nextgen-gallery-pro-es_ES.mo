��    �      �                   w   (     �     �  0   �  �        �     �     �     �     �  �   �  J   h  .   �  4   �  h     g   �  s   �     \     m     |     �  	   �  &   �     �     �  	   �     �     �     �     �                    "     4  	   9  "   C  "   f     �     �     �     �     �     �     �     �               !     A     X  	   a     k  A   p     �     �     �  
   �     �     �  L      <   M     �     �     �  	   �     �     �  +   �     �               :     ?     O     o     ~     �     �     �     �     �     �  	   �     �     �               &  �   4     �     �     �     �            (     (   H     q     x     }     �     �     �     �     �     �  '   �  (        .     :  
   @     K  	   R  
   \     g     n  
   �     �     �     �     �     �     �     �  
   �     �  	   �     �     �                     "  	   +     5     >  	   S  :   ]  ,   �     �     �     �     �       	             .     F     T     n     s  �   y  �  ]     W      t      �   :   �   @   �   �  !  �   �'  ,   I(  I   v(  .   �(  6   �(     &)  :   A)     |)     �)     �)     �)     �)     �)     �)     �)  
   �)     �)  ?   *  Y   D*  U   �*  N   �*  -   C+  w   q+  :   �+  S   $,  5   x,  8   �,  5   �,  6   -  ?   T-  B   �-  P   �-     (.  `  A.     �/  +   �/  )   �/     0     .0  $   A0     f0     0     �0     �0     �0  �  �0      �2  �   �2     >3  "   X3  9   {3  �   �3     V4  
   i4     t4     �4     �4  �   �4  G   R5  2   �5  9   �5  p   6  y   x6  �   �6     �7     �7     �7     �7  	   �7  .   �7  	   �7     8     8     $8     58     D8     I8     Y8  
   `8     k8     w8     �8  
   �8  (   �8  1   �8     �8     9     9     9  %   39  -   Y9     �9     �9     �9     �9  &   �9     �9  
   �9  
   
:     :  J   :  	   f:     p:     �:     �:     �:     �:  X   �:  S   ;     b;     i;     p;     �;     �;  "   �;  D   �;     �;     <     &<  	   A<     K<     _<     <     �<     �<     �<     �<     �<     �<     �<     �<     =     =     -=     G=     ]=  �   r=     *>     3>     9>     J>     ^>     e>  *   r>  *   �>     �>     �>     �>     �>     �>     ?  	   ?     "?      2?  +   S?  '   ?     �?     �?     �?     �?     �?     �?     �?     �?  
   @     @     $@     -@     6@     ?@  	   P@     Z@     f@     r@     z@     �@     �@     �@     �@     �@  	   �@     �@     �@     �@     A  R   A  =   pA     �A     �A     �A      �A     �A      B     B     B     /B     ;B  
   YB  	   dB  �   nB  �  VC     QH     jH     �H  1   �H  K   �H  �  
I  o   �O  .   VP  Z   �P  3   �P  G   Q     \Q  A   xQ     �Q     �Q     �Q     �Q     �Q     R     !R     4R  
   HR  
   SR  ;   ^R  l   �R  d   S  Y   lS  1   �S  �   �S  @   �T  ]   �T  (   4U  &   ]U  '   �U  +   �U  3   �U  7   V  ]   DV     �V  �  �V     ?X  4   \X  1   �X     �X     �X  -   �X     (Y     AY     PY     _Y     hY   %1$s comment %1$s comments %d order(s) have been processed and marked as paid. This page will now reload to display the updated order information. %s cannot be empty. %s is in an invalid format. '%s' or '%s' are invalid ISO 4217 numeric codes. <p>Thanks very much for your purchase! We'll be in touch shortly via email to confirm your order and to provide details on payment.</p> Add To Cart Address Address Line 1 Address Line 2 All order statuses An error has occurred while processing an order on your website with a Stripe payment. Please include the following information when filing a bug report: An error has occurred while processing your order. Please try again later. An error has occurred, please try again later. An unexpected problem occurred contacting PayPal.com An unexpected problem occurred contacting PayPal.com to register your site for payment notifications: %s An unexpected problem occurred when contacting PayPal.com to create your order. Please try again later. An unexpected problem occurred when contacting PayPal.com. Please contact the site owner to complete your purchase. Awaiting Payment Calculating... Cancel Cancel page Cancelled Change quantities to update your cart. Check Check Again Check Now Check stripe Checking... Checkout Checkout page City Comment Comments Continue shopping Cost Cost (%s) Could not complete order at PayPal Could not find order reference #%s Could not save order: Country Coupon Coupon code Coupon code must be unique Coupon code must not be blank Customer Delete Description Digital Downloads Digital Downloads for Order #%s Digital downloads page Disabled Discount: Done Done! You have a valid credit card on file (last four digits %s). Download Economy Shipping Edit Pricelist Empty cart Expedited Shipping Failed Form contains errors, please correct all errors before submitting the order. Form contains errors, please fix these errors before saving. Fraud Free Free Shipping Full Name Image International Shipping (Prints) Invalid country selected, please try again. Invalid coupon Invalid request Invalid zip or postal code. Item Leave a comment Low, Medium, or High Resolution Manage Coupons Manage Galleries Manage Pricelist Manage Pricelists Metal Prints Mounted Prints Name Name: New Order New Pricelist No images selected No orders found Not found message Nothing found Oops! This page usually displays details for image purchases, but you have not ordered any images yet. Please feel free to continue browsing. Thanks for visiting. Optional Order Order Cancelled Order Details Order Status Order from %s Order submitted to PayPal for processing Order submitted to Stripe for processing Orders Paid Pay by check Pay with Card Pay with PayPal Payment Gateway Phone Place order Please Add Address Please enter a valid zip or postal code Please provide a name and e-mail address Postal Code Price Price (%s) Price: Pricelist Pricelists Prints Priority Shipping Processing Processing... Product Quantity Remove Remove card Reply Required Resolution Save Saving... Search Orders Search term Select Country Select Region Send Ship to: Ship via: Shipping Shipping information Shipping: Site error while processing a NextGen Pro order via Stripe Sorry, this image is not currently for sale. Standard Shipping State State / Region Status: 404 Image not found Subject Submitted Submitted to PayPal Submitting {0} image{1} Submitting... Subtotal before discount: Tax: Taxes Thank you for your order, %%customer_name%%.

You ordered %%item_count%% items, and have been billed a total of %%total_amount%%.

To review your order, please go to %%order_details_page%%.

Thanks for shopping at %%site_url%%! Thank you for your order, [customer_name]. You ordered the following items:
                [items]

                <h3>Order Details</h3>
                <p>
                Subtotal: [subtotal_amount]<br/>
                [if_used_coupon]Discount: [discount_amount]<br/>[/if_used_coupon]
                [if_ordered_shippable_items]Shipping: [shipping_amount]<br/>[/if_ordered_shippable_items]
                [if_has_tax]Tax: [tax_amount]<br/>[/if_has_tax]
                Total: [total_amount]<br/>
                </p>

                [if_ordered_shippable_items]
                <p>
                We will be shipping your items to:<br/>
                [shipping_street_address]<br/>
                [if_shipping_has_address_line]
                    [shipping_address_line]<br/>
                [/if_shipping_has_address_line]

                [shipping_city], [shipping_state] [shipping_zip]<br/>
                [shipping_country]
                </p>
                [/if_ordered_shippable_items]

                [if_ordered_digital_downloads]
                <h3>Digital Downloads</h3>
                <p>You may download your digital products <a href='[digital_downloads_page_url]'>here.</a></p>
                [/if_ordered_digital_downloads]
             Thank you for your purchase! Thank-you page Thanks The billed amount and the amount received are not the same The credit card you submitted (last four digits %s) has expired. The customer [customer_name] ordered the following items:
            [items]

            <h3>Order Details</h3>
            <p>
            Subtotal: [subtotal_amount]<br/>
            [if_used_coupon]Discount: [discount_amount]<br/>[/if_used_coupon]
            [if_ordered_shippable_items]Shipping: [shipping_amount]<br/>[/if_ordered_shippable_items]
            [if_has_tax]Tax: [tax_amount]<br/>[/if_has_tax]
            Total: [total_amount]<br/>
            [if_ordered_printlab_items]
                Printlab Status: [printlab_status_full]</br>
            [/if_ordered_printlab_items]
            [if_printlab_order_success]
                Cost of Goods: [cost_of_goods]<br/>
                [if_printlab_pdf_invoice_link]
                    Invoice: <a target='_blank' href='[printlab_pdf_invoice_link]'>View PDF</a></br>
                [/if_printlab_pdf_invoice_link]
            [/if_printlab_order_success]
            </p>

            [if_ordered_shippable_items]
            <p>
            We will be shipping your items to:<br/>
            [shipping_street_address]<br/>
            [if_shipping_has_address_line]
            [shipping_address_line]<br/>
            [/if_shipping_has_address_line]
            [shipping_city], [shipping_state] [shipping_zip]<br/>
            [shipping_country]<br/>
            [if_shipping_has_phone]
            Ph: [shipping_phone]
            [/if_shipping_has_phone]
            </p>
            [/if_ordered_shippable_items]

            [if_ordered_digital_downloads]
            <h3>Digital Downloads</h3>
            <p>Download link for digital products: <a href='[digital_downloads_page_url]'>here.</a></p>
            [/if_ordered_digital_downloads]
         The order has been submitted to PayPal, and we're waiting for a response from PayPal to indicate that the payment has been made. There have been no items added to your cart. There was a problem looking up your order status. Please try again later. There was a problem trying to remove your card This field must be unique from every other coupon code This image is not for sale This order has been marked as fraud and has been reported. Totals Unknown Unpaid Update Cart Update shipping & taxes User confirmation Verifying order View Cart / Checkout View Order View Orders We couldn't find your order. We apologize for the inconvenience We haven't received payment confirmation yet. This may take a few minutes. Please wait... We're sorry, but one or more items you've selected cannot be shipped to this country. We're sorry, but something went wrong processing your order. Please try again. We're sorry, but we couldn't find your order. We're sorry, but we were unable to save your credit card information. Please check your card information and try again. We're verifying your order. This might take a few minutes. You must be <a href="%s" id="nggpl-comment-logout">logged in</a> to post a comment. You must provide a city for the shipping information. You must provide a country for the shipping information. You must provide a name for the shipping information. You must provide a state for the shipping information. You must provide a street address for the shipping information. You must provide a valid postal code for the shipping information. You must provide an email address in case of any problems fulfilling your order. You order was cancelled. You received a payment of %%total_amount%% from %%customer_name%% (%%email%%). For more details, visit: %%order_details_page%%

%%gateway_admin_note%%

Here is a comma separated list of the image file names. You can copy and
paste this in your favorite image management software to quickly search for
and find all selected images.

Files: %%file_list%% Your card has been removed Your cart also contains %d items at no cost Your cart also contains 1 item at no cost Your cart has been updated Your cart is empty Your comment is awaiting moderation. Your order was cancelled Zip Zip / Postal Code seconds see instructions Project-Id-Version: NextGEN Pro
Report-Msgid-Bugs-To: https://www.imagely.com/wordpress-gallery-plugin/nextgen-pro/
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2015-04-14 22:06:10+00:00
PO-Revision-Date: 2022-03-04 17:31+0000
Last-Translator: 
Language-Team: Español
X-Generator: Loco https://localise.biz/
X-Domain: nextgen-gallery-pro
Language: es-ES
Plural-Forms: nplurals=2; plural=n != 1;
X-Loco-Version: 2.5.8; wp-5.9.1 %1$s comentario %1$s comentarios %d pedido ha sido procesado y marcado como pagado. Esta página se recargará para mostrar la información del pedido actualizada. %s no puede estar vacío. %s está en un formato incorrecto. '%s' o '%s' son códigos numéricos ISO 4217 no válidos. <p>¡Muchas gracias por tu pedido! Pronto nos pondremos en contacto contigo por email para confirmar el pedido y para proporcionarte detalles sobre el pago.</p> Añadir al carrito Dirección Dirección, línea 1 Dirección, línea 2 Estado de trasanciones Ocurrió un error procesando un pedido en tu página web con un pago mediante Stripe. Por favor, incluye la siguiente información cuando reportes el error: Ocurrió un error procesando tu pedido. Por favor, inténtalo de nuevo. Ocurrió un error, inténtalo de nuevo más tarde. Ocurrió un error inesperado al contactar con PayPal.com. Ocurrió un error inesperado al contactar con PayPal.com para registrar tu sitio para notificaciones de pago: %s Ocurrió un error inesperado al contactar con PayPal.com para crear tu pedido. Por favor, inténtalo de nuevo más tarde. Ocurrió un error inesperado al contactar con PayPal.com. Por favor, ponte en contacto con el prepietario del sitio para completar la compra. Esperando pago Calculando... Cancelar Página de cancelación Cancelado Cambia la cantidad para actualizar tu carrito. Comprobar Comprobar de nuevo Comprobar ahora Comprobar Stripe Comprobando... Pago Página de pago Ciudad Comentario Comentarios Seguir comprando Coste Coste (%s) No se pudo completar el pedido en PayPal No se pudo encontrar el pedido con referencia #%s No se pudo guardar el pedido: País Cupón Cupón de descuento El código del cupón debe ser único El código del cupón no debe estar en blanco Cliente Eliminar Descripción Descargas digitales Descargas digitales para el pedido #%s Página de descargas digitales Desactivar Descuento: Hecho ¡Hecho! Tienes una tarjeta de crédito válida (últimos 4 dígitos: %s). Descargar Envío económico Editar tarifa Vaciar carrito Envío urgente Falló El formulario contiene errores, por favor, corrige todos los errores antes de continuar. El formulario contiene errores, por favor, corrige esos errores antes de continuar. Fraude Gratis Envío gratuito Nombre completo Imagen Envío internacional (Impresiones) El país seleccionado no es válido, por favor, vuelve a intentarlo. Cupón no válido Petición no válida Código postal incorrecto. Artículo Hacer un comentario Baja, Media, o Alta Resolución Editar cupons Editar galeries Editar precio Editar precios Impresiones metálicas Impresiones montadas Nombre Nombre: Nuevo pedido Nueva tarifa No se seleccionaron imágenes No se encontraron pedidos Mensaje no encontrado No se encontró nada ¡Ups! Esta página suele mostrar detalles de la compra de imágenes, pero no compraste ninguna imagen todavía. Por favor, siéntete libre de seguir buscando. Gracias por visitarnos. Opcional Orden Pedido cancelado Detalles del pedido Estado Pedido de %s Pedido enviado a PayPla para ser procesado Pedido enviado a Stripe para ser procesado Ventas Pagado Paga con cheque Pagar con tarjeta Paga con PayPal Sistema de pago Teléfono Realizar pedido Por favor, indica una dirección Por favor, indica un código postal válido Por favor, indica un nombre y un e-mail Código postal Precio Precio (%s) Precio: Elige tarifa Elige tarifas Impresiones Envío prioritario Procesando Procesando... Producto Cantidad Eliminar Eliminar tarjeta Responder Obligatorio Resolución Guardar Guardando... Buscar ventas Buscar palabra Seleccionar país Seleccionar provincia Enviar Enviar a: Enviar mediante Envío Información de envío Gastos de envío: Error en la página mientras se procesaba un pedido de NextGen Pro mediante Stripe Lo sentimos, esta imagen no está a la venta en este momento. Envío estándar País Provincia / Estado Estado: 404 Imagen no encontrada Asunto Enviado Enviado a PayPal Enviando {0} image{1} Enviando... Subtotal antes del descuento: Impuestos: Impuestos Gracias por tu pedido, %%customer_name%%.

Pediste %%item_count%% artículos, y se te ha cobrado en total %%total_amount%%.

Para revisar tu pedido, por favor, visita la %%order_details_page%%.

Gracias por comprar en %%site_url%%! Gracias por tu pedido, [customer_name]. Encargaste los siguientes artículos:
                [items]

                <h3>Detalles del pedido</h3>
                <p>
                Subtotal: [subtotal_amount]<br/>
                [if_used_coupon]Descuento: [discount_amount]<br/>[/if_used_coupon]
                [if_ordered_shippable_items]Envío: [shipping_amount]<br/>[/if_ordered_shippable_items]
                [if_has_tax]Impuestos: [tax_amount]<br/>[/if_has_tax]
                Total: [total_amount]<br/>
                </p>

                [if_ordered_shippable_items]
                <p>
                Enviaremos tus productos a:<br/>
                [shipping_street_address]<br/>
                [if_shipping_has_address_line]
                    [shipping_address_line]<br/>
                [/if_shipping_has_address_line]

                [shipping_city], [shipping_state] [shipping_zip]<br/>
                [shipping_country]
                </p>
                [/if_ordered_shippable_items]

                [if_ordered_digital_downloads]
                <h3>Descargas digitales</h3>
                <p>Puedes descargar tus productos <a href='[digital_downloads_page_url]'>aquí.</a></p>
                [/if_ordered_digital_downloads]
             ¡Gracias por tu pedido! Página de agradecimiento Gracias El total cobrado y el total recibido no coinciden La tarjeta de crédito que indicaste (últimos cuatro dígitos %s) expiró. El cliente [customer_name] pidió los siguientes artículos:
            [items]

            <h3>Detalles del pedido</h3>
            <p>
            Subtotal: [subtotal_amount]<br/>
            [if_used_coupon]Descuento: [discount_amount]<br/>[/if_used_coupon]
            [if_ordered_shippable_items]Envío: [shipping_amount]<br/>[/if_ordered_shippable_items]
            [if_has_tax]Impuestos: [tax_amount]<br/>[/if_has_tax]
            Total: [total_amount]<br/>
            [if_ordered_printlab_items]
                Printlab Status: [printlab_status_full]</br>
            [/if_ordered_printlab_items]
            [if_printlab_order_success]
                Precio de coste: [cost_of_goods]<br/>
                [if_printlab_pdf_invoice_link]
                    Factura: <a target='_blank' href='[printlab_pdf_invoice_link]'>Ver PDF</a></br>
                [/if_printlab_pdf_invoice_link]
            [/if_printlab_order_success]
            </p>

            [if_ordered_shippable_items]
            <p>
            Enviaremos tus artículos a:<br/>
            [shipping_street_address]<br/>
            [if_shipping_has_address_line]
            [shipping_address_line]<br/>
            [/if_shipping_has_address_line]
            [shipping_city], [shipping_state] [shipping_zip]<br/>
            [shipping_country]<br/>
            [if_shipping_has_phone]
            Tel: [shipping_phone]
            [/if_shipping_has_phone]
            </p>
            [/if_ordered_shippable_items]

            [if_ordered_digital_downloads]
            <h3>Descargas digitales</h3>
            <p>Enlace de descarga para los productos digitales: <a href='[digital_downloads_page_url]'>aquí.</a></p>
            [/if_ordered_digital_downloads]
         El pedido fue enviado a PayPal; estamos esperando una respuesta de PayPal para indicar que el pago se efectuó. No se añadió ningún artículo a tu carrito. Ocurrió un problema consultando el estado de tu pedido. Por favor, inténtalo más tarde. Ocurrió un problema intentando eliminar tu tarjeta Este campo debe ser único, distinto a cualquier otro código de cupón La imagen no está en venta Este pedido ha sido marcado como fraudulento y ha sido reportado. Total Desconocido Pendiente de pago Actualizar carrito Actualizar envío e impuestos Confirmar usuario Verificando pedido Ver carrito / Pagar Ver pedido Ver ventas No podemos encontrar tu pedido. Sentimos los inconvenientes Todavía no hemos recibido la confirmación del pago. Esto puedo tomar algunos minutos. Por favor, espera... Lo sentimos, pero uno o más artículos de los que has seleccionado no se pueden enviar a ese país. Lo sentimos, pero ocurrió un error procesando tu pedido. Por favor, inténtalo de nuevo. Lo sentimos, pero no podemos encontrar tu pedido. Lo sentimos, pero no hemos podido guardar la información de tu tarjeta de crédito. Por favor, revisa la información de tu tarjeta y vuélvelo a intentar. Estamos verificando tu pedido. Esto puede tomar algunos minutos. Debes <a href="%s" id="nggpl-comment-logout">iniciar sesión</a> para publicar un comentario. Debes indicar una ciudad para el envío. Debes indicar un país para el envío. Debes indicar un nombre para el envío. Debes indicar una provincia para el envío. Debes indicar una dirección postal para el envío. Debes indicar un código postal válido para el envío. Debes indicar una dirección de email por si ocurre un problema con la gestión de tu pedido. Tu pedido fue cancelado. Recibiste un pago de %%total_amount%% de %%customer_name%% (%%email%%). Para más detalles, visita: %%order_details_page%%

%%gateway_admin_note%%

Aquí tienes una lista de nombres de imagen separada por comas. Puedes copiar y pegar esta lista en tu programa de gestión de imágenes preferido para buscar con facilidad y encontrar las imágenes seleccionadas.

Imágenes: %%file_list%% Tu tarjeta ha sido eliminada Tu carrito contiene también %d artículos gratuitos Tu carrito contiene también 1 artículo gratuito Tu carrito ha sido actualizado Tu carrito está vacío Tu comentario está pendiente de moderación. Tu pedido fue cancelado. Código postal Código postal segundos ver instrucciones 