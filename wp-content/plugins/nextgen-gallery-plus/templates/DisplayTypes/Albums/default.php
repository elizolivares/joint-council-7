<?php
/**
 * Shared template for list and grid albums.
 *
 * @var \Imagely\NGG\DataTypes\DisplayedGallery $displayed_gallery
 * @var array $entities
 * @var bool $open_gallery_in_lightbox
 * @var string $css_class
 * @var string $id
 * @var string $thumbnail_size_name
 * @package NextGEN Pro
 */

?>
<?php $this->start_element( 'nextgen_gallery.gallery_container', 'container', $displayed_gallery ); ?>
	<div class="ngg-pro-album <?php echo esc_attr( $css_class ); ?>" id="<?php echo esc_attr( $id ); ?>">
		<?php foreach ( $entities as $entity ) { ?>
			<div class='image_container'>
				<div class='image_link_wrapper'>
					<?php
					if ( $open_gallery_in_lightbox && 'gallery' === $entity->entity_type ) {
						$anchor = $entity->displayed_gallery->effect_code . "
                              href='" . esc_attr( $entity->link ) . "'
                              title='" . esc_attr( $entity->galdesc ) . "'
                              data-src='" . esc_attr( $entity->previewpic_image_url ) . "'
                              data-fullsize='" . esc_attr( $entity->previewpic_image_url ) . "'
                              data-thumbnail='" . esc_attr( $entity->previewpic_thumbnail_url ) . "'
                              data-title='" . esc_attr( $entity->previewpic_image->alttext ) . "'
                              data-description='" . esc_attr( stripslashes( $entity->previewpic_image->description ) ) . "'
                              data-image-id='" . esc_attr( $entity->previewpic ) . "'";
					} else {
						$anchor = "href='" . esc_attr( $entity->link )
									. "'title='" . esc_attr( $entity->galdesc ) . "'";
					}
					?>
					<?php $this->start_element( 'nextgen_gallery.image', 'item', $entity ); ?>
						<span class="gallery_link">
							<a <?php echo $anchor; ?>>
								<?php
								\Imagely\NGGPro\Display\HiDPI::render_picture_element(
									$entity->previewpic,
									$thumbnail_size_name,
									[ 'class' => 'gallery_preview' ]
								);
								?>
							</a>
						</span>
					<?php $this->end_element(); ?>
					<span class="caption_link">
						<a <?php echo $anchor; ?>>
							<?php print wp_kses( $entity->title, \Imagely\NGG\Display\I18N::get_kses_allowed_html() ); ?>
						</a>
					</span>
					<div class="image_description">
						<?php print wp_kses( nl2br( $entity->galdesc ?? '' ), \Imagely\NGG\Display\I18N::get_kses_allowed_html() ); ?>
					</div>
					<br class="clear"/>
				</div>
			</div>
		<?php } ?>
	</div>
<?php $this->end_element(); ?>
