<?php
/**
 * @var DisplayedGallery $displayed_gallery
 */
use Imagely\NGG\DataTypes\DisplayedGallery;
?>
<?php $this->start_element( 'nextgen_gallery.gallery_container', 'container', $displayed_gallery ); ?>
<div class="ngg-pro-mosaic-container" data-ngg-pro-mosaic-id="<?php echo $displayed_gallery->id(); ?>"></div>
<?php $this->end_element(); ?>