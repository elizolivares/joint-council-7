<?php
/**
 * @var string $border_color
 * @var int $border_size
 * @var string $id
 * @var int $spacing
 */
?>
<style>
	<?php if ( $border_size ) { ?>
		#<?php echo $id; ?> img { border: solid <?php echo $border_size; ?>px <?php echo $border_color; ?>; }
	<?php } else { ?>
		#<?php echo $id; ?> img { border: none; }
	<?php } ?>

	#<?php echo $id; ?> .image-wrapper {
		margin: <?php echo $spacing; ?>px;
	}
</style>