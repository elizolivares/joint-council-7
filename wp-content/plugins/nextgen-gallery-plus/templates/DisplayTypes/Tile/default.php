<?php

/**
 * Template for the Tile display type.
 *
 * @var \Imagely\NGG\DataStorage\Manager $storage
 * @var \Imagely\NGG\DataTypes\DisplayedGallery $displayed_gallery
 * @var array $images
 * @var array $rows
 * @var string $contentWidth
 * @var string $effect_code
 * @var string $type
 * @package NextGEN Pro
 */

use Imagely\NGGPro\Display\View;

$this->start_element( 'nextgen_gallery.gallery_container', 'container', $displayed_gallery ); ?>
	<div class="tiled-gallery type-<?php print $type; ?> tiled-gallery-unresized"
		data-original-width="<?php print esc_attr( $contentWidth ); ?>"
		itemscope itemtype="http://schema.org/ImageGallery">
		<?php $this->start_element( 'nextgen_gallery.image_list_container', 'container', $images ); ?>
			<?php
			$view = new View(
				'DisplayTypes/Tile/' . $type . '-layout',
				[
					'rows'                   => $rows,
					'effect_code'            => $effect_code,
					'storage'                => $storage,
					'displayed_gallery'      => $displayed_gallery,
					'display_type_rendering' => true,
				],
				'photocrati-nextgen_pro_tile#/' . $type . '-layout'
			);
			$view->render( false );
			?>
		<?php $this->end_element(); ?>
	</div>
<?php $this->end_element(); ?>