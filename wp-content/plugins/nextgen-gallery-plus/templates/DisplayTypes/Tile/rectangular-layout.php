<?php

/**
 * Template for the Tile display type.
 *
 * @var \Imagely\NGG\DataStorage\Manager $storage
 * @var \Imagely\NGG\DataTypes\DisplayedGallery $displayed_gallery
 * @var array $rows
 * @var string $effect_code
 * @package NextGEN Pro
 */

use Imagely\NGGPro\Display\Animations\Manager as Animations;

use Imagely\NGGPro\Display\View;
use Imagely\NGG\Settings\Settings;

$settings = Settings::get_instance();

foreach ( $rows as $row ) { ?>
	<div class="gallery-row"
		style="width: <?php print esc_attr( $row->width ); ?>px;
				height: <?php print esc_attr( $row->height ); ?>px;"
		data-original-width="<?php print esc_attr( $row->width ); ?>"
		data-original-height="<?php print esc_attr( $row->height ); ?>">
		<?php foreach ( $row->groups as $group ) { ?>
			<div class="gallery-group images-<?php print esc_attr( count( $group->images ) ); ?>"
				style="width: <?php print esc_attr( $group->width ); ?>px;
						height: <?php print esc_attr( $group->height ); ?>px;"
				data-original-width="<?php print esc_attr( $group->width ); ?>"
				data-original-height="<?php print esc_attr( $group->height ); ?>">
				<?php
				// $counter is used to prevent animations from applying to every image in this gallery.
				$counter = 0;
				foreach ( $group->items() as $item ) {
					$params = [
						'item'                   => $item,
						'effect_code'            => $effect_code,
						'storage'                => $storage,
						'displayed_gallery'      => $displayed_gallery,
						'display_type_rendering' => true,
					];
					$view   = new View(
						'DisplayTypes/Tile/individual-image',
						$params,
						'photocrati-nextgen_pro_tile#individual-image'
					);
					$view->render( false );

					// We don't want the entire gallery to animate, only the first image.
					if ( 0 === $counter && $settings->get( 'animate_images_enable', false ) ) {
						Animations::deregister_renderer();
					}
					++$counter;
				}
				?>
			</div>
		<?php } ?>
	</div>
	<?php
}

// Finally: restore the animations hook which was previously disabled after the first image was rendered.
if ( $settings->get( 'animate_images_enable', false ) ) {
	Animations::register_renderer();
}
