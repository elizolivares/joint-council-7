<?php

namespace Imagely\NGGPro\License;

use Imagely\NGGPro\Install\SettingsInstaller;
class Installer extends SettingsInstaller
{
    function __construct()
    {
        $this->set_defaults(['imagely_license_key' => '']);
        $this->set_groups(['']);
    }
}