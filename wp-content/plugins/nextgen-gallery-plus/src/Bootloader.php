<?php

namespace Imagely\NGGPro;

class Bootloader
{
    public static $minimum_ngg_version = '3.56';
    public static $plugin_directory_root;
    public static $plugin_filename;
    public static $plugin_id;
    public static $plugin_version;
    public static $script_version;
    /**
     * @param string $plugin_version
     * @param string $plugin_filename
     * @param string $plugin_id
     */
    public function __construct($plugin_version, $plugin_filename, $plugin_id)
    {
        self::$plugin_version = $plugin_version;
        self::$plugin_filename = $plugin_filename;
        self::$plugin_id = $plugin_id;
        self::$plugin_directory_root = dirname(self::$plugin_filename) . DIRECTORY_SEPARATOR;
        self::$script_version = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? (string) time() : self::$plugin_version;
        // Another copy of this plugin or one of its siblings has already been loaded -- do not continue.
        if (defined('NGG_PRO_PLUGIN_VERSION') || defined('NGG_PLUS_PLUGIN_VERSION') || defined('NGG_STARTER_PLUGIN_VERSION')) {
            return;
        }
        \add_filter('ngg_do_install_or_setup_process', function ($do_upgrade) {
            $settings = \Imagely\NGG\Settings\Settings::get_instance();
            $current_version_setting = $settings->get('ngg_' . self::$plugin_id . '_plugin_version', 0);
            if (!$current_version_setting || $current_version_setting !== self::$plugin_version) {
                $do_upgrade = true;
            }
            return $do_upgrade;
        });
        \add_action('ngg_did_install_or_setup_process', function () {
            // Record the current plugin version; changes to this and setting are how updates are triggered.
            $settings = \Imagely\NGG\Settings\Settings::get_instance();
            $settings->set('ngg_' . self::$plugin_id . '_plugin_version', self::$plugin_version);
            $settings->save();
            $over_time = get_option('nextgen_over_time', []);
            if (empty($over_time['installed_pro'])) {
                $over_time['installed_version'] = self::$plugin_version;
                $over_time['installed_date'] = wp_date('U');
                $over_time['installed_pro'] = $over_time['installed_date'];
                if (!isset($over_time['installed_lite'])) {
                    $over_time['installed_lite'] = false;
                }
                update_option('nextgen_over_time', $over_time);
            }
        });
        // Deactivates this plugin and displays a notification if NextGEN is missing or incompatible.
        \add_action('admin_notices', [$this, 'admin_notices']);
        // The base plugin constants must be defined now so that if NextGEN is activated before this plugin is, then
        // these plugins' constants will be visible to NextGEN.
        $this->define_base_constants();
        // This plugin depends on NextGEN Gallery. If NextGEN is not yet active, this plugin delays initialization until
        // NextGEN Gallery has initialized itself.
        if (defined('NGG_PLUGIN_VERSION')) {
            $this->load_plugin();
        } else {
            add_action('ngg_initialized', [$this, 'load_plugin']);
        }
    }
    public static function get_plugin_name()
    {
        if ('pro' === self::$plugin_id) {
            return __('NextGEN Gallery Pro', 'nextgen-gallery-pro');
        } elseif ('plus' === self::$plugin_id) {
            return __('NextGEN Gallery Plus', 'nextgen-gallery-pro');
        } elseif ('starter' === self::$plugin_id) {
            return __('NextGEN Gallery Starter', 'nextgen-gallery-pro');
        }
    }
    public function load_plugin()
    {
        // Version mismatch: do not load any further.
        if (!defined('NGG_PLUGIN_VERSION') || version_compare(NGG_PLUGIN_VERSION, self::$minimum_ngg_version) == -1) {
            return;
        }
        // Register our and Composer's autoloader
        spl_autoload_register([$this, 'autoloader']);
        require_once self::$plugin_directory_root . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
        // This file contains empty wrappers to the new namespaced classes and methods to maintain compat for custom templates.
        include_once 'CompatibilityStims.php';
        // Begin actual initialization
        $this->register_hooks();
        if (is_admin()) {
            $this->load_pope_product();
        }
    }
    /**
     * Define the base constants for the current product.
     */
    public function define_base_constants()
    {
        /**
         * @remove-for-nextgen-pro
         * @remove-for-nextgen-starter
         */
        if ('plus' === self::$plugin_id) {
            $this->define_plus_constants();
        }
        $this->define_constants();
    }
    public function define_constants()
    {
        define('NGG_PRO_PLUGIN_DIR', plugin_dir_path(self::$plugin_filename));
        // Used by NextGEN itself to determine what features to load, offer up, disable, or patch.
        define('NGG_PRO_API_VERSION', 4.0);
        // Display type ID
        define('NGG_PRO_ALBUMS', 'photocrati-nextgen_pro_albums');
        define('NGG_PRO_BLOG_GALLERY', 'photocrati-nextgen_pro_blog_gallery');
        define('NGG_PRO_FILM', 'photocrati-nextgen_pro_film');
        define('NGG_PRO_GALLERIA', 'photocrati-galleria');
        define('NGG_PRO_GRID_ALBUM', 'photocrati-nextgen_pro_grid_album');
        define('NGG_PRO_HORIZONTAL_FILMSTRIP', 'photocrati-nextgen_pro_horizontal_filmstrip');
        define('NGG_PRO_IMAGEBROWSER', 'photocrati-nextgen_pro_imagebrowser');
        define('NGG_PRO_LIST_ALBUM', 'photocrati-nextgen_pro_list_album');
        define('NGG_PRO_MASONRY', 'photocrati-nextgen_pro_masonry');
        define('NGG_PRO_MOSAIC', 'photocrati-nextgen_pro_mosaic');
        define('NGG_PRO_SEARCH', 'imagely-pro-search');
        define('NGG_PRO_SIDESCROLL', 'photocrati-nextgen_pro_sidescroll');
        define('NGG_PRO_SLIDESHOW', 'photocrati-nextgen_pro_slideshow');
        define('NGG_PRO_THUMBNAIL_GRID', 'photocrati-nextgen_pro_thumbnail_grid');
        define('NGG_PRO_TILE', 'photocrati-nextgen_pro_tile');
        // Lightbox constants
        define('NGG_PRO_LIGHTBOX', 'photocrati-nextgen_pro_lightbox');
        define('NGG_PRO_LIGHTBOX_TRIGGER', NGG_PRO_LIGHTBOX);
        define('NGG_PRO_LIGHTBOX_COMMENT_TRIGGER', 'photocrati-nextgen_pro_lightbox_comments');
        define('NGG_PRO_LIGHTBOX_VERSION', '3.99.0');
    }
    /**
     * @remove-for-nextgen-pro
     * @remove-for-nextgen-starter
     */
    public function define_plus_constants()
    {
        if ('plus' !== self::$plugin_id) {
            return;
        }
        define('NGG_PLUS_PLUGIN_BASENAME', plugin_basename(self::$plugin_filename));
        define('NGG_PLUS_PLUGIN_VERSION', self::$plugin_version);
    }
    public function autoloader($class)
    {
        $prefix = 'Imagely\\NGGPro\\';
        $len = strlen($prefix);
        if (strncmp($prefix, $class, $len) !== 0) {
            return;
        }
        $relative_class = substr($class, $len);
        $file = self::$plugin_directory_root . 'src' . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $relative_class) . '.php';
        if (file_exists($file)) {
            require $file;
        }
    }
    /**
     * Loads the POPE product which provides admin pages and functions
     */
    public function load_pope_product()
    {
        $registry = \C_Component_Registry::get_instance();
        $registry->add_module_path(self::$plugin_directory_root . 'modules' . DIRECTORY_SEPARATOR, 2);
        $registry->load_product('photocrati-nextgen-pro');
        $registry->initialize_all_modules();
    }
    public function register_hooks()
    {
        add_action('init', [$this, 'load_i18n'], 2);
        \Imagely\NGGPro\DisplayType\Manager::register();
        /**
         * @remove-for-nextgen-starter
         */
        if ('starter' !== self::$plugin_id) {
            $this->register_pro_and_plus_hooks();
        }
        // When first downloaded from imagely.com or members.photocrati.com the plugin zip will contain a license.php
        // file in the root of the plugin directory. This lets the License Manager know where to find it.
        add_filter('photocrati_license_path_list', function ($path_list, $product) {
            $path_list[] = self::$plugin_directory_root;
            return $path_list;
        }, 10, 2);
        \Imagely\NGGPro\License\Manager::register_hooks();
        \Imagely\NGGPro\Display\HiDPI::register_hooks();
        (new \Imagely\NGGPro\Install\UsageTracking())->hooks();
    }
    /**
     * @remove-for-nextgen-starter
     */
    public function register_pro_and_plus_hooks()
    {
        if ('starter' === self::$plugin_id) {
            return;
        }
        // Provides default settings for commerce, proofing, and captions
        add_filter('ngg_datamapper_client_display_type', function ($className) {
            return '\\Imagely\\NGGPro\\DataMappers\\DisplayType';
        });
        add_action('rest_api_init', ['\\Imagely\\NGGPro\\REST\\Manager', 'rest_api_init']);
        \Imagely\NGGPro\DisplayType\Manager::register_hooks();
        \Imagely\NGGPro\Display\HoverCaptions::register_hooks();
        \Imagely\NGGPro\Display\ImageProtection\Manager::register_hooks();
        \Imagely\NGGPro\Lightbox\CommentsManager::register_hooks();
        \Imagely\NGGPro\Lightbox\Manager::register_hooks();
        \Imagely\NGGPro\Display\Animations\Manager::register_hooks();
    }
    /**
     * Registers the plugins textdomain for gettext translations.
     *
     * @return void
     */
    public function load_i18n()
    {
        $dir = basename(self::$plugin_directory_root) . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'I18N';
        load_plugin_textdomain('nextgen-gallery-pro', false, $dir);
    }
    public function admin_notices()
    {
        $nextgen_version = defined('NGG_PLUGIN_VERSION') ? NGG_PLUGIN_VERSION : null;
        if (!$nextgen_version) {
            $message = sprintf(__('Please install &amp; activate <a href="http://wordpress.org/plugins/nextgen-gallery/" target="_blank">NextGEN Gallery</a> to allow %s to work.', 'nextgen-gallery-pro'), self::get_plugin_name());
            echo '<div class="updated"><p>' . $message . '</p></div>';
        } elseif (version_compare($nextgen_version, self::$minimum_ngg_version) == -1) {
            $upgrade_url = \admin_url('/plugin-install.php?tab=plugin-information&plugin=nextgen-gallery&section=changelog&TB_iframe=true&width=640&height=250');
            $message = sprintf(__("NextGEN Gallery %1\$s is incompatible with %2\$s %3\$s. Please update <a class='thickbox' href='%4\$s'>NextGEN Gallery</a> to version %5\$s or higher. %6\$s has been deactivated.", 'nextgen-gallery-pro'), $nextgen_version, self::get_plugin_name(), self::$plugin_version, $upgrade_url, self::$minimum_ngg_version, self::get_plugin_name());
            echo '<div class="updated"><p>' . $message . '</p></div>';
            \deactivate_plugins(\plugin_basename(self::$plugin_filename));
        } elseif (\delete_option('photocrati_pro_recently_activated')) {
            $message = __('To activate the NextGEN Pro Lightbox please go to Gallery > Other Options > Lightbox Effects.', 'nextgen-gallery-pro');
            echo '<div class="updated"><p>' . $message . '</p></div>';
        }
    }
}