<?php

/**
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\Display;

use Imagely\NGG\DataTypes\DisplayedGallery;
use Imagely\NGG\Display\DisplayManager;
class HoverCaptions
{
    static $_galleries_displayed = [];
    static $_pro_captions_run_once = false;
    public static function register_hooks()
    {
        $self = new self();
        \add_action('wp_enqueue_scripts', [$self, 'register_captions'], -250);
        \add_filter('ngg_effect_code', [$self, 'get_effect_code'], 10, 2);
        \add_action('ngg_display_type_controller_enqueue_frontend_resources', [$self, 'enqueue_frontend_resources']);
    }
    public static function get_supported_display_types()
    {
        return ['photocrati-nextgen_pro_blog_gallery', 'photocrati-nextgen_pro_film', 'photocrati-nextgen_pro_masonry', 'photocrati-nextgen_pro_mosaic', 'photocrati-nextgen_pro_sidescroll', 'photocrati-nextgen_pro_thumbnail_grid', 'photocrati-nextgen_pro_tile'];
    }
    /**
     * @param string           $retval
     * @param DisplayedGallery $displayed_gallery
     */
    public function get_effect_code($retval, $displayed_gallery)
    {
        if (isset($displayed_gallery->display_settings['captions_enabled']) && $displayed_gallery->display_settings['captions_enabled']) {
            $retval .= ' data-ngg-captions-enabled="1" data-ngg-captions-id="' . $displayed_gallery->id() . '"';
        }
        return $retval;
    }
    /**
     * @param DisplayedGallery $displayed_gallery
     */
    public function enqueue_frontend_resources($displayed_gallery)
    {
        if (!in_array($displayed_gallery->id(), self::$_galleries_displayed)) {
            self::$_galleries_displayed[] = $displayed_gallery->id();
            $ds = $displayed_gallery->display_settings;
            if (!empty($ds['captions_enabled'])) {
                DisplayManager::add_script_data('ngg_common', 'galleries.gallery_' . $displayed_gallery->id() . '.captions_enabled', true, false);
                $animation = !empty($ds['captions_animation']) ? $ds['captions_animation'] : 'slideup';
                DisplayManager::add_script_data('ngg_common', 'galleries.gallery_' . $displayed_gallery->id() . '.captions_animation', $animation, false);
                $show_title = !empty($ds['captions_display_title']) ? $ds['captions_display_title'] : true;
                DisplayManager::add_script_data('ngg_common', 'galleries.gallery_' . $displayed_gallery->id() . '.captions_display_title', $show_title, false);
                $show_description = !empty($ds['captions_display_description']) ? $ds['captions_display_description'] : true;
                DisplayManager::add_script_data('ngg_common', 'galleries.gallery_' . $displayed_gallery->id() . '.captions_display_description', $show_description, false);
            } else {
                DisplayManager::add_script_data('ngg_common', 'galleries.gallery_' . $displayed_gallery->id() . '.captions_enabled', false, false);
            }
        }
        if (isset($displayed_gallery->display_settings['captions_enabled']) && $displayed_gallery->display_settings['captions_enabled'] && !self::$_pro_captions_run_once) {
            \wp_enqueue_script('nextgen_pro_captions-js');
            \wp_enqueue_style('nextgen_pro_captions-css');
            self::$_pro_captions_run_once = true;
        }
    }
    public function register_captions()
    {
        \wp_register_script('nextgen_pro_captions_imagesloaded', StaticAssets::get_url('Display/HoverCaptions/imagesloaded.min.js', 'photocrati-nextgen_pro_captions#imagesloaded.min.js'), ['jquery']);
        wp_register_script('nextgen_pro_captions-js', StaticAssets::get_url('Display/HoverCaptions/main.js', 'photocrati-nextgen_pro_captions#captions.js'), ['underscore', 'shave.js', 'nextgen_pro_captions_imagesloaded'], false, true);
        \wp_register_style('nextgen_pro_captions-css', StaticAssets::get_url('Display/HoverCaptions/style.css', 'photocrati-nextgen_pro_captions#captions.css'));
    }
}