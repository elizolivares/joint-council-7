<?php

/**
 * Adds default settings for the animations feature.
 *
 * @package NextGEN Pro
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\Display\Animations;

use Imagely\NGGPro\Install\SettingsInstaller;
/**
 * Provide default settings for the image animations feature.
 */
class Installer extends SettingsInstaller
{
    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->set_defaults(['animate_images_enable' => 0, 'animate_images_style' => 'wobble', 'animate_images_duration' => 1500, 'animate_images_delay' => 250, 'animate_pagination_enable' => 0, 'animate_pagination_style' => 'flipInX', 'animate_pagination_duration' => 1500, 'animate_pagination_delay' => 250]);
        $this->set_groups(['']);
    }
}