<?php

namespace Imagely\NGGPro\Display;

use Imagely\NGG\Display\View as ParentClass;
class View extends ParentClass
{
    public static $default_root_dir = NGG_PRO_PLUGIN_DIR;
}