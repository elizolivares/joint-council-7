<?php

namespace Imagely\NGGPro\Display;

use Imagely\NGG\Display\StaticAssets as ParentClass;
class StaticAssets extends ParentClass
{
    public static $default_plugin_root = NGG_PRO_PLUGIN_DIR;
    public static $new_override_path_name = 'nextgen-gallery-pro-static-overrides';
}