<?php

/**
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\Display\ImageProtection;

use Imagely\NGG\Util\Installer as InstallManager;
use Imagely\NGG\DataStorage\Manager as StorageManager;
use Imagely\NGG\Settings\Settings;
use Imagely\NGG\Util\Router;
use Imagely\NGGPro\Display\StaticAssets;
class Manager
{
    public static function register_hooks()
    {
        $self = new self();
        add_action('wp_enqueue_scripts', [$self, 'register_protection_js']);
        add_action('ngg_created_new_gallery', [$self, 'protect_gallery']);
        add_filter('ngg_pro_settings_reset_installers', [$self, 'return_own_installer']);
        add_filter('ngg_effect_code', [$self, 'get_effect_code'], 10, 2);
        add_action('init', function () {
            InstallManager::add_handler('photocrati-image_protection', '\\Imagely\\NGGPro\\Display\\ImageProtection\\Installer');
        });
    }
    public function get_effect_code($retval, $displayed_gallery)
    {
        if (Settings::get_instance()->get('protect_images')) {
            $retval .= ' data-ngg-protect="1"';
        }
        return $retval;
    }
    public function get_protector_list()
    {
        return ['apache-config' => ['path' => '.htaccess', 'content' => '
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteRule _backup$ - [L,F]
</IfModule>' . "\n", 'tag-start' => '# BEGIN NextGEN Pro Protection' . "\n", 'tag-end' => '# END NextGEN Pro Protection' . "\n"], 'php-index' => ['path' => 'index.php', 'content' => '// silence is golden' . "\n", 'tag-start' => '<?php # BEGIN NextGEN Pro Protection' . "\n", 'tag-end' => '# END NextGEN Pro Protection' . "\n"]];
    }
    public function find_protector_content($text, $protector)
    {
        $tag_start = $protector['tag-start'];
        $tag_end = $protector['tag-end'];
        $pos_1 = strpos($text, $tag_start);
        $len_1 = strlen($tag_start);
        if ($pos_1 !== false) {
            $start = $pos_1 + $len_1;
            $pos_2 = strpos($text, $tag_end, $start);
            $len_2 = strlen($tag_end);
            if ($pos_2 !== false) {
                $content = substr($text, $start, $pos_2 - $start);
                return ['content' => $content, 'start' => $start, 'end' => $pos_2, 'size' => $pos_2 - $start];
            }
        }
        return false;
    }
    public function is_gallery_protected($gallery)
    {
        $storage = StorageManager::get_instance();
        $gallery_path = $storage->get_gallery_abspath($gallery);
        if ($gallery_path != null && file_exists($gallery_path)) {
            $protector_files = $this->get_protector_list();
            $retval = false;
            foreach ($protector_files as $name => $protector) {
                $path = $protector['path'];
                $full_path = path_join($gallery_path, $path);
                $retval = false;
                if (file_exists($full_path)) {
                    $full = file_get_contents($full_path);
                    $result = $this->find_protector_content($full, $protector);
                    if ($result != null && $result['content'] == $protector['content']) {
                        $retval = true;
                    }
                }
                if (!$retval) {
                    break;
                }
            }
            return $retval;
        }
        return false;
    }
    public function protect_gallery($gallery, $force = false)
    {
        $retval = $this->is_gallery_protected($gallery);
        if ($force || !$retval) {
            $storage = StorageManager::get_instance();
            $gallery_path = $storage->get_gallery_abspath($gallery);
            if ($gallery_path != null && file_exists($gallery_path)) {
                $protector_files = $this->get_protector_list();
                foreach ($protector_files as $name => $protector) {
                    $path = $protector['path'];
                    $full_path = \path_join($gallery_path, $path);
                    $full = null;
                    if (file_exists($full_path)) {
                        $full = @file_get_contents($full_path);
                        $result = $this->find_protector_content($full, $protector);
                        if ($result != null) {
                            $full = substr_replace($full, $protector['content'], $result['start'], $result['size']);
                        }
                    } else {
                        $full = $protector['tag-start'] . $protector['content'] . $protector['tag-end'];
                    }
                    if (is_writable($full_path)) {
                        @file_put_contents($full_path, $full);
                    }
                    $retval = true;
                }
            }
        }
        return $retval;
    }
    public function register_protection_js()
    {
        if (!is_admin() && Settings::get_instance()->get('protect_images')) {
            $router = Router::get_instance();
            $handle = 'pressure';
            $do_register = true;
            if (\wp_script_is('pressure', 'registered')) {
                $do_register = false;
            } elseif (\wp_script_is('pressurejs', 'registered')) {
                $handle = 'pressurejs';
                $do_register = false;
            }
            if ($do_register) {
                \wp_register_script($handle, StaticAssets::get_url('Display/ImageProtection/pressure.js', 'photocrati-image_protection#pressure.js'), ['jquery']);
            }
            \wp_register_script('photocrati-image_protection-js', StaticAssets::get_url('Display/ImageProtection/main.js', 'photocrati-image_protection#custom.js'), ['jquery', $handle], '2.2.0');
            \wp_enqueue_script('photocrati-image_protection-js');
            \wp_enqueue_style('photocrati-image_protection-css', StaticAssets::get_url('Display/ImageProtection/style.css', 'photocrati-image_protection#custom.css'));
            \wp_localize_script('photocrati-image_protection-js', 'photocrati_image_protection_global', ['enabled' => Settings::get_instance()->get('protect_images_globally')]);
        }
    }
    public function return_own_installer($installers)
    {
        $installers[] = '\\Imagely\\NGGPro\\Display\\ImageProtection\\Installer';
        return $installers;
    }
}