<?php

/**
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\Display\ImageProtection;

use Imagely\NGGPro\Install\SettingsInstaller;
class Installer extends SettingsInstaller
{
    function __construct()
    {
        $this->set_defaults(['protect_images' => 0, 'protect_images_globally' => 0]);
        $this->set_groups(['']);
    }
}