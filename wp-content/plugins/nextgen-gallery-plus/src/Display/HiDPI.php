<?php

namespace Imagely\NGGPro\Display;

use Imagely\NGG\DataMappers\Image as ImageMapper;
use Imagely\NGG\DataStorage\Manager as StorageManager;
use Imagely\NGG\DynamicThumbnails\Manager as DynamicThumbnailsManager;
use Imagely\NGG\DataTypes\Image;
use Imagely\NGG\Settings\Settings;
class HiDPI
{
    public static function register_hooks()
    {
        $self = new self();
        \add_action('ngg_generated_image', [$self, 'generate_image_size'], 10, 3);
    }
    /**
     * @param Image      $image
     * @param string     $named_size
     * @param null|array $params
     * @return void
     */
    public function generate_image_size($image, $named_size, $params = null)
    {
        // Avoid creating an infinite loop!
        $size = self::get_hidpi_named_size($image, $named_size);
        if ($size == $named_size) {
            return;
        }
        $manager = StorageManager::get_instance();
        $hidpi_image = $manager->generate_image_size($image, $size);
        $hidpi_image->destruct();
    }
    public static function render_picture_source($image, $named_size, $hidpi_size_name = null)
    {
        $storage = StorageManager::get_instance();
        $srcsets = str_replace(' ', '%20', [$storage->get_image_url($image, $named_size)]);
        if ($hidpi_size_name) {
            $srcsets[] = str_replace(' ', '%20', $storage->get_image_url($image, $hidpi_size_name)) . ' 2x';
        }
        return sprintf("<source srcset='%s'>", implode(", ", $srcsets));
    }
    /**
     * Return the named size for a HiDPI-version of another named size. E.g. if the named size that provided is
     * "thumbnails" this method will return the named size for the HiDPI-version of the thumbnail
     *
     * @param Image  $image
     * @param string $original_named_size
     * @return string
     */
    public static function get_hidpi_named_size($image, $original_named_size)
    {
        // Generate a named size for the 2x version
        $hidpi_named_size = $original_named_size;
        // Don't generate HiDPI image if "resize images upon upload" isn't enabled
        if (Settings::get_instance()->get('imgAutoResize', false)) {
            $storage = StorageManager::get_instance();
            $dynthumbs = DynamicThumbnailsManager::get_instance();
            // Copy the original image generation parameters, but double the image size
            if ($dynthumbs && $dynthumbs->is_size_dynamic($original_named_size)) {
                $hidpi_params = $dynthumbs->get_params_from_name($original_named_size, true);
            } else {
                $hidpi_params = $storage->get_image_size_params($image, $original_named_size, $storage->get_image_dimensions($image, $original_named_size));
            }
            if ($hidpi_params) {
                // We need to ensure that our original image is large enough to generate the HiDPI / 2x image.
                if (!empty($hidpi_params['width'])) {
                    $hidpi_params['width'] *= 2;
                }
                if (!empty($hidpi_params['height'])) {
                    $hidpi_params['height'] *= 2;
                }
                if (isset($hidpi_params['crop_frame'])) {
                    $hidpi_params['crop'] = true;
                }
                // If a backup exists, we'll generate from that
                if ($backup_abspath = $storage->get_image_abspath($image, 'backup', true)) {
                    $backup_dimensions = $storage->get_image_dimensions($image, 'backup');
                    if (!isset($backup_dimensions['width']) || !isset($backup_dimensions['height'])) {
                        $size = getimagesize($backup_abspath);
                        if (is_array($size) && isset($size[0]) && isset($size[1])) {
                            $backup_dimensions['width'] = $size[0];
                            $backup_dimensions['height'] = $size[1];
                        }
                    }
                    if (isset($backup_dimensions['width']) && isset($backup_dimensions['height']) && isset($hidpi_params['width']) && isset($hidpi_params['height'])) {
                        if ($hidpi_params['width'] >= $backup_dimensions['width'] || $hidpi_params['height'] >= $backup_dimensions['height']) {
                            $hidpi_params['width'] = $backup_dimensions['width'];
                            $hidpi_params['height'] = $backup_dimensions['height'];
                        }
                        $hidpi_named_size = $dynthumbs->get_size_name($hidpi_params);
                    } else {
                        $hidpi_named_size = 'full';
                    }
                } else {
                    $dimensions = $storage->get_image_dimensions($image);
                    if (isset($dimensions['width']) && isset($dimensions['height'])) {
                        if ($hidpi_params['width'] >= $dimensions['width'] || $hidpi_params['height'] >= $dimensions['height']) {
                            $hidpi_named_size = 'full';
                        } else {
                            $hidpi_named_size = $dynthumbs->get_size_name($hidpi_params);
                        }
                    }
                }
            }
        }
        return $hidpi_named_size;
    }
    /**
     * Renders a picture element for a particular image at named size.
     *
     * @param Image        $image
     * @param string|array $params_or_named_size
     * @param bool         $echo
     *
     * @return string
     */
    public static function render_picture_element($image, $params_or_named_size, $attrs = [], $echo = true)
    {
        $retval = '';
        if (!is_object($image)) {
            $image = ImageMapper::get_instance()->find($image);
        }
        if ($image) {
            $parts = self::prepare_picture_element($image, $params_or_named_size, $attrs);
            $retval = implode("\n", ['<picture>', "\t", implode("\n\t", $parts['sources']), "<img {$parts['attr_str']}/>", '</picture>']);
        }
        if ($echo) {
            echo $retval;
        }
        return $retval;
    }
    public static function has_dynamic_images($str)
    {
        return strpos($str, '/nextgen-image/') !== false;
    }
    public static function prepare_picture_element($image, $params_or_named_size, $attrs = [])
    {
        $retval = [];
        $dynthumbs = DynamicThumbnailsManager::get_instance();
        $storage = StorageManager::get_instance();
        $sources = [];
        $srcsets = [];
        $srcsets2 = [];
        // returned unprocessed, unlike srcsets above
        // Get the named size to display
        $named_size = $params_or_named_size;
        if (is_array($params_or_named_size)) {
            $named_size = $dynthumbs->get_size_name($params_or_named_size);
        }
        $url = $storage->get_image_url($image, $named_size, true);
        $image_url = str_replace(' ', '%20', $url);
        // Get HiDPI named size to display
        $hidpi_named_size = self::get_hidpi_named_size($image, $named_size);
        $hidpi_url = str_replace(' ', '%20', $storage->get_image_url($image, $hidpi_named_size));
        // Set attributes
        $srcsets[] = isset($_REQUEST['force_hidpi']) ? $hidpi_url : $image_url;
        $srcsets2['original'] = isset($_REQUEST['force_hidpi']) ? $hidpi_url : $image_url;
        $dimensions = $storage->get_image_dimensions($image, $named_size);
        if (!array_key_exists('title', $attrs)) {
            $attrs['title'] = $image->alttext;
        }
        if (!array_key_exists('alt', $attrs)) {
            $attrs['alt'] = $image->alttext;
        }
        if ($dimensions && isset($dimensions['width']) && isset($dimensions['height'])) {
            if (!array_key_exists('width', $attrs)) {
                $attrs['width'] = $dimensions['width'];
            }
            if (!array_key_exists('height', $attrs)) {
                $attrs['height'] = $dimensions['height'];
            }
            if (!array_key_exists('style', $attrs)) {
                $attrs['style'] = "max-width:{$attrs['width']}px;max-height:{$attrs['height']}px";
            }
        }
        // Add sources
        if ($hidpi_named_size != $named_size) {
            $sources[] = self::render_picture_source($image, $named_size, $hidpi_named_size);
            $srcsets[] = $hidpi_url . ' 2x';
            $srcsets2['hdpi'] = $hidpi_url;
        } else {
            $sources[] = self::render_picture_source($image, $named_size);
        }
        // Create attribute strings
        $attrs['src'] = $image_url;
        $attrs['srcset'] = implode(', ', $srcsets);
        $attr_str_array = [];
        foreach ($attrs as $key => $value) {
            if (!is_null($value)) {
                $attr_str_array[$key] = esc_attr($key) . '="' . esc_attr($value) . '"';
            }
        }
        if (self::has_dynamic_images($attrs['src']) || self::has_dynamic_images($attrs['srcset'])) {
            $attr_str_array['loading'] = 'loading="lazy"';
        }
        $attr_strs = implode(' ', $attr_str_array);
        $retval['use_hdpi'] = $hidpi_named_size != $named_size;
        $retval['srcsets'] = $srcsets;
        $retval['srcsets_unfiltered'] = $srcsets2;
        $retval['sources'] = $sources;
        $retval['attr_str'] = $attr_strs;
        $retval['attr_array'] = $attr_str_array;
        return $retval;
    }
}