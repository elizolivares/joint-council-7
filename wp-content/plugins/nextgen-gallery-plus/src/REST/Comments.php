<?php

/**
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\REST;

use Imagely\NGGPro\DataMappers\Comment as CommentMapper;
use Imagely\NGGPro\DataTypes\Comment as CommentType;
use Imagely\NGGPro\Display\View;
use WP_REST_Request;
class Comments
{
    /**
     * @param array           $retval
     * @param WP_REST_Request $request
     * @return array
     */
    public static function add_comments_data($retval, $request)
    {
        /** @var string|FALSE $lang */
        if ($lang = $request->get_param('lang') && class_exists('SitePress')) {
            global $sitepress;
            global $locale;
            $locale = $sitepress->get_locale($lang);
            $sitepress->switch_lang($lang);
            \remove_filter('locale', [$sitepress, 'locale']);
            \add_filter('locale', function () use($request, $sitepress) {
                return $sitepress->get_locale($request->get_param('lang'));
            }, -10);
            \load_textdomain('default', WP_LANG_DIR . DIRECTORY_SEPARATOR . $locale . '.mo');
        }
        ob_start();
        $mapper = CommentMapper::get_instance();
        \add_filter('comments_template', function ($template) {
            if (strpos($template, 'ngg_comments') !== false) {
                $view = new View('Comments/default');
                $template = $view->find_template_abspath('Comments/default');
            }
            return $template;
        });
        $ids = explode(',', $request->get_param('id'));
        $page = $request->get_param('page') ?: 0;
        $type = $request->get_param('type');
        foreach ($ids as $id) {
            /** @var CommentType $post */
            $comment = $mapper->find_or_create($type, $id, $request->get_param('from'));
            if ($comment) {
                $retval[$id]['comments'] = $comment->get_comments_data($page);
            }
        }
        ob_end_clean();
        return $retval;
    }
}