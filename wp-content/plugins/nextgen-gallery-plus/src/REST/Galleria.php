<?php

namespace Imagely\NGGPro\REST;

use Imagely\NGG\DataMappers\DisplayedGallery as DisplayedGalleryMapper;
use Imagely\NGG\DataTypes\DisplayedGallery;
use Imagely\NGG\Settings\Settings;
use Imagely\NGGPro\DisplayTypes\Galleria as GalleriaDisplayType;
use WP_REST_Request;
use WP_REST_Response;
class Galleria extends \WP_REST_Controller
{
    public function __construct()
    {
        $this->namespace = 'nggpro/v1';
        $this->rest_base = 'Galleria';
    }
    public function register_routes()
    {
        \register_rest_route($this->namespace, '/' . $this->rest_base . '/GetImages', ['args' => ['id' => ['type' => 'string|int', 'required' => true], 'gallery' => ['type' => 'object', 'required' => false], 'lang' => ['type' => 'string', 'required' => false], 'page' => ['type' => 'integer', 'required' => false]], ['methods' => \WP_REST_Server::CREATABLE, 'callback' => [$this, 'GetImages'], 'permission_callback' => '__return_true']]);
        \register_rest_route($this->namespace, '/' . $this->rest_base . '/GetSidebarData', ['args' => ['type' => ['type' => 'string', 'required' => true], 'id' => ['type' => 'string', 'required' => true], 'from' => ['type' => 'string', 'required' => true], 'page' => ['type' => 'integer', 'required' => false]], ['methods' => \WP_REST_Server::CREATABLE, 'callback' => [$this, 'GetSidebarData'], 'permission_callback' => '__return_true']]);
    }
    /**
     * @param WP_REST_Request $request
     * @return WP_REST_Response
     */
    public function GetImages($request)
    {
        $retval = [];
        $id = $request->get_param('id');
        $displayed_gallery_mapper = DisplayedGalleryMapper::get_instance();
        if ($request->get_param('lang') && class_exists('SitePress')) {
            global $sitepress;
            $sitepress->switch_lang($request->get_param('lang'));
        }
        // Fetch ATP galleries or build our displayed gallery by parameters
        /** @var DisplayedGallery $displayed_gallery */
        if (is_numeric($id)) {
            $displayed_gallery = $displayed_gallery_mapper->find($id, true);
        } else {
            $params = $request->get_param('gallery');
            $displayed_gallery = new DisplayedGallery((object) $params);
        }
        if ($displayed_gallery) {
            $settings = Settings::get_instance()->get('ngg_pro_lightbox');
            // we already have the first 'page' worth localized in the page HTML
            $page = $request->get_param('page') ?: 0;
            if ($displayed_gallery->is_album_gallery && $page == 0) {
                $page = 1;
            }
            $offset = $settings['localize_limit'] * ($page === 1 ? $page : $page - 1);
            $retval = GalleriaDisplayType::format_entities($displayed_gallery->get_entities($settings['localize_limit'], $offset));
        }
        $retval = \apply_filters('ngg_pro_lightbox_images_queue', $retval);
        return new WP_REST_Response($retval);
    }
    /**
     * @param WP_REST_Request $request
     * @return WP_REST_Response
     */
    public function GetSidebarData($request)
    {
        $retval = \apply_filters('ngg_pro_lightbox_sidebar_data_request', [], $request);
        return new WP_REST_Response($retval);
    }
}