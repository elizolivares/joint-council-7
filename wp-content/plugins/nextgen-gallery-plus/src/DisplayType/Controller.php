<?php

/**
 * Provides overrides for Pro display type controllers over the NextGEN parent.
 *
 * @package NextGEN Pro
 */
namespace Imagely\NGGPro\DisplayType;

use Imagely\NGG\DisplayType\Controller as ParentController;
use Imagely\NGGPro\Bootloader;
/**
 * Provides overrides for Pro display type controllers over the NextGEN parent.
 */
class Controller extends ParentController
{
    /**
     * Allows the admin forms that display available templates to limit the selection to one directory.
     *
     * @return string
     */
    public function get_template_directory_abspath() : string
    {
        $reflection = new \ReflectionClass($this);
        return path_join(Bootloader::$plugin_directory_root, 'templates' . DIRECTORY_SEPARATOR . 'DisplayTypes' . DIRECTORY_SEPARATOR . $reflection->getShortName());
    }
}