<?php

/**
 * Provides a non-namespaced file, loaded during NGG Pro/Plus/Starter activation hooks, to prevent duplicate extensions.
 *
 * @package NextGEN Pro
 */
/**
 * Prevent conflicts by disabling other NextGEN Gallery addons that are already active.
 *
 * @param string $plugin_id The plugin currently being activated.
 */
function prevent_duplicate_nextgen_extensions(string $plugin_id)
{
    $plugins = (array) \get_option('active_plugins');
    $filenames_to_match = ['nggallery-pro.php', 'ngg-plus.php', 'ngg-starter.php'];
    array_map(function ($plugin) use($filenames_to_match) {
        foreach ($filenames_to_match as $filename) {
            if (stripos($plugin, $filename)) {
                \deactivate_plugins($plugin);
            }
        }
    }, $plugins);
    // This sets an option for admin_notices to catch and display a one-time message post-activation.
    /**
     * Remove this from NextGEN Starter.
     *
     * @remove-for-nextgen-starter
     */
    if ('starter' !== $plugin_id) {
        \update_option('photocrati_pro_recently_activated', 'true');
    }
}