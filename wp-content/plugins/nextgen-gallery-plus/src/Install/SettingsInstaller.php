<?php

namespace Imagely\NGGPro\Install;

use Imagely\NGG\Settings\Settings;
abstract class SettingsInstaller
{
    protected $defaults = [];
    protected $current = [];
    protected $groups = [];
    public function set_defaults($defaults = [])
    {
        $this->defaults = $defaults;
    }
    public function load_current_settings()
    {
        $settings = Settings::get_instance();
        foreach ($this->defaults as $key => $unused) {
            $this->current[$key] = $settings->get($key);
        }
    }
    public function set_current_settings()
    {
        $settings = Settings::get_instance();
        foreach ($this->current as $key => $val) {
            $settings->set($key, $val);
        }
    }
    public function get_groups()
    {
        return $this->groups;
    }
    public function set_groups($groups = [])
    {
        $this->groups = $groups;
    }
    function reset()
    {
        $this->uninstall(true);
        $settings = Settings::get_instance();
        foreach ($this->defaults as $key => $val) {
            $settings->set($key, $val);
        }
        $settings->save();
    }
    public function install()
    {
        $settings = Settings::get_instance();
        foreach ($this->defaults as $key => $val) {
            $settings->set_default_value($key, $val);
        }
    }
    public function uninstall($hard = false)
    {
        if ($hard) {
            $settings = Settings::get_instance();
            foreach ($this->defaults as $key => $val) {
                $settings->delete($key);
            }
            $settings->save();
        }
    }
}