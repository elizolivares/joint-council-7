<?php

/**
 * @deprecated
 */
class M_NextGen_PictureFill
{
    /**
     * @deprecated
     */
    public static function render_picture_source($image, $named_size, $hidpi_size_name = null, $media = null)
    {
        \Imagely\NGGPro\Display\HiDPI::render_picture_source($image, $named_size, $hidpi_size_name);
    }
    /**
     * @deprecated
     */
    public static function get_hidpi_named_size($image, $original_named_size)
    {
        \Imagely\NGGPro\Display\HiDPI::get_hidpi_named_size($image, $original_named_size);
    }
    /**
     * @deprecated
     */
    public static function render_picture_element($image, $params_or_named_size, $attrs = [], $echo = true)
    {
        \Imagely\NGGPro\Display\HiDPI::render_picture_element($image, $params_or_named_size, $attrs, $echo);
    }
    /**
     * @deprecated
     */
    public static function has_dynamic_images($str)
    {
        \Imagely\NGGPro\Display\HiDPI::has_dynamic_images($str);
    }
    /**
     * @deprecated
     */
    public static function prepare_picture_element($image, $params_or_named_size, $attrs = [])
    {
        \Imagely\NGGPro\Display\HiDPI::prepare_picture_element($image, $params_or_named_size, $attrs);
    }
}
/**
 * @deprecated
 */
class M_NextGen_Pro_I18N
{
    /**
     * @deprecated
     */
    public static function get_kses_allowed_html()
    {
        return \Imagely\NGG\Display\I18N::get_kses_allowed_html();
    }
}
/**
 * @remove-for-nextgen-starter
 * @deprecated
 * @param $galleryID
 * @param string    $template
 * @return string
 */
function nggShowProImageBrowser($galleryID, $template = '')
{
    $renderer = \Imagely\NGG\DisplayedGallery\Renderer::get_instance();
    $retval = $renderer->display_images(['gallery_ids' => [$galleryID], 'display_type' => NGG_PRO_IMAGEBROWSER, 'template' => $template]);
    return apply_filters('ngg_show_imagebrowser_content', $retval, $galleryID);
}
/**
 * @remove-for-nextgen-starter
 * @deprecated
 * @param $picturelist
 * @param string      $template
 * @return string
 */
function nggCreateProImageBrowser($picturelist, $template = '')
{
    $renderer = \Imagely\NGG\DisplayedGallery\Renderer::get_instance();
    $image_ids = [];
    foreach ($picturelist as $image) {
        $image_ids[] = $image->pid;
    }
    return $renderer->display_images(['image_ids' => $image_ids, 'display_type' => NGG_PRO_IMAGEBROWSER, 'template' => $template]);
}
/**
 * @remove-for-nextgen-starter
 * @remove-for-nextgen-plus
 */
if ('pro' === \Imagely\NGGPro\Bootloader::$plugin_id) {
    // In case bcmath isn't enabled, we provide these wrappers
    if (!function_exists('bcadd')) {
        function bcadd($one, $two, $scale = null)
        {
            return floatval($one) + floatval($two);
        }
    }
    if (!function_exists('bcmul')) {
        function bcmul($one, $two, $scale = null)
        {
            return floatval($one) * floatval($two);
        }
    }
    if (!function_exists('bcdiv')) {
        function bcdiv($one, $two, $scale = null)
        {
            return floatval($one) / floatval($two);
        }
    }
    if (!function_exists('bcsub')) {
        function bcsub($one, $two, $scale = null)
        {
            return floatval($one) - floatval($two);
        }
    }
    if (!function_exists('bcmod')) {
        function bcmod($one, $two, $scale = null)
        {
            return floatval($one) % floatval($two);
        }
    }
}