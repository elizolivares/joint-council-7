<?php

/**
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\DataTypes;

use Imagely\NGGPro\DataMappers\Comment as CommentMapper;
use Imagely\NGGPro\Lightbox\CommentsManager;
use Imagely\NGG\DataMapper\WPModel;
class Comment extends WPModel
{
    public $name;
    public function get_mapper()
    {
        return CommentMapper::get_instance();
    }
    function validation()
    {
        $errors = array_merge([], $this->validates_presence_of('name'), $this->validates_uniqueness_of('name'));
        return empty($errors) ? true : $errors;
    }
    function get_comments_data($page = 0)
    {
        \add_action('pre_get_posts', [$this, 'set_comment_data_query_args'], PHP_INT_MAX, 1);
        $retval = [];
        $retval['container_id'] = $this->{$this->id_field};
        ob_start();
        $args = ['post_type' => 'photocrati-comments', 'p' => $retval['container_id'], 'cpage' => (int) $page];
        // filtering must be disabled for this to function
        CommentsManager::$filter_comments = false;
        \query_posts($args);
        while (have_posts()) {
            \the_post();
            \comments_template('ngg_comments');
        }
        $retval['rendered_view'] = ob_get_contents();
        // restore to our previous state
        \wp_reset_query();
        CommentsManager::$filter_comments = true;
        ob_end_clean();
        \remove_action('pre_get_posts', [$this, 'set_comment_data_query_args'], PHP_INT_MAX, 1);
        return $retval;
    }
    /**
     * Prevent other plugins from adding a filter that negates our ability to do basic database searches
     *
     * @param $query
     */
    function set_comment_data_query_args($query)
    {
        $query->query_vars['suppress_filters'] = true;
    }
}