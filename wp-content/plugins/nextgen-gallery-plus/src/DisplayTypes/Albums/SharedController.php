<?php

/** @remove-for-nextgen-starter */
namespace Imagely\NGGPro\DisplayTypes\Albums;

use Imagely\NGG\DataMappers\Album as AlbumMapper;
use Imagely\NGG\DataMappers\Gallery as GalleryMapper;
use Imagely\NGG\DataMappers\Image as ImageMapper;
use Imagely\NGG\DataStorage\Manager as StorageManager;
use Imagely\NGG\DisplayType\Controller as DisplayTypeController;
use Imagely\NGG\DisplayTypes\Albums\SharedController as ParentController;
use Imagely\NGG\Display\StaticAssets as ParentStaticAssets;
use Imagely\NGG\DisplayedGallery\Renderer as DisplayedGalleryRenderer;
use Imagely\NGG\DynamicThumbnails\Manager as DynamicThumbnailsManager;
use Imagely\NGGPro\Bootloader;
use Imagely\NGGPro\Display\View;
use Imagely\NGG\DataTypes\{DisplayedGallery, Image};
use Imagely\NGG\Display\LightboxManager;
use Imagely\NGG\Util\Router;
class SharedController extends ParentController
{
    // These two are used to prevent two album shortcodes on one page from displaying the same results twice
    public static $displayed_galleries = [];
    public static $displayed_albums = [];
    public function enqueue_frontend_resources($displayed_gallery)
    {
        DisplayTypeController::enqueue_frontend_resources($displayed_gallery);
        $this->enqueue_pagination_resources();
        $ds = $displayed_gallery->display_settings;
        if (!empty($ds['enable_breadcrumbs']) || !empty($ds['original_settings']['enable_breadcrumbs'])) {
            \wp_enqueue_style('nextgen_basic_album_breadcrumbs_style', ParentStaticAssets::get_url('Albums/breadcrumbs.css', 'photocrati-nextgen_basic_album#breadcrumbs.css'), [], Bootloader::$script_version);
        }
    }
    /**
     * Determines if the current $displayed_gallery should be displayed as another DisplayedGallery; for example if
     * a thumbnail view should be displayed as a slideshow or imagebrowser.
     *
     * @param DisplayedGallery $displayed_gallery The original displayed gallery.
     * @return DisplayedGallery The original, or new, displayed gallery to display.
     */
    public function get_alternate_displayed_gallery(DisplayedGallery $displayed_gallery) : DisplayedGallery
    {
        // Prevent recursive checks for further alternates causing additional modifications to the settings array
        $id = $displayed_gallery->id();
        if (!empty(self::$alternate_displayed_galleries[$id])) {
            return self::$alternate_displayed_galleries[$id];
        }
        // Without this line the param() method will always return NULL when in wp_enqueue_scripts
        $renderer = DisplayedGalleryRenderer::get_instance('inner');
        $renderer->do_app_rewrites($displayed_gallery);
        $mapper = GalleryMapper::get_instance();
        $router = Router::get_instance();
        // Try finding the gallery by slug first. If nothing is found, we assume that the user passed in a gallery id instead
        $gallery = $router->get_parameter('gallery');
        $result = $mapper->get_by_slug($gallery);
        if ($result) {
            $gallery = $result->{$result->id_field};
        }
        if (empty($this->albums)) {
            $this->albums = $displayed_gallery->get_albums();
        }
        $gallery_params = ['container_ids' => [$gallery], 'display_type' => $displayed_gallery->display_settings['gallery_display_type'], 'original_album_entities' => $this->albums, 'original_display_type' => $displayed_gallery->display_type, 'original_settings' => $displayed_gallery->display_settings, 'source' => 'galleries'];
        if (!empty($displayed_gallery->display_settings['gallery_display_template'])) {
            $gallery_params['template'] = $displayed_gallery->display_settings['gallery_display_template'];
        }
        $displayed_gallery = $renderer->params_to_displayed_gallery($gallery_params);
        if (is_null($displayed_gallery->id())) {
            $displayed_gallery->id(md5(json_encode($displayed_gallery->get_entity())));
        }
        self::$alternate_displayed_galleries[$id] = $displayed_gallery;
        return $displayed_gallery;
    }
    public function get_displayed_gallery_thumbnail_size_name(DisplayedGallery $displayed_gallery) : string
    {
        $thumbnail_size_name = 'thumb';
        if (isset($displayed_gallery->display_settings['override_thumbnail_settings']) && $displayed_gallery->display_settings['override_thumbnail_settings']) {
            $dynthumbs = DynamicThumbnailsManager::get_instance();
            $dyn_params = ['height' => $displayed_gallery->display_settings['thumbnail_height'], 'width' => $displayed_gallery->display_settings['thumbnail_width']];
            if ($displayed_gallery->display_settings['thumbnail_quality']) {
                $dyn_params['quality'] = $displayed_gallery->display_settings['thumbnail_quality'];
            }
            if ($displayed_gallery->display_settings['thumbnail_crop']) {
                $dyn_params['crop'] = true;
            }
            if ($displayed_gallery->display_settings['thumbnail_watermark']) {
                $dyn_params['watermark'] = true;
            }
            $thumbnail_size_name = $dynthumbs->get_size_name($dyn_params);
        }
        return $thumbnail_size_name;
    }
    /**
     * @param DisplayedGallery $displayed_gallery
     * @param bool $return
     *
     * @return string
     */
    public function index_action($displayed_gallery, $return = false) : string
    {
        $router = Router::get_instance();
        // Ensure that the open_gallery_in_lightbox setting is present
        if (!array_key_exists('open_gallery_in_lightbox', $displayed_gallery->display_settings)) {
            $displayed_gallery->display_settings['open_gallery_in_lightbox'] = 0;
        }
        if (empty($this->albums)) {
            $this->albums = $displayed_gallery->get_albums();
        }
        // Determine what to render:
        // 1) A gallery
        if ($router->get_parameter('gallery')) {
            \add_filter('ngg_display_type_rendering_object', [$this, 'add_breadcrumbs_and_descriptions'], 10, 2);
            $retval = $this->render_gallery($displayed_gallery, true);
            \remove_filter('ngg_display_type_rendering_object', [$this, 'add_breadcrumbs_and_descriptions'], 10);
        } elseif ($album_id = $router->get_parameter('album')) {
            if (!empty(self::$displayed_albums[$album_id])) {
                $retval = '';
            } else {
                self::$displayed_albums[$album_id] = true;
                $mapper = AlbumMapper::get_instance();
                $result = $mapper->select()->where(['slug = %s', $album_id])->limit(1)->run_query();
                $result = array_pop($result);
                $displayed_gallery->container_ids = [$result->{$result->id_field}];
                \add_filter('ngg_display_type_rendering_object', [$this, 'add_breadcrumbs_and_descriptions'], 10, 2);
                $retval = $this->render_album($displayed_gallery, $this->albums, $return);
                \remove_filter('ngg_display_type_rendering_object', [$this, 'add_breadcrumbs_and_descriptions'], 10);
            }
        } else {
            $retval = $this->render_album($displayed_gallery, null, $return);
        }
        return $this->render_dyncss($displayed_gallery) . $retval;
    }
    /**
     * @param DisplayedGallery $displayed_gallery
     * @param string           $thumbnail_size_name
     * @return mixed|void
     */
    public function prepare_entities($displayed_gallery, $thumbnail_size_name)
    {
        $mapper = ImageMapper::get_instance();
        $storage = StorageManager::get_instance();
        $router = Router::get_instance();
        $current_url = $router->get_routed_app()->get_routed_url();
        $entities = $displayed_gallery->get_included_entities();
        foreach ($entities as &$entity) {
            $entity->entity_type = $entity_type = intval($entity->is_gallery) ? 'gallery' : 'album';
            // Is the gallery actually a link to a page?
            if (isset($entity->pageid) && $entity->pageid > 0) {
                $entity->link = get_page_link($entity->pageid);
            } else {
                $page_url = $current_url;
                $parent_album = $this->get_parent_album_for($entity->{$entity->id_field});
                if ($parent_album) {
                    $app = $router->get_routed_app();
                    $page_url = $app->remove_parameter('album', null, $page_url);
                    $page_url = $app->remove_parameter('gallery', null, $page_url);
                    $page_url = $app->remove_parameter('nggpage', null, $page_url);
                    $page_url = $this->set_param_for($page_url, 'album', $parent_album->slug);
                }
                $entity->link = $this->set_param_for($page_url, $entity_type, $entity->slug);
            }
            // Add image information to the gallery object
            $preview_img = $mapper->find($entity->previewpic);
            $entity->previewpic_image = $preview_img;
            $entity->thumb_size = $storage->get_image_dimensions($preview_img, $thumbnail_size_name);
            $entity->previewpic_image_url = $storage->get_image_url($preview_img, 'full');
            $entity->previewpic_thumbnail_url = $storage->get_image_url($preview_img, $thumbnail_size_name);
            $entity->previewpic_thumb_url = $entity->previewpic_thumbnail_url;
            // If the setting is on we need to inject an effect code.
            if (!empty($displayed_gallery->display_settings['open_gallery_in_lightbox']) && $entity_type == 'gallery') {
                $entity = $this->make_child_displayed_gallery($entity, $displayed_gallery->display_settings);
                $lightbox = LightboxManager::get_instance()->get_selected();
                if ($lightbox->is_supported($displayed_gallery)) {
                    $entity->displayed_gallery->effect_code = $this->get_effect_code($entity->displayed_gallery);
                }
            }
        }
        return $entities;
    }
    /**
     * @param DisplayedGallery $displayed_gallery
     * @param null|Image[]     $original_entities
     * @param bool             $return
     * @return mixed
     */
    public function render_album($displayed_gallery, $original_entities, $return)
    {
        $id = 'displayed_gallery_' . $displayed_gallery->id();
        $thumbnail_size_name = $this->get_displayed_gallery_thumbnail_size_name($displayed_gallery);
        $entities = $this->prepare_entities($displayed_gallery, $thumbnail_size_name);
        $params = array_merge($displayed_gallery->display_settings, ['css_class' => $this->get_css_class(), 'entities' => $entities, 'effect_code' => $this->get_effect_code($displayed_gallery), 'id' => $id, 'thumbnail_size_name' => $thumbnail_size_name]);
        $params = $this->prepare_display_parameters($displayed_gallery, $params);
        if (!is_null($original_entities)) {
            $displayed_gallery->display_settings['original_album_id'] = 'a' . $displayed_gallery->container_ids[0];
            $displayed_gallery->display_settings['original_album_entities'] = $original_entities;
        }
        $view = new View('DisplayTypes/Albums/default', $params, NGG_PRO_ALBUMS . '#index');
        $result = $view->render(true);
        $result = preg_replace('~>\\s*\\n\\s*<~', '><', $result);
        if ($return) {
            return $result;
        } else {
            // This is the output of a rendered template, possibly from a third party.
            //phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
            print $result;
            return $result;
        }
    }
    /**
     * @param DisplayedGallery $displayed_gallery
     * @return string
     */
    public function render_dyncss($displayed_gallery)
    {
        $display_settings = $displayed_gallery->display_settings;
        $display_settings['id'] = 'displayed_gallery_' . $displayed_gallery->id();
        $dyncss = new View('DisplayTypes/' . $this->get_template_dirname() . '/dyncss', $display_settings, $this->get_legacy_template_id());
        return $dyncss->render(true);
    }
    /**
     * @param DisplayedGallery $displayed_gallery
     * @param bool             $return
     * @return string
     */
    public function render_gallery($displayed_gallery, $return = false)
    {
        $alternate_displayed_gallery = $this->get_alternate_displayed_gallery($displayed_gallery);
        $id = $alternate_displayed_gallery->id();
        // Prevent displaying the same gallery twice per request
        if (!empty(self::$displayed_galleries[$id])) {
            $output = '';
        } else {
            self::$displayed_galleries[$id] = true;
            $output = DisplayedGalleryRenderer::get_instance()->display_images($alternate_displayed_gallery, $return);
        }
        return $output;
    }
    /**
     * @param string      $url
     * @param string      $key
     * @param string      $value
     * @param null|string $id
     * @param bool        $use_prefix
     * @return mixed|string|string[]|null
     */
    public function set_param_for($url, $key, $value, $id = null, $use_prefix = false)
    {
        $app = Router::get_instance()->get_routed_app();
        $retval = $app->set_parameter($key, $value, $id, $use_prefix, $url);
        // Adjust the return value
        while (preg_match('#album--([^/]+)#', $retval, $matches)) {
            $retval = str_replace($matches[0], $matches[1], $retval);
        }
        while (preg_match('#gallery--([^/]+)#', $retval, $matches)) {
            $retval = str_replace($matches[0], $matches[1], $retval);
        }
        return $retval;
    }
}