<?php

/**
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\DisplayTypes;

use Imagely\NGGPro\DisplayType\Controller as ParentController;
use Imagely\NGG\DataStorage\Manager as StorageManager;
use Imagely\NGG\DynamicThumbnails\Manager as DynamicThumbnailsManager;
use Imagely\NGGPro\Bootloader;
use Imagely\NGGPro\Display\StaticAssets;
use Imagely\NGGPro\Display\View;
use Imagely\NGG\DataTypes\DisplayedGallery;
use Imagely\NGG\Display\DisplayManager;
class Masonry extends ParentController
{
    /**
     * @param DisplayedGallery $displayed_gallery
     */
    function enqueue_frontend_resources($displayed_gallery)
    {
        \wp_enqueue_style('nextgen_pro_masonry_style', StaticAssets::get_url('DisplayTypes/Masonry/style.css', 'photocrati-nextgen_pro_masonry#style.css'), [], Bootloader::$script_version);
        // WordPress 5.3 still only has Masonry version 3.3.2. To prevent conflicts we use the WordPress provided
        // masonry JS *if* it is already enqueued by something else, but we prefer to use our own at version 4.2.2
        if (wp_script_is('jquery-masonry', 'enqueued')) {
            \wp_enqueue_script('nextgen_pro_masonry_script', StaticAssets::get_url('DisplayTypes/Masonry/main_compat.js', 'photocrati-nextgen_pro_masonry#nextgen_pro_masonry_compat.js'), ['jquery-masonry', 'ngg_waitforimages'], Bootloader::$script_version);
            \wp_localize_script('nextgen_pro_masonry_script', 'nextgen_pro_masonry_settings', ['columnWidth' => $displayed_gallery->display_settings['size'], 'gutterWidth' => $displayed_gallery->display_settings['padding']]);
        } else {
            DisplayManager::enqueue_fontawesome();
            // WordPress' masonry isn't being used so use our own
            \wp_enqueue_script('nextgen_pro_masonry_masonry_script', StaticAssets::get_url('DisplayTypes/Masonry/masonry.min.js', 'photocrati-nextgen_pro_masonry#masonry.min.js'), ['jquery', 'ngg_waitforimages'], Bootloader::$script_version);
            \wp_enqueue_script('nextgen_pro_masonry_script', StaticAssets::get_url('DisplayTypes/Masonry/main.js', 'photocrati-nextgen_pro_masonry#nextgen_pro_masonry.js'), ['nextgen_pro_masonry_masonry_script'], Bootloader::$script_version);
            \wp_localize_script('nextgen_pro_masonry_masonry_script', 'nextgen_pro_masonry_settings', ['center_gallery' => $displayed_gallery->display_settings['center_gallery']]);
        }
        parent::enqueue_frontend_resources($displayed_gallery);
    }
    public function get_default_settings()
    {
        return \apply_filters('ngg_pro_masonry_default_settings', ['center_gallery' => 0, 'display_type_view' => 'default', 'padding' => 10, 'size' => 180]);
    }
    public function get_preview_image_url()
    {
        return StaticAssets::get_url('DisplayTypes/Masonry/preview.jpg');
    }
    function index_action($displayed_gallery, $return = false)
    {
        $images = $displayed_gallery->get_included_entities();
        // This display type was never meant to work with NextGen's trigger icons but the setting was set to 'always' as
        // a default value in the pro-masonry-mapper. Prevent users from getting an improperly rendered gallery trigger:
        $displayed_gallery->display_settings['ngg_triggers_display'] = 'never';
        if (!$images) {
            $view = new View('GalleryDisplay/NoImagesFound', [], 'photocrati-nextgen_gallery_display#no_images_found');
            return $view->render($return);
        }
        $params = $displayed_gallery->display_settings;
        $params['images'] = $images;
        $params['storage'] = StorageManager::get_instance();
        $params['effect_code'] = $this->get_effect_code($displayed_gallery);
        $params['displayed_gallery_id'] = $displayed_gallery->id();
        $params['thumbnail_size_name'] = DynamicThumbnailsManager::get_instance()->get_size_name(['width' => $params['size'], 'crop' => false]);
        $params = $this->prepare_display_parameters($displayed_gallery, $params);
        $view = new View('DisplayTypes/Masonry/default', $params, 'photocrati-nextgen_pro_masonry#index');
        return $view->render($return);
    }
    function install($reset = false)
    {
        $this->install_display_type(NGG_PRO_MASONRY, ['title' => __('NextGEN Pro Masonry', 'nextgen-gallery-pro'), 'entity_types' => ['image'], 'default_source' => 'galleries', 'hidden_from_ui' => false, 'view_order' => NGG_DISPLAY_PRIORITY_BASE + NGG_DISPLAY_PRIORITY_STEP * 10 + 50, 'settings' => $this->get_default_settings(), 'aliases' => ['masonry', 'pro_masonry', 'nextgen_pro_masonry']], $reset);
    }
}