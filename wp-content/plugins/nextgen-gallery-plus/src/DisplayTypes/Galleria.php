<?php

namespace Imagely\NGGPro\DisplayTypes;

use Imagely\NGGPro\DisplayType\Controller as ParentController;
use Imagely\NGG\DataStorage\Manager as StorageManager;
use Imagely\NGG\DynamicThumbnails\Manager as DynamicThumbnailsManager;
use Imagely\NGGPro\Bootloader;
use Imagely\NGGPro\Display\HiDPI;
use Imagely\NGGPro\Display\StaticAssets;
use Imagely\NGGPro\Display\View;
use Imagely\NGG\DataTypes\DisplayedGallery;
use Imagely\NGG\DataTypes\Image;
use Imagely\NGG\Display\DisplayManager;
use Imagely\NGG\Settings\Settings;
class Galleria extends ParentController
{
    public static $localized_galleries = [];
    /**
     * @param DisplayedGallery $displayed_gallery
     * @param string?          $type
     * @return DisplayedGallery
     */
    function compute_aspect_ratio($displayed_gallery, $type = null)
    {
        $storage = StorageManager::get_instance();
        $list = $displayed_gallery->get_included_entities();
        if ($type == null) {
            $type = !empty($displayed_gallery->display_settings['aspect_ratio']) ? $displayed_gallery->display_settings['aspect_ratio'] : 'image_average';
        }
        switch ($type) {
            case 'first_image':
                if ($list != null) {
                    $image = $list[0];
                    $dims = $storage->get_image_dimensions($image);
                    $ratio = round($dims['width'] / $dims['height'], 2);
                    $displayed_gallery->display_settings['aspect_ratio_computed'] = $ratio;
                }
                break;
            case 'image_average':
                if ($list != null) {
                    $ratio_sum = 0;
                    $image_count = 0;
                    foreach ($list as $image) {
                        $dims = $storage->get_image_dimensions($image);
                        if ($dims) {
                            $ratio = round($dims['width'] / $dims['height'], 2);
                            $ratio_sum += $ratio;
                            ++$image_count;
                        }
                    }
                    $computed_ratio = round($ratio_sum / $image_count, 2);
                    if ($computed_ratio > 0) {
                        $displayed_gallery->display_settings['aspect_ratio_computed'] = $computed_ratio;
                    }
                }
                break;
        }
        return $displayed_gallery;
    }
    /**
     * @param DisplayedGallery $displayed_gallery
     * @param int|null         $limit
     * @param string           $size_name
     * @param string           $thumbnail_size_name
     */
    static function enqueue_entities($displayed_gallery, $limit = null, $size_name = 'full', $thumbnail_size_name = 'thumb')
    {
        if (in_array($displayed_gallery->id(), self::$localized_galleries)) {
            return;
        }
        self::$localized_galleries[] = $displayed_gallery->id();
        if (is_null($limit)) {
            $settings = Settings::get_instance()->get('ngg_pro_lightbox');
            $limit = $settings['localize_limit'];
        }
        $ds = $displayed_gallery->display_settings;
        // If the display_type is mosaic, slideshow or filmstrip, we need to override the limit.
        // Because those displays do not have pagination.
        if (in_array($displayed_gallery->display_type, ['photocrati-nextgen_pro_mosaic', 'photocrati-nextgen_pro_horizontal_filmstrip', 'photocrati-nextgen_pro_slideshow'], true)) {
            $entities = $displayed_gallery->get_entities();
        } else {
            $entities = $displayed_gallery->get_entities($limit);
        }
        if (!empty($ds['override_thumbnail_settings']) && $ds['override_thumbnail_settings']) {
            $dynthumbs = DynamicThumbnailsManager::get_instance();
            $dyn_params = ['width' => $ds['thumbnail_width'], 'height' => $ds['thumbnail_height'], 'crop' => true];
            $thumbnail_size_name = $dynthumbs->get_size_name($dyn_params);
        }
        if (!empty($ds['override_image_settings']) && $ds['override_image_settings'] && !empty($ds['override_image_size_name'])) {
            $size_name = $ds['override_image_size_name'];
        }
        // Localize the gallery images for startup performance
        DisplayManager::add_script_data('ngg_common', 'galleries.gallery_' . $displayed_gallery->id() . '.images_list', \apply_filters('ngg_pro_lightbox_images_queue', self::format_entities($entities, $size_name, $thumbnail_size_name)), false);
        DisplayManager::add_script_data('ngg_common', 'galleries.gallery_' . $displayed_gallery->id() . '.images_list_limit', $limit, false);
        DisplayManager::add_script_data('ngg_common', 'galleries.gallery_' . $displayed_gallery->id() . '.images_list_count', $displayed_gallery->get_entity_count(), false);
    }
    /**
     * @param DisplayedGallery $displayed_gallery
     */
    function enqueue_frontend_resources($displayed_gallery)
    {
        parent::enqueue_frontend_resources($displayed_gallery);
        // Add some properties to the displayed gallery
        if (($source = $displayed_gallery->get_source()) && in_array('image', $source->returns)) {
            $displayed_gallery = $this->compute_aspect_ratio($displayed_gallery);
        }
        self::enqueue_entities($displayed_gallery);
        wp_register_script('ngg_galleria', StaticAssets::get_url('DisplayTypes/Galleria/galleria-1.6.1.js', NGG_PRO_GALLERIA . '#galleria-1.6.1.js'), ['jquery'], '1.6.1');
        wp_enqueue_script('ngg_galleria_init', StaticAssets::get_url('DisplayTypes/Galleria/ngg_galleria.js', NGG_PRO_GALLERIA . '#ngg_galleria.js'), ['ngg_galleria'], Bootloader::$script_version);
    }
    /**
     * @param Image[]             $entities
     * @param $size_name
     * @param $thumbnail_size_name
     * @return array
     */
    static function format_entities($entities = [], $size_name = 'full', $thumbnail_size_name = 'thumb')
    {
        $retval = [];
        if (!empty($entities)) {
            $storage = StorageManager::get_instance();
            foreach ($entities as $entity) {
                if (isset($entity->is_gallery) && !$entity->is_gallery) {
                    continue;
                }
                if (isset($entity->is_album) && !$entity->is_album) {
                    continue;
                }
                $size = $storage->get_image_dimensions($entity, $size_name);
                $thumb_size = $storage->get_image_dimensions($entity, $thumbnail_size_name);
                $parts = HiDPI::prepare_picture_element($entity, $size_name);
                $full_parts = HiDPI::prepare_picture_element($entity, 'full');
                $retval[] = ['image' => $storage->get_image_url($entity, $size_name), 'srcsets' => $parts['srcsets_unfiltered'], 'use_hdpi' => $parts['use_hdpi'], 'title' => $entity->alttext, 'description' => $entity->description, 'image_id' => $entity->{$entity->id_field}, 'thumb' => $storage->get_image_url($entity, $thumbnail_size_name), 'width' => $size['width'], 'height' => $size['height'], 'full_image' => $storage->get_image_url($entity, 'full'), 'full_use_hdpi' => $full_parts['use_hdpi'], 'full_srcsets' => $full_parts['srcsets_unfiltered'], 'thumb_dimensions' => ['width' => $thumb_size['width'], 'height' => $thumb_size['height']]];
            }
        }
        return $retval;
    }
    /**
     * @param DisplayedGallery $displayed_gallery
     * @param bool             $return
     * @return string?
     */
    function index_action($displayed_gallery, $return = false)
    {
        // Yes this is a weird / bad place to put this, but it's just not worth the time to correct existing mistakes just to make this one line cleaner
        $displayed_gallery->display_settings['theme'] = $displayed_gallery->display_type === 'photocrati-nextgen_pro_slideshow' ? 'nextgen_pro_slideshow' : 'nextgen_pro_horizontal_filmstrip';
        $params = [
            'theme' => $displayed_gallery->display_settings['theme'],
            'displayed_gallery_id' => $displayed_gallery->id(),
            // do not remove: this is not used in the template, but without it ecommerce triggers will not display
            'images' => $displayed_gallery->get_entities(),
            'effect_code' => $this->get_effect_code($displayed_gallery),
            'storage' => StorageManager::get_instance(),
        ];
        $params = $this->prepare_display_parameters($displayed_gallery, $params);
        $view = new View('DisplayTypes/Galleria/default', $params, NGG_PRO_GALLERIA . '#galleria');
        return $view->render($return);
    }
}