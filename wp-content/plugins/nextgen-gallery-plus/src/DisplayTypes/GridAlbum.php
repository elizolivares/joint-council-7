<?php

/** @remove-for-nextgen-starter */
namespace Imagely\NGGPro\DisplayTypes;

use Imagely\NGG\Display\StaticAssets as ParentStaticAssets;
use Imagely\NGGPro\Bootloader;
use Imagely\NGGPro\DisplayTypes\Albums\SharedController;
use Imagely\NGGPro\Display\StaticAssets;
use Imagely\NGG\DataTypes\DisplayedGallery;
use Imagely\NGG\Settings\Settings;
class GridAlbum extends SharedController
{
    public function get_css_class() : string
    {
        return 'nextgen_pro_grid_album';
    }
    public function get_template_dirname() : string
    {
        return 'GridAlbum';
    }
    public function get_legacy_template_id() : string
    {
        return 'photocrati-nextgen_pro_albums#nextgen_pro_grid_album_dyncss';
    }
    /**
     * @param DisplayedGallery $displayed_gallery
     */
    public function enqueue_frontend_resources($displayed_gallery)
    {
        parent::enqueue_frontend_resources($displayed_gallery);
        \wp_enqueue_style('nextgen_pro_grid_album', StaticAssets::get_url('DisplayTypes/GridAlbum/style.css', 'photocrati-nextgen_pro_albums#nextgen_pro_grid_album.css'), [], Bootloader::$script_version);
        \wp_enqueue_script('nextgen_pro_albums', StaticAssets::get_url('DisplayTypes/Albums/main.js', 'photocrati-nextgen_pro_albums#nextgen_pro_album_init.js'), ['jquery', 'shave.js'], Bootloader::$script_version, true);
    }
    public function install($reset = false)
    {
        $this->install_display_type(NGG_PRO_GRID_ALBUM, ['title' => __('NextGEN Pro Grid Album', 'nextgen-gallery-pro'), 'entity_types' => ['gallery', 'album'], 'default_source' => 'albums', 'hidden_from_ui' => false, 'view_order' => NGG_DISPLAY_PRIORITY_BASE + NGG_DISPLAY_PRIORITY_STEP * 10 + 210, 'settings' => $this->get_default_settings(), 'aliases' => ['pro_grid_album', 'grid_album', 'nextgen_pro_grid_album']], $reset);
    }
    public function get_preview_image_url() : string
    {
        return StaticAssets::get_url('DisplayTypes/GridAlbum/preview.jpg');
    }
    public function get_default_settings()
    {
        $settings = Settings::get_instance();
        return \apply_filters('ngg_pro_grid_album_default_settings', [
            // Default child gallery view
            'gallery_display_type' => defined('NGG_PRO_THUMBNAIL_GRID') ? NGG_PRO_THUMBNAIL_GRID : NGG_BASIC_THUMBNAILS,
            // Basic style settings
            'background_color' => '#FFFFFF',
            'border_color' => '#CCCCCC',
            'border_size' => 1,
            'caption_color' => '#333333',
            'display_type_view' => 'default',
            'enable_breadcrumbs' => 1,
            'enable_descriptions' => 0,
            'padding' => 20,
            'spacing' => 10,
            // Thumbnail dimensions
            'open_gallery_in_lightbox' => 0,
            'override_thumbnail_settings' => 0,
            'thumbnail_crop' => $settings->get('thumbfix'),
            'thumbnail_height' => $settings->get('thumbheight'),
            'thumbnail_quality' => $settings->get('thumbquality'),
            'thumbnail_watermark' => 0,
            'thumbnail_width' => $settings->get('thumbwidth'),
            // Grid albums do not share a caption_size
            'caption_size' => 13,
        ]);
    }
}