<?php

/** @remove-for-nextgen-starter */
namespace Imagely\NGGPro\DisplayTypes;

use Imagely\NGG\Display\StaticAssets as ParentStaticAssets;
use Imagely\NGGPro\DisplayTypes\Albums\SharedController;
use Imagely\NGGPro\Display\StaticAssets;
use Imagely\NGG\DataTypes\DisplayedGallery;
use Imagely\NGG\Settings\Settings;
class ListAlbum extends SharedController
{
    public function get_css_class() : string
    {
        return 'nextgen_pro_list_album';
    }
    public function get_template_dirname() : string
    {
        return 'ListAlbum';
    }
    public function get_legacy_template_id() : string
    {
        return NGG_PRO_ALBUMS . '#nextgen_pro_list_album_dyncss';
    }
    /**
     * @param DisplayedGallery $displayed_gallery
     */
    public function enqueue_frontend_resources($displayed_gallery)
    {
        parent::enqueue_frontend_resources($displayed_gallery);
        \wp_enqueue_style('nextgen_pro_list_album', StaticAssets::get_url('DisplayTypes/ListAlbum/style.css', 'photocrati-nextgen_pro_albums#nextgen_pro_list_album.css'));
        \wp_enqueue_script('nextgen_pro_albums', StaticAssets::get_url('DisplayTypes/Albums/main.js', 'photocrati-nextgen_pro_albums#nextgen_pro_album_init.js'), [], false, true);
    }
    public function install($reset = false)
    {
        $this->install_display_type(NGG_PRO_LIST_ALBUM, ['title' => __('NextGEN Pro List Album', 'nextgen-gallery-pro'), 'entity_types' => ['gallery', 'album'], 'default_source' => 'albums', 'hidden_from_ui' => false, 'view_order' => NGG_DISPLAY_PRIORITY_BASE + NGG_DISPLAY_PRIORITY_STEP * 10 + 200, 'settings' => $this->get_default_settings(), 'aliases' => ['pro_list_album', 'list_album', 'nextgen_pro_list_album']], $reset);
    }
    public function get_preview_image_url() : string
    {
        return StaticAssets::get_url('DisplayTypes/ListAlbum/preview.jpg');
    }
    public function get_default_settings()
    {
        $settings = Settings::get_instance();
        return \apply_filters('ngg_pro_list_album_default_settings', [
            // Default child gallery view
            'gallery_display_type' => defined('NGG_PRO_THUMBNAIL_GRID') ? NGG_PRO_THUMBNAIL_GRID : NGG_BASIC_THUMBNAILS,
            // Basic style settings
            'background_color' => '#FFFFFF',
            'border_color' => '#CCCCCC',
            'border_size' => 1,
            'caption_color' => '#333333',
            'display_type_view' => 'default',
            'enable_breadcrumbs' => 1,
            'enable_descriptions' => 0,
            'padding' => 20,
            'spacing' => 10,
            // Thumbnail dimensions
            'open_gallery_in_lightbox' => 0,
            'override_thumbnail_settings' => 0,
            'thumbnail_crop' => $settings->get('thumbfix'),
            'thumbnail_height' => $settings->get('thumbheight'),
            'thumbnail_quality' => $settings->get('thumbquality'),
            'thumbnail_watermark' => 0,
            'thumbnail_width' => $settings->get('thumbwidth'),
            // Grid albums do not share a caption_size
            'caption_size' => 18,
            // Grid albums cannot customize description size and color
            'description_color' => '#33333',
            'description_size' => 13,
        ]);
    }
}