<?php

namespace Imagely\NGGPro\DisplayTypes;

use Imagely\NGGPro\DisplayType\Controller as ParentController;
use Imagely\NGG\DataMappers\Image as ImageMapper;
use Imagely\NGG\DataStorage\Manager as StorageManager;
use Imagely\NGG\DynamicThumbnails\Manager as DynamicThumbnailsManager;
use Imagely\NGGPro\Bootloader;
use Imagely\NGGPro\Display\StaticAssets;
use Imagely\NGGPro\Display\View;
use Imagely\NGG\DataTypes\DisplayedGallery;
use Imagely\NGG\Settings\Settings;
use Imagely\NGG\Util\Router;
class Film extends ParentController
{
    /** @var array Allows index_action() and enqueue_frontend_resources() shared access to $displayed_gallery->get_included_entities() results */
    public static $cached_images = [];
    /**
     * @param DisplayedGallery $displayed_gallery
     * @return array
     */
    public function prepare_display($displayed_gallery)
    {
        $id = $displayed_gallery->id();
        if (!empty(self::$cached_images[$id])) {
            return self::$cached_images[$id];
        }
        $router = Router::get_instance();
        $display_settings = $displayed_gallery->display_settings;
        $current_page = (int) $router->get_parameter('nggpage', $displayed_gallery->id(), 1);
        if (!isset($display_settings['images_per_page'])) {
            $display_settings['images_per_page'] = Settings::get_instance()->get('images_per_page');
        }
        $offset = $display_settings['images_per_page'] * ($current_page - 1);
        $total = $displayed_gallery->get_entity_count();
        $images = $displayed_gallery->get_included_entities($display_settings['images_per_page'], $offset);
        if (in_array($displayed_gallery->source, ['random', 'recent'])) {
            $display_settings['disable_pagination'] = true;
        }
        if ($images) {
            if ($display_settings['images_per_page'] && !$display_settings['disable_pagination']) {
                $pagination_result = $this->create_pagination($current_page, $total, $display_settings['images_per_page']);
            }
        }
        $pagination = !empty($pagination_result['output']) ? $pagination_result['output'] : null;
        // Get named size of thumbnail images
        $thumbnail_size_name = 'thumbnail';
        if ($display_settings['override_thumbnail_settings']) {
            $dynthumbs = DynamicThumbnailsManager::get_instance();
            $dyn_params = ['width' => $display_settings['thumbnail_width'], 'height' => $display_settings['thumbnail_height']];
            if ($display_settings['thumbnail_quality']) {
                $dyn_params['quality'] = $display_settings['thumbnail_quality'];
            }
            if ($display_settings['thumbnail_crop']) {
                $dyn_params['crop'] = true;
            }
            if ($display_settings['thumbnail_watermark']) {
                $dyn_params['watermark'] = true;
            }
            $thumbnail_size_name = $dynthumbs->get_size_name($dyn_params);
        }
        // Calculate image statistics
        $stats = $this->get_entity_statistics($images, $thumbnail_size_name, true);
        $display_settings['longest'] = $stats['longest'];
        $display_settings['widest'] = $stats['widest'];
        self::$cached_images[$id] = ['display_settings' => $display_settings, 'effect_code' => $this->get_effect_code($displayed_gallery), 'id' => $id, 'images' => $images, 'pagination' => $pagination, 'storage' => StorageManager::get_instance(), 'thumbnail_size_name' => $thumbnail_size_name];
        return self::$cached_images[$id];
    }
    /**
     * Returns the longest and widest dimensions from a list of entities. Only used by Pro Film.
     *
     * @param $entities
     * @param $named_size
     * @param bool       $style_images Unused
     * @return array
     */
    public function get_entity_statistics($entities, $named_size, $style_images = false)
    {
        $longest = $widest = 0;
        $storage = StorageManager::get_instance();
        $image_mapper = ImageMapper::get_instance();
        foreach ($entities as $entity) {
            $image = null;
            if (isset($entity->pid)) {
                $image = $entity;
            } elseif (isset($entity->previewpic)) {
                $image = $image_mapper->find($entity->previewpic);
            }
            // Once we have the image, get its dimensions
            if ($image) {
                $dimensions = $storage->get_image_dimensions($image, $named_size);
                if ($dimensions['width'] > $widest) {
                    $widest = $dimensions['width'];
                }
                if ($dimensions['height'] > $longest) {
                    $longest = $dimensions['height'];
                }
            }
        }
        return ['longest' => $longest, 'widest' => $widest];
    }
    /**
     * @param DisplayedGallery $displayed_gallery
     * @param bool             $return
     * @return string
     */
    public function index_action($displayed_gallery, $return = false)
    {
        $params = $this->prepare_display_parameters($displayed_gallery, $this->prepare_display($displayed_gallery));
        $dyncss_params = $params['display_settings'];
        $dyncss_params['id'] = $params['id'];
        $dyncss = new View('DisplayTypes/Film/dyncss', $dyncss_params, 'photocrati-nextgen_pro_film#nextgen_pro_film_dyncss');
        $view = new View('DisplayTypes/Film/default', $params, 'photocrati-nextgen_pro_film#nextgen_pro_film');
        // Render & remove spaces between HTML tags
        $retval = preg_replace('~>\\s*\\n\\s*<~', '><', $dyncss->render(true) . $view->render(true));
        if (!$return) {
            echo $retval;
        }
        return $retval;
    }
    /**
     * @return false
     */
    function is_cachable()
    {
        return false;
    }
    function enqueue_frontend_resources($displayed_gallery)
    {
        parent::enqueue_frontend_resources($displayed_gallery);
        $this->enqueue_pagination_resources();
        \wp_enqueue_style('nextgen_pro_film', StaticAssets::get_url('DisplayTypes/Film/style.css', 'photocrati-nextgen_pro_film#nextgen_pro_film.css'), [], Bootloader::$script_version);
    }
    public function get_preview_image_url()
    {
        return StaticAssets::get_url('DisplayTypes/Film/preview.jpg');
    }
    public function get_default_settings()
    {
        $settings = Settings::get_instance();
        return \apply_filters('ngg_pro_film_default_settings', ['alttext_display' => 0, 'alttext_font_color' => '', 'alttext_font_size' => '', 'border_color' => '#CCCCCC', 'border_size' => 1, 'description_display' => 0, 'description_font_color' => '', 'description_font_size' => '', 'disable_pagination' => 0, 'display_type_view' => 'default', 'frame_color' => '#FFFFFF', 'frame_size' => 20, 'image_spacing' => 5, 'images_per_page' => $settings->get('galImages'), 'ngg_triggers_display' => 'always', 'thumbnail_crop' => 0, 'thumbnail_height' => $settings->get('thumbheight'), 'thumbnail_quality' => $settings->get('thumbquality'), 'thumbnail_watermark' => 0, 'thumbnail_width' => $settings->get('thumbwidth'), 'override_thumbnail_settings' => 0]);
    }
    function install($reset = false)
    {
        $this->install_display_type(NGG_PRO_FILM, ['aliases' => ['film', 'pro_film'], 'default_source' => 'galleries', 'entity_types' => ['image'], 'hidden_from_ui' => false, 'title' => __('NextGEN Pro Film', 'nextgen-gallery-pro'), 'view_order' => NGG_DISPLAY_PRIORITY_BASE + NGG_DISPLAY_PRIORITY_STEP * 10 + 30, 'settings' => $this->get_default_settings()], $reset);
    }
}