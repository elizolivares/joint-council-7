<?php

/** @remove-for-nextgen-starter */
namespace Imagely\NGGPro\DisplayTypes;

use Imagely\NGGPro\DisplayType\Controller as ParentController;
use Imagely\NGG\DataMappers\Album as AlbumMapper;
use Imagely\NGG\DisplayedGallery\Renderer as DisplayedGalleryRenderer;
use Imagely\NGG\Util\Transient as TransientManager;
use Imagely\NGGPro\Bootloader;
use Imagely\NGGPro\Display\StaticAssets;
use Imagely\NGGPro\Display\View;
use Imagely\NGG\DataTypes\Album;
use Imagely\NGG\DataTypes\DisplayedGallery;
use Imagely\NGG\Settings\Settings;
use Imagely\NGG\Util\Router;
class Search extends ParentController
{
    public static $galleries_displayed = [];
    public static $displayed_galleries_rendering = [];
    public static $alternate_displayed_galleries = [];
    function get_alternate_displayed_gallery(DisplayedGallery $displayed_gallery) : DisplayedGallery
    {
        if (!empty(self::$alternate_displayed_galleries[$displayed_gallery->id()])) {
            return self::$alternate_displayed_galleries[$displayed_gallery->id()];
        }
        $router = Router::get_instance();
        $user_search_term = $router->get_parameter('nggsearch');
        $displayed_gallery->display_settings = $this->validate_displayed_gallery_settings($displayed_gallery->display_settings);
        // Just to make the code a bit easier to read
        $displayed_gallery->display_settings = $this->validate_displayed_gallery_settings($displayed_gallery->display_settings);
        $display_settings = $displayed_gallery->display_settings;
        $gallery_ids = [];
        if ($displayed_gallery->source === 'galleries') {
            $gallery_ids = $displayed_gallery->container_ids;
        } elseif ($displayed_gallery->source === 'albums') {
            $gallery_ids = $this->get_album_children($displayed_gallery);
        }
        $params = $display_settings;
        $params['i18n'] = $this->get_i18n();
        $params['search_term'] = $user_search_term ? $user_search_term : '';
        // For browsers lacking javascript and/or search bots
        $params['form_submit_url'] = \get_page_link();
        // For most users we redirect the user to the appropriate URL when the form submit event fires
        $params['form_redirect_url'] = $router->get_routed_app()->set_parameter('nggsearch', 'ngg-search-placeholder', null, false, \get_page_link());
        $search_results = [];
        if ($user_search_term) {
            $tagfilter_param = $router->get_parameter('tagfilter');
            if ($display_settings['enable_tag_filter'] && $tagfilter_param) {
                $tagfilter_param = explode(',', $tagfilter_param);
            }
            $search_results = $this->search_images($user_search_term, $gallery_ids, $this->get_term_ids($tagfilter_param, false), $display_settings);
            $params['related_term_links'] = null;
            if ($display_settings['enable_tag_filter'] && !empty($search_results)) {
                $params['related_term_links'] = $this->get_related_terms_links($user_search_term, $search_results, $tagfilter_param ?: []);
            }
        }
        if (empty($search_results)) {
            if (!empty($user_search_term)) {
                $view = new View('GalleryDisplay/NoImagesFound', [], 'photocrati-nextgen_gallery_display#no_images_found');
                $params['gallery_display'] = $view->render(true);
            } else {
                $params['gallery_display'] = '';
            }
        } else {
            $renderer = DisplayedGalleryRenderer::get_instance();
            $new_params = ['source' => 'images', 'image_ids' => $search_results, 'order_by' => 'sortorder', 'sortorder' => $search_results, 'display_type' => $display_settings['gallery_display_type'], 'is_ecommerce_enabled' => $display_settings['is_ecommerce_enabled']];
            $new_displayed_gallery = $renderer->params_to_displayed_gallery($new_params);
            if ($new_displayed_gallery && $new_displayed_gallery->is_valid()) {
                if (is_null($new_displayed_gallery->id())) {
                    $new_displayed_gallery->id(md5(json_encode($new_displayed_gallery->get_entity())));
                }
                self::$alternate_displayed_galleries[$displayed_gallery->id()] = $new_displayed_gallery;
                $params['gallery_display'] = $renderer->render($new_displayed_gallery, true);
            }
        }
        $params = $this->prepare_display_parameters($displayed_gallery, $params);
        $view = new View('DisplayTypes/Search/default', $params, NGG_PRO_SEARCH . '#default');
        self::$displayed_galleries_rendering[$displayed_gallery->id()] = $view->render(true);
        if (!empty($new_displayed_gallery)) {
            return $new_displayed_gallery;
        }
        return $displayed_gallery;
    }
    /**
     * Does some hopefully unnecessary validation: because these variables are being used in raw SQl we want to be
     * thoroughly certain that ONLY these allowed strings are used
     */
    function validate_displayed_gallery_settings(array $settings) : array
    {
        if (!in_array($settings['order_by'], ['pid', 'galleryid', 'filename', 'imagedate'])) {
            $settings['order_by'] = 'pid';
        }
        if (!in_array(strtoupper($settings['order_direction']), ['ASC', 'DESC'])) {
            $settings['order_direction'] = 'ASC';
        }
        if (!in_array($settings['search_mode'], ['natural', 'boolean'])) {
            $settings['search_mode'] = 'natural';
        }
        return $settings;
    }
    /**
     * This effectively busts the standard template rendering cache
     *
     * @param DisplayedGallery $displayed_gallery
     */
    public function cache_action($displayed_gallery) : string
    {
        $id = $displayed_gallery->id();
        $router = Router::get_instance();
        if ($router->get_parameter('nggsearch') && !isset(self::$galleries_displayed[$id])) {
            return $this->index_action($displayed_gallery, true);
        } else {
            return '';
        }
    }
    /**
     * @param DisplayedGallery $displayed_gallery
     * @param bool             $return
     */
    public function index_action($displayed_gallery, $return = false) : string
    {
        if (isset(self::$galleries_displayed[$displayed_gallery->id()])) {
            return '';
        } else {
            self::$galleries_displayed[$displayed_gallery->id()] = true;
        }
        $router = Router::get_instance();
        $app = $router->get_routed_app();
        $user_search_term = $router->get_parameter('nggsearch');
        // In case the browser lacks javascript or is a bot the template's <form> uses the current post or page
        // URL so that we can redirect to the 'proper' search URL
        if ($user_search_term && $router->get_parameter('nggsearch-do-redirect')) {
            wp_redirect($this->set_parameter_value('nggsearch', $user_search_term, null, false, get_page_link()));
            exit;
        }
        // In case get_alternate_displayed_gallery() was not invoked during the wp_enqueue_scripts action
        if (empty(self::$displayed_galleries_rendering[$displayed_gallery->id()])) {
            $this->get_alternate_displayed_gallery($displayed_gallery);
        }
        return self::$displayed_galleries_rendering[$displayed_gallery->id()];
    }
    /**
     * Returns an array of related terms with their respective URL to filter or not filter based on those tags
     */
    public function get_related_terms_links(string $user_search_term, array $search_results = [], array $tagfilter_param = []) : array
    {
        $router = Router::get_instance();
        $app = $router->get_routed_app();
        $linked_terms = $this->get_image_terms($search_results);
        // The router's set_param_for() method will strip any space characters, so in case the user has
        // searched for a phrase we must first encode their search so results aren't ruined when adding pagination
        // or tag filters
        $user_search_term = str_replace(' ', '%20', $user_search_term);
        $tagfilter_links = [];
        // The current search is not restricted to any tags
        if (!$tagfilter_param) {
            $tagfilter_param = [];
        } else {
            // The current search is restricted to a tag(set) -- provide a 'clear all' link
            $clear_all_url = $this->set_parameter_value('nggsearch', $user_search_term, null, false, get_page_link());
            $clear_all_url = $app->remove_parameter('tagfilter', null, $clear_all_url);
            $tagfilter_links['ngg-clear-tag-filter'] = ['name' => __('Clear filters', 'nextgen-gallery-pro'), 'slug' => 'ngg-clear-tag-filter', 'type' => 'clearsearchfilters', 'url' => $clear_all_url, 'count' => 0];
        }
        foreach ($linked_terms as $linked_term) {
            // Skip linking to this term if it is exactly what the user searched for
            if (strtoupper($linked_term['name']) === strtoupper($user_search_term)) {
                continue;
            }
            if (in_array($linked_term['slug'], $tagfilter_param)) {
                // We are already filtering by this term, so generate a URL that removes it as a filterable term
                $new_list = $tagfilter_param;
                foreach ($new_list as $ndx => $new_list_item) {
                    if ($new_list_item == $linked_term['slug']) {
                        unset($new_list[$ndx]);
                    }
                }
                // There is at least one search term, so generate a URL that removes just this one term
                if (!empty($new_list)) {
                    $url_without_term = $this->set_parameter_value('nggsearch', $user_search_term, null, false, get_page_link());
                    $url_without_term = $this->set_parameter_value('tagfilter', implode(',', $new_list), null, false, $url_without_term);
                } else {
                    // With this term being removed from the filter there are no terms left: we want to link only to
                    // the base search without the tagfilter parameter present
                    $url_without_term = $this->set_parameter_value('nggsearch', $user_search_term, null, false, get_page_link());
                    $url_without_term = $app->remove_parameter('tagfilter', null, $url_without_term);
                }
                // Finally assemble the new array
                $tagfilter_links[$linked_term['slug']] = ['name' => $linked_term['name'], 'slug' => $linked_term['slug'], 'type' => 'del', 'url' => $url_without_term, 'count' => $linked_term['count']];
            } else {
                // This term is not being filtered: generate a URL that will include it in the tagfilter parameter
                $url_with_term = $this->set_parameter_value('nggsearch', $user_search_term, null, false, get_page_link());
                $new_list = $tagfilter_param;
                $new_list[] = $linked_term['slug'];
                $url_with_term = $this->set_parameter_value('tagfilter', implode(',', $new_list), null, false, $url_with_term);
                $tagfilter_links[$linked_term['slug']] = ['name' => $linked_term['name'], 'slug' => $linked_term['slug'], 'type' => 'add', 'url' => $url_with_term, 'count' => $linked_term['count']];
            }
        }
        return $tagfilter_links;
    }
    /**
     * Recursively fetches all album children and their children's children
     *
     * @param DisplayedGallery|Album $entity
     * @param bool                   $recursing
     * @return array
     */
    public function get_album_children($entity, $recursing = false)
    {
        $retval = [];
        if (!$recursing) {
            $children = $entity->get_included_entities();
        } else {
            $mapper = AlbumMapper::get_instance();
            /** @var Album $album */
            $album = $mapper->find($entity->{$entity->id_field}, true);
            $children = $album->get_galleries(true);
            foreach ($this->get_child_albums($album) as $child_album) {
                $retval = array_merge($retval, $this->get_album_children($child_album, true));
            }
        }
        foreach ($children as $child) {
            if (isset($child->is_gallery) && $child->is_gallery === '1' || get_class($child) === 'Gallery') {
                $retval[] = $child->{$child->id_field};
            } elseif (isset($child->is_album) && $child->is_album === '1' || get_class($child) === 'Album') {
                $retval = array_merge($retval, $this->get_album_children($child, true));
            }
        }
        return $retval;
    }
    public function get_child_albums($album) : array
    {
        $mapper = AlbumMapper::get_instance();
        $album_key = $mapper->get_primary_key_column();
        return $mapper->select()->where(["{$album_key} IN %s", $album->container_ids])->run_query();
    }
    public function get_i18n() : array
    {
        return ['button_label' => __('Search Images', 'nextgen-gallery-pro'), 'input_placeholder' => __('Search term', 'nextgen-gallery-pro')];
    }
    public function _array_trim(string $string) : string
    {
        return trim($string, "\"'\n\r");
    }
    public function _split_text(string $string) : array
    {
        // Added slashes can interfere with the following regex
        $string = stripslashes($string);
        // Split the words into an array if separated by a space or comma
        preg_match_all('/".*?("|$)|((?<=[\\s",+])|^)[^\\s",+]+/', $string, $matches);
        $retval = array_map([$this, '_array_trim'], $matches[0]);
        $retval = array_map('trim', $retval);
        // Include the original full string
        array_unshift($retval, $string);
        return $retval;
    }
    /**
     * @param array $terms
     * @param bool  $search Whether to match exact term names or to do a LIKE search
     * @return array
     */
    public function get_term_ids($terms = [], $search = false)
    {
        global $wpdb;
        if (!is_array($terms)) {
            return [];
        }
        $sanitized_terms = array_map('sanitize_title', $terms);
        $terms = array_unique(array_merge($sanitized_terms, $terms));
        // In case the search term is a phrase and matches a multi-word tag we only return that first match
        if (count($terms) > 2) {
            $full_term = end($terms);
            $found_full_match = $this->get_term_ids([$full_term], $search);
            if (!empty($found_full_match)) {
                return $found_full_match;
            }
        }
        if ($search) {
            $and_clauses = [];
            foreach ($terms as $ndx => $term) {
                $maybe_or = $ndx >= 1 ? ' OR ' : '';
                $and_clauses[] = $wpdb->prepare(" {$maybe_or} t.`name` LIKE %s", '%' . $wpdb->esc_like($term) . '%');
            }
            $and_clauses = implode('', $and_clauses);
            $query = "SELECT DISTINCT t.`term_id`\n                    FROM `{$wpdb->terms}` t\n                    INNER JOIN `{$wpdb->term_taxonomy}` AS tt ON t.`term_id` = tt.`term_id`\n                    WHERE t.`name` IS NOT NULL\n                    AND tt.`taxonomy` = 'ngg_tag'\n                    AND ({$and_clauses})";
        } else {
            $slug_part = rtrim(str_repeat('%s,', count($terms)), ',');
            $query = $wpdb->prepare("SELECT DISTINCT t.`term_id`\n                    FROM `{$wpdb->terms}` t\n                    INNER JOIN `{$wpdb->term_taxonomy}` AS tt ON t.`term_id` = tt.`term_id`\n                    WHERE t.`slug` IN ({$slug_part})\n                    AND tt.`taxonomy` = 'ngg_tag'", $terms);
        }
        $results = $wpdb->get_col($query);
        if (is_array($results)) {
            return $results;
        } else {
            return [];
        }
    }
    /**
     * Retrieves a list of all tags that are applied to an array of image ID
     */
    public function get_image_terms(array $image_ids = []) : array
    {
        global $wpdb;
        $id_part = rtrim(str_repeat('%d,', count($image_ids)), ',');
        $query = $wpdb->prepare("SELECT DISTINCT t.`name`, t.`slug`, t.`term_id`, tt.`count`\n                    FROM `{$wpdb->term_relationships}` tr\n                    INNER JOIN `{$wpdb->term_taxonomy}` AS tt ON tr.`term_taxonomy_id` = tt.`term_taxonomy_id`\n                    INNER JOIN `{$wpdb->terms}` AS t ON tt.`term_id` = t.`term_id`\n                    WHERE tr.`object_id` IN ({$id_part})\n                    AND tt.`taxonomy` = 'ngg_tag'\n                    ORDER BY t.`name` ASC", $image_ids);
        $results = $wpdb->get_results($query, ARRAY_A);
        if (is_array($results)) {
            return $results;
        } else {
            return [];
        }
    }
    public function search_images(string $request = '', array $gallery_ids = [], array $term_ids = [], array $display_settings = []) : array
    {
        // First determine if we have a cached version of this search
        $transient_manager = TransientManager::get_instance();
        $cache_key = $transient_manager->generate_key('frontend_image_search', [$request, $gallery_ids, $term_ids, $display_settings]);
        $cache_lookup = $transient_manager->get($cache_key, null);
        if (!is_null($cache_lookup)) {
            return $cache_lookup;
        }
        global $wpdb;
        // Assemble the WHERE clause
        $where_clause = 'WHERE `nggpictures`.`pid` IS NOT NULL';
        // Restrict search results to a subset of galleries
        if (!empty($gallery_ids)) {
            $gallery_part = rtrim(str_repeat('%d,', count($gallery_ids)), ',');
            $where_clause .= $wpdb->prepare(" AND `nggpictures`.`galleryid` IN ({$gallery_part})", $gallery_ids);
        }
        // Assemble the matching against the image alttext and/or description
        $having_select_part = '';
        $having_clause = '';
        $having_fields = [];
        if ($display_settings['search_alttext']) {
            $having_fields[] = 'alttext';
        }
        if ($display_settings['search_description']) {
            $having_fields[] = 'description';
        }
        if (!empty($having_fields)) {
            // mySQL defaults to natural language mode; unless boolean mode is requested we don't need to specify the mode
            $search_mode_part = $display_settings['search_mode'] === 'boolean' ? 'IN BOOLEAN MODE' : '';
            $having_fields = implode(',', $having_fields);
            $having_select_part = $wpdb->prepare(", MATCH({$having_fields}) AGAINST (%s {$search_mode_part}) AS `relevance`", $request);
            $minimum_relevance = floatval($display_settings['minimum_relevance']);
            if ($minimum_relevance < 0) {
                $minimum_relevance = 0;
            }
            $having_clause = "HAVING `relevance` >= {$minimum_relevance}";
        }
        // Also search images based on their assigned tags
        if ($display_settings['search_tags']) {
            // Check if each word searched for is a tag
            // Search both for individual words and whole phrase; both sanitized and raw
            $tag_list = [$request];
            $tag_list_array = $this->_split_text($request);
            if (is_array($tag_list_array) && count($tag_list_array) >= 1) {
                $tag_list = array_merge($tag_list, $tag_list_array);
            }
            $sanitized_tag_list = array_map('sanitize_title', $tag_list);
            $tag_list = array_unique(array_merge($tag_list, $sanitized_tag_list));
            // If the searched phrase is a term, then include all images with that term
            $search_term_ids = $this->get_term_ids($tag_list, true);
            if (!empty($search_term_ids)) {
                $tag_query_term_part = rtrim(str_repeat('%d,', count($search_term_ids)), ',');
                $tag_query = !empty($having_clause) ? ' OR ' : 'HAVING ';
                $tag_query .= "`nggpictures`.`pid` IN (\n                                   SELECT tr.`object_id`\n                                   FROM `{$wpdb->term_relationships}` AS tr\n                                   INNER JOIN `{$wpdb->term_taxonomy}` AS tt ON tr.`term_taxonomy_id` = tt.`term_taxonomy_id`\n                                   WHERE tt.`taxonomy` IN ('ngg_tag')\n                                   AND tt.`term_id` IN ({$tag_query_term_part}))";
                $having_clause .= $wpdb->prepare($tag_query, $search_term_ids);
            }
        }
        // Restrict results to only images with specific assigned terms
        if ($display_settings['enable_tag_filter'] && !empty($term_ids)) {
            $tag_filter_term_part = rtrim(str_repeat('%d,', count($term_ids)), ',');
            $tag_filter_having_clause = $wpdb->prepare('HAVING COUNT(*) >= %d', count($term_ids));
            $tag_filter_query = " AND `nggpictures`.`pid` IN (\n                                      SELECT tr.`object_id`\n                                      FROM `{$wpdb->term_relationships}` AS tr\n                                      INNER JOIN `{$wpdb->term_taxonomy}` AS tt ON tr.`term_taxonomy_id` = tt.`term_taxonomy_id`\n                                      WHERE tt.`taxonomy` IN ('ngg_tag')\n                                      AND tt.`term_id` IN ({$tag_filter_term_part})\n                                      GROUP BY tr.`object_id`\n                                      {$tag_filter_having_clause})";
            $where_clause .= $wpdb->prepare($tag_filter_query, $term_ids);
        }
        $order_clause = 'ORDER BY ';
        if ($display_settings['order_by_relevance'] && !empty($having_fields)) {
            $order_clause .= '`relevance` DESC, ';
        }
        $order_clause .= "`nggpictures`.`{$display_settings['order_by']}` {$display_settings['order_direction']}";
        $limit_clause = '';
        if (intval($display_settings['limit']) !== 0) {
            $limit_clause = $wpdb->prepare(' LIMIT %d', intval($display_settings['limit']));
        }
        // Build the final query
        $query = "SELECT `nggpictures`.`pid` {$having_select_part}\n                  FROM `{$wpdb->nggpictures}` AS `nggpictures`\n                  {$where_clause}\n                  {$having_clause}\n                  {$order_clause}\n                  {$limit_clause}";
        // Finally fetch the results of this insane query
        $results = $wpdb->get_col($query);
        // Cache the results for later lookups
        $transient_manager->set($cache_key, $results, NGG_DISPLAYED_GALLERY_CACHE_TTL);
        return $results;
    }
    public function enqueue_frontend_resources($displayed_gallery)
    {
        $router = Router::get_instance();
        // Prevent the Pro Lightbox from calling get_entities() when no search is provided
        if (!$router->get_parameter('nggsearch')) {
            Galleria::$localized_galleries[] = $displayed_gallery->ID();
        }
        // Normally we include this following call in every display type's enqueue_frontend_resources() method
        // however for this particular gallery it is not necessary *AND IT WILL DESTROY PERFORMANCE* !!
        // When viewing a child gallery that gallery will invoke this call_parent() method in a way that does not
        // cause page load times to increase four-fold! Do not uncomment this line.
        // parent::enqueue_frontend_resources($displayed_gallery);
        \wp_enqueue_style('nextgen_frontend_search_style', StaticAssets::get_url('DisplayTypes/Search/style.css', NGG_PRO_SEARCH . '#style.css'), ['dashicons'], Bootloader::$script_version);
        \wp_enqueue_script('nextgen_frontend_search_script', StaticAssets::get_url('DisplayTypes/Search/main.js', NGG_PRO_SEARCH . '#main.js'), [], Bootloader::$script_version, true);
    }
    public function get_default_settings() : array
    {
        return \apply_filters('ngg_pro_search_default_settings', [
            'search_alttext' => '1',
            'search_description' => '1',
            'search_tags' => '1',
            // Optional feature: allow frontend users to restrict search results by image tags
            'enable_tag_filter' => '1',
            // Which type of fulltext search to perform in mySQL
            'search_mode' => 'natural',
            // Query meta-attributes
            'limit' => '0',
            'order_by' => 'pid',
            'order_direction' => 'ASC',
            'order_by_relevance' => '1',
            'minimum_relevance' => '1',
            // The display type used to display results
            'gallery_display_type' => 'photocrati-nextgen_basic_thumbnails',
        ]);
    }
    public function get_preview_image_url() : string
    {
        return StaticAssets::get_url('DisplayTypes/Search/preview.png');
    }
    function install($reset = false)
    {
        global $wpdb;
        // Source the dbDelta() method
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';
        // DO NOT remove the definition of description/alttext or the indexes wont be added by dbDelta()
        \dbDelta("CREATE TABLE {$wpdb->prefix}ngg_pictures (\n             description mediumtext NULL,\n             alttext mediumtext NULL,\n             FULLTEXT KEY `alttext_search` (`alttext`),\n             FULLTEXT KEY `description_search` (`description`),\n             FULLTEXT KEY `combined_search` (`alttext`, `description`)\n        );");
        $this->install_display_type(NGG_PRO_SEARCH, ['title' => __('NextGen Frontend Image Search', 'nextgen-gallery-pro'), 'entity_types' => ['gallery', 'album', 'image'], 'default_source' => 'galleries', 'hidden_from_ui' => false, 'view_order' => NGG_DISPLAY_PRIORITY_BASE + NGG_DISPLAY_PRIORITY_STEP * 10 + 60, 'settings' => $this->get_default_settings(), 'aliases' => []], $reset);
    }
    function set_parameter_value($key, $value, $id = null, $use_prefix = false, $url = false)
    {
        $settings = Settings::get_instance();
        $param_slug = preg_quote($settings->get('router_param_slug'), '#');
        // it's difficult to make NextGEN's router work with spaces in parameter names without just encoding them
        // directly first; replace nggsearch's parameter's spaces with %20
        $url = preg_replace_callback("#(/{$param_slug}/.*)nggsearch--(.*)#", function ($matches) {
            return str_replace(' ', '%20', $matches[0]);
        }, $url);
        $routed_app = Router::get_instance()->get_routed_app();
        $retval = $routed_app->set_parameter_value($key, $value, $id, $use_prefix, $url);
        return $this->set_search_page_parameter($retval, $key, $value, $id, $use_prefix);
    }
    function set_search_page_parameter($retval, $key, $value = null, $id = null, $use_prefix = null)
    {
        $settings = Settings::get_instance();
        $param_slug = preg_quote($settings->get('router_param_slug'), '#');
        // Convert the nggpage parameter to a slug
        if (preg_match("#(/{$param_slug}/.*)nggsearch--(.*)#", $retval, $matches)) {
            $retval = rtrim(str_replace($matches[0], rtrim($matches[1], '/') . '/search/' . ltrim($matches[2], '/'), $retval), '/');
        }
        if (preg_match("#(/{$param_slug}/.*)tagfilter--(.*)#", $retval, $matches)) {
            $retval = rtrim(str_replace($matches[0], rtrim($matches[1], '/') . '/tagfilter/' . ltrim($matches[2], '/'), $retval), '/');
        }
        return $retval;
    }
}