<?php

/**
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\DisplayTypes;

use Imagely\NGGPro\Bootloader;
use Imagely\NGGPro\Display\StaticAssets;
use Imagely\NGG\DataTypes\DisplayedGallery;
use Imagely\NGG\Settings\Settings;
class HorizontalFilmstrip extends Galleria
{
    /**
     * @param DisplayedGallery $displayed_gallery
     */
    function enqueue_frontend_resources($displayed_gallery)
    {
        parent::enqueue_frontend_resources($displayed_gallery);
        \wp_enqueue_script('ngg_pro_horizontal_filmstrip_theme_js', StaticAssets::get_url('DisplayTypes/HorizontalFilmstrip/theme/galleria.nextgen_pro_horizontal_filmstrip.js', NGG_PRO_HORIZONTAL_FILMSTRIP . '#theme/galleria.nextgen_pro_horizontal_filmstrip.js'), ['ngg_galleria_init'], Bootloader::$script_version, true);
        \wp_enqueue_style('ngg_pro_horizontal_filmstrip_theme_css', StaticAssets::get_url('DisplayTypes/HorizontalFilmstrip/theme/galleria.nextgen_pro_horizontal_filmstrip.css', NGG_PRO_HORIZONTAL_FILMSTRIP . '#theme/galleria.nextgen_pro_horizontal_filmstrip.css'), [], Bootloader::$script_version);
    }
    public function get_preview_image_url()
    {
        return StaticAssets::get_url('DisplayTypes/HorizontalFilmstrip/preview.jpg');
    }
    public function get_default_settings()
    {
        $settings = Settings::get_instance();
        return \apply_filters('ngg_pro_horizontal_filmstrip_default_settings', ['aspect_ratio' => '1.5', 'border_color' => '#ffffff', 'border_size' => 0, 'caption_class' => 'caption_overlay_bottom', 'caption_height' => 70, 'image_crop' => 0, 'image_pan' => 1, 'image_quality' => '100', 'image_watermark' => 0, 'localize_limit' => '0', 'ngg_triggers_display' => 'always', 'override_image_settings' => 0, 'override_thumbnail_settings' => 1, 'show_captions' => 0, 'show_playback_controls' => 1, 'slideshow_speed' => 5, 'thumbnail_crop' => $settings->get('thumbfix'), 'thumbnail_height' => 90, 'thumbnail_quality' => $settings->get('thumbquality'), 'thumbnail_watermark' => 0, 'thumbnail_width' => 120, 'transition' => 'fade', 'transition_speed' => 1, 'width' => 100, 'width_unit' => '%']);
    }
    function install($reset = false)
    {
        $this->install_display_type(NGG_PRO_HORIZONTAL_FILMSTRIP, ['title' => __('NextGEN Pro Horizontal Filmstrip', 'nextgen-gallery-pro'), 'entity_types' => ['image'], 'default_source' => 'galleries', 'hidden_from_ui' => false, 'view_order' => NGG_DISPLAY_PRIORITY_BASE + NGG_DISPLAY_PRIORITY_STEP * 10 + 20, 'settings' => $this->get_default_settings(), 'aliases' => ['horizontal_filmstrip', 'pro_horizontal_filmstrip', 'nextgen_pro_horizontal_filmstrip']], $reset);
    }
}