<?php

/** @remove-for-nextgen-starter */
namespace Imagely\NGGPro\DisplayTypes;

use Imagely\NGGPro\DisplayType\Controller as ParentController;
use Imagely\NGG\DataStorage\Manager as StorageManager;
use Imagely\NGG\DynamicThumbnails\Manager as DynamicThumbnailsManager;
use Imagely\NGGPro\Bootloader;
use Imagely\NGGPro\Display\StaticAssets;
use Imagely\NGGPro\Display\View;
class BlogGallery extends ParentController
{
    function enqueue_frontend_resources($displayed_gallery)
    {
        parent::enqueue_frontend_resources($displayed_gallery);
        \wp_enqueue_style('nextgen_pro_blog_gallery', StaticAssets::get_url('DisplayTypes/BlogGallery/style.css', 'photocrati-nextgen_pro_blog_gallery#nextgen_pro_blog_gallery.css'), [], Bootloader::$script_version);
    }
    function index_action($displayed_gallery, $return = false)
    {
        // The HTML id of the gallery
        $id = 'displayed_gallery_' . $displayed_gallery->id();
        $image_size_name = 'full';
        $display_settings = $displayed_gallery->display_settings;
        if ($display_settings['override_image_settings']) {
            $dynthumbs = DynamicThumbnailsManager::get_instance();
            $dyn_params = [];
            if ($display_settings['image_quality']) {
                $dyn_params['quality'] = $display_settings['image_quality'];
            }
            if ($display_settings['image_crop']) {
                $dyn_params['crop'] = true;
            }
            if ($display_settings['image_watermark']) {
                $dyn_params['watermark'] = true;
            }
            $image_size_name = $dynthumbs->get_size_name($dyn_params);
        }
        $params = ['border_size' => $displayed_gallery->display_settings['border_size'], 'effect_code' => $this->get_effect_code($displayed_gallery), 'id' => $id, 'image_display_size' => $displayed_gallery->display_settings['image_display_size'], 'image_size_name' => $image_size_name, 'images' => $displayed_gallery->get_included_entities(), 'storage' => StorageManager::get_instance()];
        $params = $this->prepare_display_parameters($displayed_gallery, $params);
        $display_settings['id'] = 'displayed_gallery_' . $displayed_gallery->id();
        $dyncss = new View('DisplayTypes/BlogGallery/dyncss', $display_settings, 'photocrati-nextgen_pro_blog_gallery#nextgen_pro_blog_dyncss');
        $view = new View('DisplayTypes/BlogGallery/default', $params, 'photocrati-nextgen_pro_blog_gallery#nextgen_pro_blog');
        return $dyncss->render($return) . $view->render($return);
    }
    public function get_preview_image_url()
    {
        return StaticAssets::get_url('DisplayTypes/BlogGallery/preview.jpg');
    }
    public function get_default_settings()
    {
        return \apply_filters('ngg_pro_blog_gallery_default_settings', ['border_color' => '#FFFFFF', 'border_size' => 0, 'caption_location' => 'below', 'display_captions' => 0, 'display_type_view' => 'default', 'image_crop' => 0, 'image_display_size' => 800, 'image_max_height' => 0, 'image_quality' => '100', 'image_watermark' => 0, 'ngg_triggers_display' => 'always', 'override_image_settings' => 0, 'spacing' => 5]);
    }
    public function install($reset = false)
    {
        $this->install_display_type(NGG_PRO_BLOG_GALLERY, ['title' => __('NextGEN Pro Blog Style', 'nextgen-gallery-pro'), 'entity_types' => ['image'], 'default_source' => 'galleries', 'hidden_from_ui' => false, 'view_order' => NGG_DISPLAY_PRIORITY_BASE + NGG_DISPLAY_PRIORITY_STEP * 10 + 40, 'settings' => $this->get_default_settings(), 'aliases' => ['blog_gallery', 'pro_blog', 'pro_blog_gallery', 'nextgen_pro_blog', 'nextgen_pro_blog_gallery', 'nextgen_pro_blog_style']], $reset);
    }
}