<?php

/**
 * ThumbnailGrid display type for NextGEN Pro.
 *
 * @package NextGEN Pro
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\DisplayTypes;

use Imagely\NGGPro\DisplayType\Controller as ParentController;
use Imagely\NGG\DataStorage\Manager as StorageManager;
use Imagely\NGG\DynamicThumbnails\Manager as DynamicThumbnailsManager;
use Imagely\NGGPro\Bootloader;
use Imagely\NGGPro\Display\StaticAssets;
use Imagely\NGGPro\Display\View;
use Imagely\NGG\DataTypes\DisplayedGallery;
use Imagely\NGG\Settings\Settings;
use Imagely\NGG\Util\Router;
/**
 * ThumbnailGrid display type.
 */
class ThumbnailGrid extends ParentController
{
    /**
     * Enqueues frontend resources.
     *
     * @param DisplayedGallery $displayed_gallery The DisplayedGallery to display.
     * @return void
     */
    public function enqueue_frontend_resources($displayed_gallery)
    {
        parent::enqueue_frontend_resources($displayed_gallery);
        $this->enqueue_pagination_resources();
        \wp_enqueue_style('nextgen_pro_thumbnail_grid', StaticAssets::get_url('DisplayTypes/ThumbnailGrid/style.css', 'photocrati-nextgen_pro_thumbnail_grid#nextgen_pro_thumbnail_grid.css'), [], Bootloader::$script_version);
    }
    /**
     * Returns the default settings for this display type.
     *
     * @return array
     */
    public function get_default_settings() : array
    {
        $settings = Settings::get_instance();
        return \apply_filters('ngg_pro_thumbnail_grid_default_settings', ['border_color' => '#eeeeee', 'border_size' => 0, 'disable_pagination' => 0, 'display_type_view' => 'default', 'images_per_page' => $settings->get('galImages'), 'ngg_triggers_display' => 'never', 'number_of_columns' => 0, 'override_thumbnail_settings' => 0, 'spacing' => 2, 'thumbnail_crop' => $settings->get('thumbfix'), 'thumbnail_height' => $settings->get('thumbheight'), 'thumbnail_quality' => $settings->get('thumbquality'), 'thumbnail_watermark' => 0, 'thumbnail_width' => $settings->get('thumbwidth')]);
    }
    /**
     * Returns the path to this DisplayType's preview thumbnail.
     *
     * @return string
     */
    public function get_preview_image_url() : string
    {
        return StaticAssets::get_url('DisplayTypes/ThumbnailGrid/preview.jpg');
    }
    /**
     * The render action.
     *
     * @param DisplayedGallery $displayed_gallery The displayed gallery to render.
     * @param bool             $return Whether to print or return the output.
     * @return string|void
     */
    public function index_action($displayed_gallery, $return = false)
    {
        $router = Router::get_instance();
        // The HTML id of the gallery
        $id = 'displayed_gallery_' . $displayed_gallery->id();
        // Get named size of thumbnail images
        $thumbnail_size_name = 'thumbnail';
        $display_settings = $displayed_gallery->display_settings;
        if ($display_settings['override_thumbnail_settings']) {
            $dynthumbs = DynamicThumbnailsManager::get_instance();
            $dyn_params = ['width' => $display_settings['thumbnail_width'], 'height' => $display_settings['thumbnail_height']];
            if ($display_settings['thumbnail_quality']) {
                $dyn_params['quality'] = $display_settings['thumbnail_quality'];
            }
            if ($display_settings['thumbnail_crop']) {
                $dyn_params['crop'] = true;
            }
            if ($display_settings['thumbnail_watermark']) {
                $dyn_params['watermark'] = true;
            }
            $thumbnail_size_name = $dynthumbs->get_size_name($dyn_params);
        }
        $current_page = (int) $router->get_parameter('nggpage', $displayed_gallery->id(), 1);
        $offset = $display_settings['images_per_page'] * ($current_page - 1);
        $total = $displayed_gallery->get_entity_count();
        $images = $displayed_gallery->get_included_entities($display_settings['images_per_page'], $offset);
        if (in_array($displayed_gallery->source, ['random', 'recent'])) {
            $display_settings['disable_pagination'] = true;
        }
        if ($images && $display_settings['images_per_page'] && !$display_settings['disable_pagination']) {
            $pagination_result = $this->create_pagination($current_page, $total, $display_settings['images_per_page']);
        }
        $pagination = !empty($pagination_result['output']) ? $pagination_result['output'] : null;
        $params = ['images' => $images, 'storage' => StorageManager::get_instance(), 'thumbnail_size_name' => $thumbnail_size_name, 'effect_code' => $this->get_effect_code($displayed_gallery), 'id' => $id, 'pagination' => $pagination];
        $params = $this->prepare_display_parameters($displayed_gallery, $params);
        $display_settings['id'] = 'displayed_gallery_' . $displayed_gallery->id();
        $dyncss = new View('DisplayTypes/ThumbnailGrid/dyncss', $display_settings, 'photocrati-nextgen_pro_thumbnail_grid#nextgen_pro_thumbnail_grid_dyncss');
        // Render the view/template. We remove whitespace from between HTML elements lest the browser think we want
        // a space character (&nbsp;) between each image -- causing columns to appear between images.
        $view = new View('DisplayTypes/ThumbnailGrid/default', $params, 'photocrati-nextgen_pro_thumbnail_grid#nextgen_pro_thumbnail_grid');
        $retval = $dyncss->render(true);
        $retval .= preg_replace('~>\\s*\\n\\s*<~', '><', $view->render(true));
        if (!$return) {
            echo $retval;
        }
        return $retval;
    }
    /**
     * Installs the display type's custom post.
     *
     * @param bool $reset Whether to reset existing settings.
     * @return void
     */
    public function install($reset = false)
    {
        $this->install_display_type(NGG_PRO_THUMBNAIL_GRID, ['title' => __('NextGEN Pro Thumbnail Grid', 'nextgen-gallery-pro'), 'entity_types' => ['image'], 'default_source' => 'galleries', 'hidden_from_ui' => false, 'view_order' => NGG_DISPLAY_PRIORITY_BASE + NGG_DISPLAY_PRIORITY_STEP * 10, 'settings' => $this->get_default_settings(), 'aliases' => ['thumbnail', 'thumbnails', 'pro_thumbnail', 'pro_thumbnails', 'pro_thumbnail_grid', 'thumbnail_grid', 'nextgen_pro_thumbnail_grid', 'nextgen_pro_thumbnails', 'nextgen_pro_thumbnail']], $reset);
    }
}