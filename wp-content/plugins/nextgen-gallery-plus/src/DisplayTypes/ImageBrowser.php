<?php

/**
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\DisplayTypes;

use Imagely\NGGPro\DisplayType\Controller as ParentController;
use Imagely\NGG\DataMappers\Image as ImageMapper;
use Imagely\NGG\DataStorage\Manager as StorageManager;
use Imagely\NGG\DataStorage\MetaData as MetaDataStorage;
use Imagely\NGGPro\Bootloader;
use Imagely\NGGPro\Display\StaticAssets;
use Imagely\NGGPro\Display\View;
use Imagely\NGG\DataTypes\DisplayedGallery;
use Imagely\NGG\DataTypes\LegacyImage;
use Imagely\NGG\Util\Router;
class ImageBrowser extends ParentController
{
    /**
     * @param DisplayedGallery $displayed_gallery
     */
    function enqueue_frontend_resources($displayed_gallery)
    {
        parent::enqueue_frontend_resources($displayed_gallery);
        \wp_enqueue_style('nextgen_pro_imagebrowser_style', StaticAssets::get_url('DisplayTypes/ImageBrowser/style.css', NGG_PRO_IMAGEBROWSER . '#style.css'), [], Bootloader::$script_version);
        \wp_enqueue_script('nextgen_pro_imagebrowser_script', StaticAssets::get_url('DisplayTypes/ImageBrowser/main.js', NGG_PRO_IMAGEBROWSER . '#imagebrowser.js'), ['ngg_common'], Bootloader::$script_version, true);
    }
    /**
     * @param DisplayedGallery $displayed_gallery
     * @param bool             $return
     * @return string
     */
    function index_action($displayed_gallery, $return = false)
    {
        $picture_list = [];
        foreach ($displayed_gallery->get_included_entities() as $image) {
            $picture_list[$image->{$image->id_field}] = $image;
        }
        if ($picture_list) {
            $retval = $this->render_image_browser($displayed_gallery, $picture_list);
        } else {
            $retval = '<p>' . \esc_html_e('no images were found', 'nextgen-gallery-pro') . '</p>';
        }
        if ($return) {
            return $retval;
        } else {
            echo $retval;
            return '';
        }
    }
    /**
     * Returns the rendered template of an image browser display
     *
     * @param DisplayedGallery $displayed_gallery
     * @param array            $picture_list
     * @return string Rendered HTML (probably)
     */
    function render_image_browser($displayed_gallery, $picture_list)
    {
        $display_settings = $displayed_gallery->display_settings;
        $storage = StorageManager::get_instance();
        $imap = ImageMapper::get_instance();
        $router = Router::get_instance();
        $application = $router->get_routed_app();
        // The pid may be a slug; so we must track it & the slug target's database ID
        $pid = $router->get_parameter('pid');
        $numeric_pid = null;
        // makes the upcoming which-image-am-I loop easier
        $picture_array = [];
        foreach ($picture_list as $picture) {
            $picture_array[] = $picture->{$imap->get_primary_key_column()};
        }
        // Determine which image in the list we need to display
        if (!empty($pid)) {
            if (is_numeric($pid) && !empty($picture_list[$pid])) {
                $numeric_pid = intval($pid);
            } else {
                // in the case it's a slug we need to search for the pid
                foreach ($picture_list as $key => $picture) {
                    if ($picture->image_slug == $pid) {
                        $numeric_pid = $key;
                        break;
                    }
                }
            }
        } else {
            reset($picture_array);
            $numeric_pid = current($picture_array);
        }
        // get ids to the next and previous images
        $total = count($picture_array);
        $key = array_search($numeric_pid, $picture_array);
        if (!$key) {
            $numeric_pid = reset($picture_array);
            $key = key($picture_array);
        }
        // for "viewing image #13 of $total"
        $picture_list_pos = $key + 1;
        // Our image to display
        // TODO: Remove the use of LegacyImage type
        $picture = new LegacyImage($imap->find($numeric_pid), $displayed_gallery, true);
        $picture = \apply_filters('ngg_image_object', $picture, $numeric_pid);
        // determine URI to the next & previous images
        $back_pid = $key >= 1 ? $picture_array[$key - 1] : end($picture_array);
        // 'show' is set when using the imagebrowser as an alternate view to a thumbnail or slideshow
        // for which the basic-gallery module will rewrite the show parameter into existence as long as 'image'
        // is set. We remove 'show' here so navigation appears fluid.
        $current_url = $application->get_routed_url();
        $prev_image_link = $application->set_parameter_value('pid', $picture_list[$back_pid]->image_slug, null, false, $current_url);
        $prev_image_link = $application->remove_parameter('show', $displayed_gallery->id(), $prev_image_link);
        $next_pid = $key < $total - 1 ? $picture_array[$key + 1] : reset($picture_array);
        $next_image_link = $application->set_parameter_value('pid', $picture_list[$next_pid]->image_slug, null, false, $current_url);
        $next_image_link = $application->remove_parameter('show', $displayed_gallery->id(), $next_image_link);
        // css class
        $anchor = 'ngg-imagebrowser-' . $displayed_gallery->id() . '-' . (!\get_the_ID() ? 0 : \get_the_ID());
        // try to read EXIF data, but fallback to the db presets
        $meta = new MetaDataStorage($picture);
        $meta->sanitize();
        $meta_results = ['exif' => $meta->get_EXIF(), 'iptc' => $meta->get_IPTC(), 'xmp' => $meta->get_XMP(), 'db' => $meta->get_saved_meta()];
        $meta_results['exif'] = !$meta_results['exif'] ? $meta_results['db'] : $meta_results['exif'];
        // disable triggers IF we're rendering inside of an ajax-pagination request; var set in common.js
        if (!empty($_POST['ajax_referrer'])) {
            $displayed_gallery->display_settings['ngg_triggers_display'] = 'never';
        }
        if (!empty($display_settings['template']) && $display_settings['template'] != 'default') {
            $picture->href_link = $picture->get_href_link();
            $picture->previous_image_link = $prev_image_link;
            $picture->previous_pid = $back_pid;
            $picture->next_image_link = $next_image_link;
            $picture->next_pid = $next_pid;
            $picture->number = $picture_list_pos;
            $picture->total = $total;
            $picture->anchor = $anchor;
            return $this->legacy_render($display_settings['template'], ['image' => $picture, 'meta' => $meta, 'exif' => $meta_results['exif'], 'iptc' => $meta_results['iptc'], 'xmp' => $meta_results['xmp'], 'db' => $meta_results['db'], 'displayed_gallery' => $displayed_gallery], true, 'imagebrowser');
        } else {
            $params = $display_settings;
            $params['anchor'] = $anchor;
            $params['image'] = $picture;
            $params['storage'] =& $storage;
            $params['previous_pid'] = $back_pid;
            $params['next_pid'] = $next_pid;
            $params['number'] = $picture_list_pos;
            $params['total'] = $total;
            $params['previous_image_link'] = $prev_image_link;
            $params['next_image_link'] = $next_image_link;
            $params['effect_code'] = $this->get_effect_code($displayed_gallery);
            $params = $this->prepare_display_parameters($displayed_gallery, $params);
            $view = new View('DisplayTypes/ImageBrowser/default', $params, NGG_PRO_IMAGEBROWSER . '#nextgen_pro_imagebrowser');
            return $view->render(true);
        }
    }
    public function get_preview_image_url()
    {
        return StaticAssets::get_url('DisplayTypes/ImageBrowser/preview.jpg');
    }
    public function get_default_settings()
    {
        return \apply_filters('ngg_pro_imagebrowser_default_settings', ['ajax_pagination' => '1', 'display_type_view' => 'default', 'ngg_triggers_display' => 'never']);
    }
    function install($reset = false)
    {
        $this->install_display_type(NGG_PRO_IMAGEBROWSER, ['title' => __('NextGEN Pro ImageBrowser', 'nextgen-gallery-pro'), 'entity_types' => ['image'], 'hidden_from_ui' => false, 'default_source' => 'galleries', 'view_order' => NGG_DISPLAY_PRIORITY_BASE + NGG_DISPLAY_PRIORITY_STEP * 10 + 15, 'settings' => $this->get_default_settings(), 'aliases' => ['pro_imagebrowser', 'nextgen_pro_imagebrowser']], $reset);
    }
}