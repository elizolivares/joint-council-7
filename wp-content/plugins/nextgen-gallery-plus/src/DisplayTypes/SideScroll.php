<?php

/**
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\DisplayTypes;

use Imagely\NGGPro\DisplayType\Controller as ParentController;
use Imagely\NGG\DataStorage\Manager as StorageManager;
use Imagely\NGGPro\Bootloader;
use Imagely\NGGPro\Display\StaticAssets;
use Imagely\NGGPro\Display\View;
class SideScroll extends ParentController
{
    function enqueue_frontend_resources($displayed_gallery)
    {
        parent::enqueue_frontend_resources($displayed_gallery);
        \wp_enqueue_script('jquery-easing', StaticAssets::get_url('DisplayTypes/SideScroll/jquery.easing.1.4.1.js', NGG_PRO_SIDESCROLL . '#jquery.easing.1.4.1.js'), ['jquery'], '1.4.1', true);
        \wp_enqueue_script('jquery-scrollto', StaticAssets::get_url('DisplayTypes/SideScroll/jquery.scrollTo.js', NGG_PRO_SIDESCROLL . '#jquery.scrollTo.js'), ['jquery'], '2.1.2', true);
        \wp_enqueue_script('jquery-touchswipe', StaticAssets::get_url('DisplayTypes/SideScroll/jquery.touchSwipe.js', NGG_PRO_SIDESCROLL . '#jquery.touchSwipe.js'), ['jquery'], '1.6.18', true);
        \wp_enqueue_script('spin', StaticAssets::get_url('DisplayTypes/SideScroll/spin.js', NGG_PRO_SIDESCROLL . '#spin.js'), ['jquery'], '2.3.2', true);
        // Enqueue sidescroll.js (a fork of portfolio.js)
        \wp_enqueue_script('sidescroll', StaticAssets::get_url('DisplayTypes/SideScroll/sidescroll.js', NGG_PRO_SIDESCROLL . '#sidescroll.js'), ['jquery', 'jquery-easing', 'nextgen_pro_captions_imagesloaded', 'jquery-scrollto', 'jquery-touchswipe', 'spin'], Bootloader::$script_version, true);
        // Finally enqueue the JS to initialize portfolio.js and localize some vars
        \wp_enqueue_script('ngg_pro_sidescroll_js', StaticAssets::get_url('DisplayTypes/SideScroll/main.js', NGG_PRO_SIDESCROLL . '#nextgen_pro_sidescroll.js'), ['jquery', 'underscore', 'sidescroll', 'nextgen_pro_captions_imagesloaded'], Bootloader::$script_version, true);
        // Enqueue the associated stylesheet
        \wp_enqueue_style('nextgen_pro_sidescroll', StaticAssets::get_url('DisplayTypes/SideScroll/style.css', 'photocrati-nextgen_pro_sidescroll#nextgen_pro_sidescroll.css'), [], Bootloader::$script_version);
        // The Sidescroll display is mostly generated in javascript using a forked version of an unmaintained JS
        // library which does not play well with having animations displaying before the gallery is built.
        \Imagely\NGGPro\Display\Animations\Manager::enqueue_settings_for_displayed_gallery($displayed_gallery);
    }
    public function get_default_settings()
    {
        return \apply_filters('ngg_pro_sidescroll_default_settings', ['display_type_view' => 'default', 'height' => 400]);
    }
    public function get_preview_image_url()
    {
        return StaticAssets::get_url('DisplayTypes/SideScroll/preview.jpg');
    }
    public function index_action($displayed_gallery, $return = false)
    {
        $params = ['effect_code' => $this->get_effect_code($displayed_gallery), 'id' => $displayed_gallery->id(), 'images' => $displayed_gallery->get_included_entities(), 'storage' => StorageManager::get_instance(), 'thumbnail_size_name' => 'full'];
        $params = $this->prepare_display_parameters($displayed_gallery, $params);
        $view = new View('DisplayTypes/SideScroll/default', $params, 'photocrati-nextgen_pro_sidescroll#nextgen_pro_sidescroll');
        $retval = preg_replace('~>\\s*\\n\\s*<~', '><', $view->render(true));
        if (!$return) {
            echo $retval;
        }
        return $retval;
    }
    public function install($reset = false)
    {
        $this->install_display_type(NGG_PRO_SIDESCROLL, ['title' => __('NextGEN Pro Sidescroll', 'nextgen-gallery-pro'), 'entity_types' => ['image'], 'default_source' => 'galleries', 'hidden_from_ui' => false, 'view_order' => NGG_DISPLAY_PRIORITY_BASE + NGG_DISPLAY_PRIORITY_STEP * 10 + 30, 'settings' => $this->get_default_settings(), 'aliases' => ['sidescroll', 'pro_sidescroll']], $reset);
    }
}