<?php

namespace Imagely\NGGPro\DisplayTypes;

use Imagely\NGGPro\DisplayType\Controller as ParentController;
use Imagely\NGG\DynamicThumbnails\Manager as DynamicThumbnailsManager;
use Imagely\NGGPro\Bootloader;
use Imagely\NGGPro\Display\StaticAssets;
use Imagely\NGGPro\Display\View;
use Imagely\NGG\Display\DisplayManager;
class Mosaic extends ParentController
{
    function enqueue_frontend_resources($displayed_gallery)
    {
        $size_name = DynamicThumbnailsManager::get_instance()->get_size_name(['height' => $displayed_gallery->display_settings['row_height'] * 2, 'crop' => false]);
        $displayed_gallery->display_settings['override_image_settings'] = true;
        $displayed_gallery->display_settings['override_image_size_name'] = $size_name;
        parent::enqueue_frontend_resources($displayed_gallery);
        Galleria::enqueue_entities($displayed_gallery, 0, $size_name);
        \wp_enqueue_style('ngg_pro_mosaic_style', StaticAssets::get_url('DisplayTypes/Mosaic/style.css', NGG_PRO_MOSAIC . '#style.css'), [], Bootloader::$script_version);
        \wp_enqueue_script('ngg_pro_mosaic_base', StaticAssets::get_url('DisplayTypes/Mosaic/justified-gallery-3.8.1-modded.js', NGG_PRO_MOSAIC . '#justified-gallery-3.8.1-modded.js'), ['jquery'], '3.8.1', true);
        \wp_enqueue_script('ngg_pro_mosaic_script', StaticAssets::get_url('DisplayTypes/Mosaic/main.js', NGG_PRO_MOSAIC . '#mosaic_init.js'), ['ngg_pro_mosaic_base', 'underscore', 'ngg_waitforimages'], Bootloader::$script_version, true);
        DisplayManager::add_script_data('ngg_common', 'galleries.gallery_' . $displayed_gallery->id() . '.mosaic_effect_code', $this->get_effect_code($displayed_gallery), false);
    }
    public function get_default_settings()
    {
        return \apply_filters('ngg_pro_mosaic_default_settings', ['display_type_view' => 'default', 'last_row' => 'justify', 'lazy_load_batch' => '15', 'lazy_load_enable' => '1', 'lazy_load_initial' => '35', 'localize_limit' => '0', 'margins' => '5', 'row_height' => '180']);
    }
    public function get_preview_image_url()
    {
        return StaticAssets::get_url('DisplayTypes/Mosaic/preview.jpg');
    }
    function index_action($displayed_gallery, $return = false)
    {
        $params = $this->prepare_display_parameters($displayed_gallery, []);
        $view = new View('DisplayTypes/Mosaic/default', $params, NGG_PRO_MOSAIC . '#default');
        return $view->render($return);
    }
    function install($reset = false)
    {
        $this->install_display_type(NGG_PRO_MOSAIC, ['title' => __('NextGen Pro Mosaic', 'nextgen-gallery-pro'), 'entity_types' => ['image'], 'default_source' => 'galleries', 'hidden_from_ui' => false, 'view_order' => NGG_DISPLAY_PRIORITY_BASE + NGG_DISPLAY_PRIORITY_STEP * 10 + 55, 'settings' => $this->get_default_settings(), 'aliases' => ['mosaic', 'pro_mosaic', 'nextgen_pro_mosaic']], $reset);
    }
}