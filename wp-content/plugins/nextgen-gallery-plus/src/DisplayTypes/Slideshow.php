<?php

/**
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\DisplayTypes;

use Imagely\NGGPro\Bootloader;
use Imagely\NGGPro\Display\StaticAssets;
use Imagely\NGG\DataTypes\DisplayedGallery;
class Slideshow extends Galleria
{
    /**
     * @param DisplayedGallery $displayed_gallery
     */
    function enqueue_frontend_resources($displayed_gallery)
    {
        parent::enqueue_frontend_resources($displayed_gallery);
        \wp_enqueue_script('ngg_pro_slideshow_theme_js', StaticAssets::get_url('DisplayTypes/Slideshow/theme/galleria.nextgen_pro_slideshow.js', NGG_PRO_SLIDESHOW . '#theme/galleria.nextgen_pro_slideshow.js'), ['ngg_galleria_init'], Bootloader::$script_version, true);
        \wp_enqueue_style('ngg_pro_slideshow_theme_css', StaticAssets::get_url('DisplayTypes/Slideshow/theme/galleria.nextgen_pro_slideshow.css', NGG_PRO_SLIDESHOW . '#theme/galleria.nextgen_pro_slideshow.css'), [], Bootloader::$script_version);
    }
    public function get_preview_image_url()
    {
        return StaticAssets::get_url('DisplayTypes/Slideshow/preview.jpg');
    }
    public function get_default_settings()
    {
        return \apply_filters('ngg_pro_slideshow_default_settings', ['aspect_ratio' => '1.5', 'border_color' => '#ffffff', 'border_size' => 0, 'caption_class' => 'caption_overlay_bottom', 'caption_height' => 70, 'image_crop' => 0, 'image_pan' => 1, 'localize_limit' => '0', 'ngg_triggers_display' => 'always', 'show_captions' => 0, 'show_playback_controls' => 1, 'slideshow_speed' => 5, 'transition' => 'fade', 'transition_speed' => 1, 'width' => 100, 'width_unit' => '%']);
    }
    function install($reset = false)
    {
        $this->install_display_type(NGG_PRO_SLIDESHOW, ['title' => __('NextGEN Pro Slideshow', 'nextgen-gallery-pro'), 'entity_types' => ['image'], 'default_source' => 'galleries', 'hidden_from_ui' => false, 'view_order' => NGG_DISPLAY_PRIORITY_BASE + NGG_DISPLAY_PRIORITY_STEP * 10 + 10, 'settings' => $this->get_default_settings(), 'aliases' => ['slideshow', 'pro_slideshow', 'nextgen_pro_slideshow']], $reset);
    }
}