<?php

/** @remove-for-nextgen-starter */
namespace Imagely\NGGPro\DataMappers;

use Imagely\NGG\DataMappers\DisplayType as ParentMapper;
use Imagely\NGGPro\Display\HoverCaptions;
class DisplayType extends ParentMapper
{
    function set_defaults($entity)
    {
        parent::set_defaults($entity);
        if (in_array($entity->name, HoverCaptions::get_supported_display_types())) {
            $this->set_default_value($entity, 'settings', 'captions_animation', 'slideup');
            $this->set_default_value($entity, 'settings', 'captions_display_description', true);
            $this->set_default_value($entity, 'settings', 'captions_display_sharing', true);
            $this->set_default_value($entity, 'settings', 'captions_display_title', true);
            $this->set_default_value($entity, 'settings', 'captions_enabled', false);
        }
    }
}