<?php

/**
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\DataMappers;

use Imagely\NGGPro\DataTypes\Comment as CommentType;
use Imagely\NGG\DataMapper\WPPostDriver;
use Imagely\NGG\Settings\Settings;
class Comment extends WPPostDriver
{
    public static $instance;
    public $model_class = '\\Imagely\\NGGPro\\DataTypes\\Comment';
    /**
     * @return Comment
     */
    public static function get_instance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new Comment();
        }
        return self::$instance;
    }
    function __construct($object_name = '')
    {
        parent::__construct('photocrati-comments');
    }
    /**
     * @param string $name
     * @param bool   $model
     * @return CommentType
     */
    function find_by_post_title($name, $model = false)
    {
        $retval = null;
        $this->select();
        $this->where(['post_title = %s', $name]);
        $results = $this->run_query(false, $model);
        if ($results) {
            $retval = $results[0];
        }
        return $retval;
    }
    function find_or_create($type, $id, $referer = false)
    {
        $name = $this->get_stub($type, $id);
        $post = $this->find_by_post_title($name, true);
        $settings = Settings::get_instance();
        /** @var array $lightbox_settings */
        $lightbox_settings = $settings->get('ngg_pro_lightbox');
        // Do not create new comment CPT when it is not enabled in the Pro Lightbox
        if (!$post && $lightbox_settings['enable_comments']) {
            $post = new CommentType();
            $post->name = $name;
            $post->post_title = $name;
            $post->comment_status = 'open';
            $post->post_status = 'publish';
            $post->post_type = 'comments';
            $post->post_excerpt = $referer;
            $this->save($post);
            $post = $this->find_by_post_title($name, true);
        }
        return $post;
    }
    function get_stub($type, $id)
    {
        return sprintf(__('NextGEN Comment Link - %1$s - %2$s', 'nextgen-gallery-pro'), $type, $id);
    }
}