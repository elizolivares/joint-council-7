<?php

/**
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\Lightbox;

use Imagely\NGG\Settings\Settings;
use Imagely\NGG\Util\Serializable;
class Installer
{
    function install($reset = false)
    {
        $settings = Settings::get_instance();
        $defaults = [
            'background_color' => '#ffffff',
            'carousel_background_color' => '',
            'carousel_text_color' => '#7a7a7a',
            'display_captions' => 0,
            'display_carousel' => 1,
            'display_comments' => 0,
            'enable_carousel' => 'always',
            'enable_comments' => 1,
            'enable_routing' => 1,
            'enable_sharing' => 1,
            'enable_twitter_cards' => 0,
            'facebook_app_id' => '',
            'icon_background' => '#444444',
            'icon_background_enabled' => 1,
            'icon_background_rounded' => 0,
            'icon_color' => '#ffffff',
            'image_crop' => 'false',
            // it is important that this not be a number zero
            'image_pan' => 0,
            'interaction_pause' => 1,
            'localize_limit' => 100,
            'overlay_icon_color' => '',
            'padding' => '0',
            'padding_unit' => 'px',
            'router_slug' => 'gallery',
            'sidebar_background_color' => '',
            'sidebar_button_background' => '',
            'sidebar_button_color' => '',
            'slideshow_speed' => 5,
            'style' => 'white',
            'touch_transition_effect' => 'slide',
            'transition_effect' => 'slide',
            'transition_speed' => 0.4,
            'twitter_username' => '',
        ];
        // Create settings array
        if (!$settings->exists('ngg_pro_lightbox')) {
            $settings->set('ngg_pro_lightbox', []);
        }
        $ngg_pro_lightbox = $settings->get('ngg_pro_lightbox');
        // Need migration logic from custom post type
        global $wpdb;
        $row = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->posts} WHERE post_type = 'lightbox_library' AND post_title = %s", NGG_PRO_LIGHTBOX));
        if ($row) {
            $row->post_content = Serializable::unserialize($row->post_content);
            $ngg_pro_lightbox = $row->post_content['display_settings'];
            @\wp_delete_post($row->ID, true);
        }
        // Set defaults
        foreach ($defaults as $key => $value) {
            if (!array_key_exists($key, $ngg_pro_lightbox)) {
                $ngg_pro_lightbox[$key] = $value;
            }
        }
        // Save the data
        $settings->set('ngg_pro_lightbox', $ngg_pro_lightbox);
    }
    function uninstall($hard = false)
    {
        $settings = Settings::get_instance();
        if ($hard) {
            $settings->delete('ngg_pro_lightbox');
            $settings->save();
        }
    }
}