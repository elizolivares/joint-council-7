<?php

/**
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\Lightbox;

use Imagely\NGG\DataMappers\Image as ImageMapper;
use Imagely\NGG\DataStorage\Manager as StorageManager;
use Imagely\NGGPro\Display\View;
use Imagely\NGG\Display\LightboxManager;
use Imagely\NGG\Util\Router;
class OpenGraphController
{
    // /nextgen-share/{url}/{slug}
    public static function index_action($return = false)
    {
        \wp_dequeue_script('photocrati_ajax');
        \wp_dequeue_script('frame_event_publisher');
        \wp_dequeue_script('jquery');
        \wp_dequeue_style('nextgen_gallery_related_images');
        $router = Router::get_instance();
        $img_mapper = ImageMapper::get_instance();
        $image_id = $router->get_parameter('image_id');
        if ($image = $img_mapper->find($image_id)) {
            $displayed_gallery_id = $router->get_parameter('displayed_gallery_id');
            // Template parameters
            $params = ['img' => $image];
            // Get the url & dimensions
            $named_size = $router->get_parameter('named_size');
            $storage = StorageManager::get_instance();
            $dimensions = $storage->get_image_dimensions($image, $named_size);
            $image->url = $storage->get_image_url($image, $named_size, true);
            $image->width = $dimensions['width'];
            $image->height = $dimensions['height'];
            // Generate the lightbox url
            $lightboxes = LightboxManager::get_instance();
            $lightbox = $lightboxes->get(NGG_PRO_LIGHTBOX);
            $uri = urldecode($router->get_parameter('uri'));
            $lightbox_slug = $lightbox->values['nplModalSettings']['router_slug'];
            $qs = self::get_querystring();
            if ($qs) {
                $lightbox_url = $router->get_url('/', false, 'root');
                $lightbox_url .= '?' . $qs;
            } else {
                $lightbox_url = $router->get_url($uri, false, 'root');
                $lightbox_url .= '/';
            }
            $params['lightbox_url'] = "{$lightbox_url}#{$lightbox_slug}/{$displayed_gallery_id}/{$image_id}";
            // Add the blog name
            $params['blog_name'] = \get_bloginfo('name');
            if ($lightbox->values['nplModalSettings']['enable_twitter_cards']) {
                $params['enable_twitter_cards'] = $lightbox->values['nplModalSettings']['enable_twitter_cards'];
                $params['twitter_username'] = $lightbox->values['nplModalSettings']['twitter_username'];
            }
            // Add routed url
            $protocol = $router->is_https() ? 'https://' : 'http://';
            $params['routed_url'] = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            // Render the opengraph metadata
            $time = strtotime('+1 day');
            if (!headers_sent()) {
                header('Expires: ' . strftime('%a, %d %b %Y %H:%M:%S %Z', $time));
            }
            $view = new View('OpenGraph/default', $params, 'photocrati-nextgen_pro_lightbox#opengraph');
            $view->render();
            // Prevent themes from conflicting and displaying a second set of OpenGraph tags
            exit;
        } else {
            header(__('Status: 404 Image not found', 'nextgen-gallery-pro'));
            echo __('Image not found', 'nextgen-gallery-pro');
            exit;
        }
    }
    /**
     * The querystring contains the URI segment to return to, but possibly other querystring data that should be included
     * in the lightbox url. This function returns the querystring without the return data
     */
    public static function get_querystring()
    {
        // The URI parameter is handled independently of this method
        $uri = preg_replace('/uri=[^&]+&?/', '', Router::get_instance()->get_querystring());
        // fbclid is provided by facebook and clutters our otherwise pretty url
        return preg_replace('/fbclid=[^&]+&?/', '', $uri);
    }
}