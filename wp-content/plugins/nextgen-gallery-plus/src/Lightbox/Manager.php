<?php

/**
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\Lightbox;

use Imagely\NGG\Util\Installer as InstallManager;
use Imagely\NGGPro\DisplayTypes\Galleria;
use Imagely\NGGPro\Display\StaticAssets;
use Imagely\NGG\DataTypes\Lightbox;
use Imagely\NGG\Display\DisplayManager;
use Imagely\NGG\Display\LightboxManager;
use Imagely\NGG\DisplayedGallery\TriggerManager;
use Imagely\NGG\Settings\Settings;
use Imagely\NGG\Util\Router;
class Manager
{
    static $galleries_displayed = [];
    protected static $components = [];
    public static function register_hooks()
    {
        $self = new self();
        \add_filter('ngg_effect_code', [$self, 'get_effect_code'], 10, 2);
        \add_action('ngg_display_type_controller_enqueue_frontend_resources', [$self, 'enqueue_frontend_resources']);
        \add_action('ngg_registered_default_lightboxes', [$self, 'register_lightbox']);
        \add_action('wp_enqueue_scripts', [$self, 'maybe_enqueue_fontawesome']);
        // The Basic & Pro Albums emit this event when creating a child gallery for the Pro Lightbox to open.
        \add_action('ngg_albums_enqueue_child_entity_data', function ($displayed_gallery) {
            Galleria::enqueue_entities($displayed_gallery);
        });
        \add_action('init', function () {
            // These must be registered before we begin adding the triggers themselves
            $manager = TriggerManager::get_instance();
            $handler = '\\Imagely\\NGG\\DisplayedGallery\\ImageTriggerHandler';
            $manager->register_display_type_handler('photocrati-nextgen_pro_thumbnail_grid', $handler);
            $manager->register_display_type_handler('photocrati-nextgen_pro_blog_gallery', $handler);
            $manager->register_display_type_handler('photocrati-nextgen_pro_film', $handler);
            $manager->add(NGG_PRO_LIGHTBOX_TRIGGER, 'Imagely\\NGGPro\\Lightbox\\Trigger');
            $manager->add(NGG_PRO_LIGHTBOX_COMMENT_TRIGGER, 'Imagely\\NGGPro\\Lightbox\\Trigger');
            InstallManager::add_handler(NGG_PRO_LIGHTBOX, 'Imagely\\NGGPro\\Lightbox\\Installer');
            $router = Router::get_instance();
            $app = $router->create_app('/nextgen-share');
            $app->rewrite('/{displayed_gallery_id}/{image_id}', '/displayed_gallery_id--{displayed_gallery_id}/image_id--{image_id}/named_size--thumb', false, true);
            $app->rewrite('/{displayed_gallery_id}/{image_id}/{named_size}', '/displayed_gallery_id--{displayed_gallery_id}/image_id--{image_id}/named_size--{named_size}');
            $app->route(['/'], ['controller' => '\\Imagely\\NGGPro\\Lightbox\\OpenGraphController', 'action' => 'index_action']);
        }, 2);
    }
    public static function get_effect_code($retval, $displayed_gallery)
    {
        // Swap the gallery placeholder
        $retval = str_replace('%PRO_LIGHTBOX_GALLERY_ID%', $displayed_gallery->id(), $retval);
        $lightbox = LightboxManager::get_instance()->get(NGG_PRO_LIGHTBOX);
        if ($lightbox && $lightbox->values['nplModalSettings']['enable_comments'] && $lightbox->values['nplModalSettings']['display_comments']) {
            $retval .= ' data-nplmodal-show-comments="1"';
        }
        return $retval;
    }
    public static function enqueue_frontend_resources($displayed_gallery)
    {
        if (!in_array($displayed_gallery->id(), self::$galleries_displayed) && Settings::get_instance()->get('thumbEffect') === NGG_PRO_LIGHTBOX) {
            // prevent calling get_included_entities() more than once
            self::$galleries_displayed[] = $displayed_gallery->id();
            // The comments sidebar content may depend on the request being from an authenticated user.
            \wp_localize_script('ngg_common', 'nextgen_pro_lightbox_authentication', ['nonce' => \wp_create_nonce('wp_rest')]);
            foreach (self::get_components() as $name => $handler) {
                $handler = new $handler();
                $handler->name = $name;
                $handler->displayed_gallery = $displayed_gallery;
                $handler->enqueue_static_resources();
            }
            $controller = new Galleria();
            $controller->enqueue_frontend_resources($displayed_gallery);
        }
    }
    public function register_lightbox()
    {
        $router = Router::get_instance();
        $settings = Settings::get_instance()->get('ngg_pro_lightbox', []);
        $lightboxes = LightboxManager::get_instance();
        // Define the Pro Lightbox
        $lightbox = new Lightbox(NGG_PRO_LIGHTBOX);
        $lightbox->albums_supported = true;
        $lightbox->title = __('NextGEN Pro Lightbox', 'nextgen-gallery-pro');
        $lightbox->code = "class='nextgen_pro_lightbox' data-nplmodal-gallery-id='%PRO_LIGHTBOX_GALLERY_ID%'";
        $lightbox->styles = [[StaticAssets::get_url('Lightbox/style.css', 'photocrati-nextgen_pro_lightbox#style.css'), 'photocrati-nextgen_pro_lightbox#style.css'], [StaticAssets::get_url('Lightbox/theme/galleria.nextgen_pro_lightbox.css', 'photocrati-nextgen_pro_lightbox#theme/galleria.nextgen_pro_lightbox.css'), 'photocrati-nextgen_pro_lightbox#theme/galleria.nextgen_pro_lightbox.css']];
        $lightbox->scripts = ['wordpress#underscore', [StaticAssets::get_url('Lightbox/parsesrcset.js', 'photocrati-nextgen_pro_lightbox#parsesrcset.js'), 'photocrati-nextgen_pro_lightbox#parsesrcset.js'], 'wordpress#ngg_galleria', [StaticAssets::get_url('Lightbox/nextgen_pro_lightbox.js', 'photocrati-nextgen_pro_lightbox#nextgen_pro_lightbox.js'), 'photocrati-nextgen_pro_lightbox#nextgen_pro_lightbox.js'], [StaticAssets::get_url('Lightbox/theme/galleria.nextgen_pro_lightbox.js', 'photocrati-nextgen_pro_lightbox#theme/galleria.nextgen_pro_lightbox.js'), 'photocrati-nextgen_pro_lightbox#theme/galleria.nextgen_pro_lightbox.js']];
        // Set lightbox display properties
        $settings['is_front_page'] = is_front_page() ? 1 : 0;
        $settings['share_url'] = $router->get_url('/nextgen-share/{gallery_id}/{image_id}/{named_size}', true, 'root');
        $settings['wp_site_url'] = $router->get_base_url('site');
        $settings['protect_images'] = (bool) Settings::get_instance()->get('protect_images');
        $settings['style'] = str_replace('.css', '', $settings['style']);
        // Provide the current language so XHR requests can request translations in the same locale
        if (defined('ICL_LANGUAGE_CODE')) {
            $settings['lang'] = $router->get_parameter('lang', null, false) ? $router->get_parameter('lang') : ICL_LANGUAGE_CODE;
        }
        $settings['i18n'] = ['toggle_social_sidebar' => __('Toggle social sidebar', 'nextgen-gallery-pro'), 'play_pause' => __('Play / Pause', 'nextgen-gallery-pro'), 'toggle_fullsize' => __('Toggle fullsize', 'nextgen-gallery-pro'), 'toggle_image_info' => __('Toggle image info', 'nextgen-gallery-pro'), 'close_window' => __('Close window', 'nextgen-gallery-pro'), 'share' => ['twitter' => __('Share on Twitter', 'nextgen-gallery-pro'), 'facebook' => __('Share on Facebook', 'nextgen-gallery-pro'), 'pinterest' => __('Share on Pinterest', 'nextgen-gallery-pro')]];
        $lightbox->values = ['nplModalSettings' => $settings];
        $lightboxes->register(NGG_PRO_LIGHTBOX, $lightbox);
    }
    public function maybe_enqueue_fontawesome()
    {
        $settings = Settings::get_instance();
        $context = $settings->get('thumbEffectContext', '');
        if ($context !== 'nextgen_images') {
            DisplayManager::enqueue_fontawesome();
        }
    }
    public static function get_components()
    {
        return self::$components;
    }
    public static function add_component($name, $handler)
    {
        self::$components[$name] = $handler;
    }
    public static function remove_component($name, $handler)
    {
        unset(self::$components[$name]);
    }
}