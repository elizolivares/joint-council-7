<?php

/**
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\Lightbox;

use Imagely\NGG\Display\DisplayManager;
use Imagely\NGG\DisplayType\Controller as DisplayTypeController;
use Imagely\NGGPro\Display\StaticAssets;
use Imagely\NGG\DataTypes\DisplayedGallery;
use Imagely\NGG\Display\LightboxManager;
use Imagely\NGG\Settings\Settings;
class Trigger extends TriggerBase
{
    static $_pro_lightbox_enabled = null;
    public $run_once = false;
    public $name;
    function get_css_class()
    {
        $classes = 'fa ngg-trigger nextgen_pro_lightbox';
        if ($this->name == NGG_PRO_LIGHTBOX_TRIGGER) {
            return $classes . ' fa-share-square';
        } else {
            return $classes . ' fa-comment';
        }
    }
    /**
     * @param string           $name
     * @param DisplayedGallery $displayed_gallery
     * @return bool
     */
    static function is_renderable($name, $displayed_gallery)
    {
        $retval = false;
        // Both of these triggers require the Pro Lightbox to be configured as the lightbox effect
        if (self::are_triggers_enabled($displayed_gallery) && self::is_pro_lightbox_enabled() && self::does_source_return_images($displayed_gallery)) {
            // If comments are enabled, display the trigger button to open the comments sidebar
            if ($name == NGG_PRO_LIGHTBOX_COMMENT_TRIGGER) {
                $library = self::get_pro_lightbox();
                if (isset($library->values['nplModalSettings']['enable_comments']) && $library->values['nplModalSettings']['enable_comments']) {
                    $retval = true;
                }
            } else {
                $retval = true;
            }
        }
        return $retval;
    }
    /**
     * @param DisplayedGallery $displayed_gallery
     * @return bool
     */
    static function does_source_return_images($displayed_gallery)
    {
        $retval = false;
        if (($source = $displayed_gallery->get_source()) && in_array('image', $source->returns)) {
            $retval = true;
        }
        return $retval;
    }
    static function is_pro_lightbox_enabled()
    {
        if (is_null(self::$_pro_lightbox_enabled)) {
            $settings = Settings::get_instance();
            if ($settings->get('thumbEffect') == NGG_PRO_LIGHTBOX) {
                self::$_pro_lightbox_enabled = true;
            } else {
                self::$_pro_lightbox_enabled = false;
            }
        }
        return self::$_pro_lightbox_enabled;
    }
    /**
     * @param DisplayedGallery $displayed_gallery
     * @return bool
     */
    static function are_triggers_enabled($displayed_gallery)
    {
        return isset($displayed_gallery->display_settings['ngg_triggers_display']) && $displayed_gallery->display_settings['ngg_triggers_display'] != 'never';
    }
    static function get_pro_lightbox()
    {
        return LightboxManager::get_instance()->get(NGG_PRO_LIGHTBOX);
    }
    function get_attributes()
    {
        $retval = ['class' => $this->get_css_class(), 'data-nplmodal-gallery-id' => $this->displayed_gallery->id()];
        // If we're adding the trigger to an image, then we need
        // to add an attribute for the Pro Lightbox to know which image to display
        if ($this->view->get_id() == 'nextgen_gallery.image') {
            $image = $this->view->get_object();
            $retval['data-image-id'] = $image->{$image->id_field};
        }
        // If we're adding the commenting trigger, then we need to tell the Pro Lightbox to open the sidebar when clicked
        if ($this->name == NGG_PRO_LIGHTBOX_COMMENT_TRIGGER) {
            $retval['data-nplmodal-show-comments'] = 1;
        }
        return $retval;
    }
    public function enqueue_resources($displayed_gallery)
    {
        if (!wp_style_is('fontawesome', 'registered')) {
            DisplayManager::enqueue_fontawesome();
        }
        if (!wp_style_is('ngg-trigger-buttons', 'registered')) {
            \wp_register_style('ngg-trigger-buttons', StaticAssets::get_url('Lightbox/TriggerButtons.css', 'photocrati-nextgen_pro_lightbox#trigger_buttons.css'), [], NGG_PRO_LIGHTBOX_VERSION);
            \wp_enqueue_style('ngg-trigger-buttons');
        }
    }
}