<?php

/**
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\Lightbox;

use Imagely\NGG\DataMappers\Gallery as GalleryMapper;
use Imagely\NGG\DataMappers\Image as ImageMapper;
use Imagely\NGGPro\Display\StaticAssets;
use Imagely\NGG\Display\LightboxManager;
class CommentsManager
{
    protected $interrupt_comment_post_redirection = false;
    // Not all parts of WP should see our comments
    public static $filter_comments = true;
    public static function register_hooks()
    {
        $self = new self();
        add_action('admin_enqueue_scripts', [$self, 'admin_enqueue_scripts']);
        add_action('init', [$self, 'register_post_type']);
        add_action('pre_comment_on_post', [$self, 'pre_comment_on_post']);
        add_filter('comment_class', [$self, 'comment_class'], 10, 5);
        add_filter('comment_post_redirect', [$self, 'comment_post_redirect'], 10, 2);
        add_filter('comment_row_actions', [$self, 'comment_row_actions'], 10, 2);
        add_filter('get_edit_post_link', [$self, 'set_custom_post_link'], 10, 2);
        add_filter('notify_post_author', [$self, 'disable_comment_notifications'], 10, 2);
        add_filter('post_type_link', [$self, 'set_custom_post_link'], 10, 2);
        add_filter('the_title', [$self, 'the_title'], 10, 2);
        if (!is_admin()) {
            add_filter('the_comments', [$self, 'the_comments']);
        }
    }
    /**
     * Because we create a fake post to attach comments to the "notify post author of new comments" feature built into
     * WP can trigger and send new post notifications to the user that first viewed/commented on an image, if they were
     * authenticated with WordPress. This disables author notification for NextGEN comments entirely.
     *
     * @param bool $notify
     * @param int  $comment_id
     * @return bool
     */
    public function disable_comment_notifications($notify, $comment_id)
    {
        $comment = \get_comment($comment_id);
        $post = \get_post($comment->comment_post_ID);
        if ('photocrati-comments' === $post->post_type) {
            return false;
        }
        return $notify;
    }
    function comment_row_actions($actions, $comment)
    {
        if (\get_comment_meta($comment->comment_ID, 'generated_by', true) !== 'photocrati-comments') {
            return $actions;
        }
        $lightbox = LightboxManager::get_instance()->get_selected();
        if ($lightbox->name !== NGG_PRO_LIGHTBOX) {
            return $actions;
        }
        $href = \get_comment_meta($comment->comment_ID, 'ngg_origin_url', true);
        if (empty($href)) {
            return $actions;
        }
        $actions['nextgen_source'] = sprintf(__("<a href='%s' target='_blank'>View in lightbox</a>", 'nextgen-gallery-pro'), $href);
        return $actions;
    }
    function admin_enqueue_scripts($hook)
    {
        if ($hook != 'edit-comments.php') {
            return;
        }
        \wp_enqueue_style('nextgen-pro-comments', StaticAssets::get_url('Comments/admin.css', 'photocrati-comments#admin.css'));
    }
    function comment_class($classes, $class, $comment_id, $comment, $post_id)
    {
        if (\get_comment_meta($comment->comment_ID, 'generated_by', true) != 'photocrati-comments') {
            return $classes;
        }
        $classes[] = 'nextgen-comment';
        return $classes;
    }
    function the_title($title, $id = null)
    {
        $post = get_post();
        if (!$post || !isset($post->post_type) || $post->post_type != 'photocrati-comments') {
            return $title;
        }
        $image_id = intval(str_replace('nextgen-comment-link-image-', '', $post->post_name));
        $image = ImageMapper::get_instance()->find($image_id);
        $gallery = GalleryMapper::get_instance()->find($image->galleryid);
        return sprintf(__("Image in gallery '%s'", 'nextgen-gallery-pro'), $gallery->title);
    }
    /**
     * @param string     $post_link
     * @param int|object $post
     * @return string
     */
    function set_custom_post_link($post_link, $post)
    {
        if (is_int($post)) {
            $post = get_post($post);
        }
        if ($post->post_type != 'photocrati-comments') {
            return $post_link;
        }
        $image_id = intval(str_replace('nextgen-comment-link-image-', '', $post->post_name));
        if ($image = ImageMapper::get_instance()->find($image_id)) {
            $post_link = \wp_nonce_url('admin.php?page=nggallery-manage-gallery&mode=edit&gid=' . $image->galleryid);
        }
        return $post_link;
    }
    function register_post_type()
    {
        \register_post_type('photocrati-comments', ['label' => __('Comment', 'nextgen-gallery-pro'), 'labels' => ['name' => __('Comment', 'nextgen-gallery-pro'), 'singular_name' => __('Comment', 'nextgen-gallery-pro'), 'view_item' => __('View Gallery', 'nextgen-gallery-pro')], 'public' => false, 'show_in_menu' => false, 'show_in_admin_bar' => false, 'supports' => ['comments' => true, 'title' => false, 'editor' => false], 'rewrite' => false, 'query_var' => false]);
    }
    /**
     * @param int $post_id
     */
    function pre_comment_on_post($post_id)
    {
        $post = \get_post($post_id);
        if ($post->post_type == 'photocrati-comments' && !defined('DOING_AJAX')) {
            // start a new output buffer just in case any plugins or themes throw any warnings, errors, or any other
            // unwanted texts from forcing themselves into our json response
            ob_start();
            $this->interrupt_comment_post_redirection = true;
            define('DOING_AJAX', true);
        }
    }
    /**
     * The last action wp-comments-post.php before redirecting is to call the set_comment_post_redirect filter. To
     * prevent the WP 302 HTTP response we output our own json here and end execution. This should ONLY be done if
     * the pre_comment_on_post action (see above) has determined the comment belongs to one of our wrapper posts!
     *
     * @param string $location
     * @param object $comment
     */
    function comment_post_redirect($location, $comment)
    {
        if ($this->interrupt_comment_post_redirection) {
            // use this to track which comments we created
            \add_comment_meta($comment->comment_ID, 'generated_by', 'photocrati-comments');
            if (isset($_REQUEST['ngg_comment_origin_url']) && !empty($_REQUEST['ngg_comment_origin_url'])) {
                \add_comment_meta($comment->comment_ID, 'ngg_origin_url', $_REQUEST['ngg_comment_origin_url']);
            }
            if (isset($_REQUEST['nextgen_generated_comment']) && $_REQUEST['nextgen_generated_comment'] == 'true') {
                while (ob_get_level() > 0) {
                    ob_end_clean();
                }
                echo json_encode(['success' => true]);
                exit;
            }
        }
        return $location;
    }
    /**
     * Applies a filter to recent comments that prevents comments generated by this module from appearing in
     * the "Recent Comments" widget provided by WordPress
     *
     * @param array $comments
     * @return array $comments
     */
    function the_comments($comments = [])
    {
        // Unfortunately WordPress' has poor ability to filter comments based on their metadata when calling
        // get_comments(). Because of this we must filter comments we have generated *after* allowing them to
        // be retrieved.
        if (self::$filter_comments) {
            foreach ($comments as $ndx => $comment) {
                $meta = get_comment_meta($comment->comment_ID);
                if (!empty($meta['generated_by']) && $meta['generated_by'][0] == 'photocrati-comments') {
                    unset($comments[$ndx]);
                }
            }
        }
        return $comments;
    }
}