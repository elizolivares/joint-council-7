<?php

/**
 * @remove-for-nextgen-starter
 */
namespace Imagely\NGGPro\Lightbox;

abstract class TriggerBase
{
    public $displayed_gallery;
    public $name;
    public $view;
    static function is_renderable($name, $displayed_gallery)
    {
        return true;
    }
    function get_css_class()
    {
        return 'far fa-circle';
    }
    function get_attributes()
    {
        return ['class' => $this->get_css_class()];
    }
    function render()
    {
        $attributes = [];
        foreach ($this->get_attributes() as $k => $v) {
            $k = esc_attr($k);
            $v = esc_attr($v);
            $attributes[] = "{$k}='{$v}'";
        }
        $attributes = implode(' ', $attributes);
        return "<i {$attributes}></i>";
    }
}