jQuery(document).foundation();
/*
These functions make sure WordPress
and Foundation play nice together.
*/

jQuery(document).ready(function() {

    // Remove empty P tags created by WP inside of Accordion and Orbit
    jQuery('.accordion p:empty, .orbit p:empty').remove();

	 // Makes sure last grid item floats left
	jQuery('.archive-grid .columns').last().addClass( 'end' );

	// Adds Flex Video to YouTube and Vimeo Embeds
	jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
	    if ( jQuery(this).innerWidth() / jQuery(this).innerHeight() > 1.5 ) {
	      jQuery(this).wrap("<div class='widescreen flex-video'/>");
	    } else {
	      jQuery(this).wrap("<div class='flex-video'/>");
	    }
        
  });
  // Height equalization of containers in the footer and elsewhere. No way to do this with just css.
    jQuery('.query-news-wrapper .query-row').matchHeight();
    jQuery('.query-locals-wrapper .query-row').matchHeight();
    jQuery('.match-row .news-item').matchHeight();
    jQuery('.match').matchHeight();
    jQuery('.match .text').matchHeight();
    jQuery('.query-home-news-1 .news-item').matchHeight({
        target: jQuery('.column-match')
    });
    jQuery('.query-homenews4 .news-item').matchHeight({
        target: jQuery('.row-match')
    });
    
  // Link some widgets
    jQuery(".front-locals .textwidget").click(function() {
	  window.location = jQuery(this).find("a").attr("href"); 
	  return false;
	});
    jQuery(".front-difference .textwidget").click(function() {
	  window.location = jQuery(this).find("a").attr("href"); 
	  return false;
	});
	jQuery(".teamster-difference").click(function() {
	  window.location = jQuery(this).find("a").attr("href"); 
	  return false;
	});
  // Cheap hack to toggle filters on certain landing pages
    	//Resource Directory:
    jQuery('.local-sidebar-toggle').click(function() { 
            jQuery('.query-locals-sidebar-wrapper .query-content').slideToggle("300");
            jQuery('.local-sidebar-toggle').toggleClass("shrink");
    });
  

});
jQuery(window).load(function() {
    /*
	 * Replace all SVG images with inline SVG
	 */
	jQuery('img.svg').each(function(){
	    var $img = jQuery(this);
	    var imgID = $img.attr('id');
	    var imgClass = $img.attr('class');
	    var imgURL = $img.attr('src');
	
	    jQuery.get(imgURL, function(data) {
	        // Get the SVG tag, ignore the rest
	        var $svg = jQuery(data).find('svg');
	
	        // Add replaced image's ID to the new SVG
	        if(typeof imgID !== 'undefined') {
	            $svg = $svg.attr('id', imgID);
	        }
	        // Add replaced image's classes to the new SVG
	        if(typeof imgClass !== 'undefined') {
	            $svg = $svg.attr('class', imgClass+' replaced-svg');
	        }
	
	        // Remove any invalid XML tags as per http://validator.w3.org
	        $svg = $svg.removeAttr('xmlns:a');
	
	        // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
	        if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
	            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
	        }
	
	        // Replace image with new SVG
	        $img.replaceWith($svg);
	
	    }, 'xml');
	
	}); 
});
