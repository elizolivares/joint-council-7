<?php
// Theme support options
require_once(get_template_directory().'/assets/functions/theme-support.php'); 

// WP Head and other cleanup functions
require_once(get_template_directory().'/assets/functions/cleanup.php'); 

// Register scripts and stylesheets
require_once(get_template_directory().'/assets/functions/enqueue-scripts.php'); 

// Register custom menus and menu walkers
require_once(get_template_directory().'/assets/functions/menu.php'); 

// Register sidebars/widget areas
require_once(get_template_directory().'/assets/functions/sidebar.php'); 

// Makes WordPress comments suck less
require_once(get_template_directory().'/assets/functions/comments.php'); 

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/assets/functions/page-navi.php'); 

// Adds support for multiple languages
require_once(get_template_directory().'/assets/translation/translation.php'); 


// Remove 4.2 Emoji Support
// require_once(get_template_directory().'/assets/functions/disable-emoji.php'); 

// Adds site styles to the WordPress editor
//require_once(get_template_directory().'/assets/functions/editor-styles.php'); 

// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/assets/functions/related-posts.php'); 

// Use this as a template for custom post types
// require_once(get_template_directory().'/assets/functions/custom-post-type.php');

// Customize the WordPress login menu
// require_once(get_template_directory().'/assets/functions/login.php'); 

// Customize the WordPress admin
// require_once(get_template_directory().'/assets/functions/admin.php'); 


/**
 * Enqueue Fonts
 */
 
// Google Fonts
function jc7_add_google_fonts() {
	wp_enqueue_style( 'jc7-google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700|Titillium+Web:300,400,600,700', false ); 
}
add_action( 'wp_enqueue_scripts', 'jc7_add_google_fonts' );

// Font Awesome
function enqueue_load_fa() {
wp_enqueue_style( 'load-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_load_fa' );

/**
 * Remove 'Category:' prefix from category page titles
 */
function prefix_category_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    }
    return $title;
}
add_filter( 'get_the_archive_title', 'prefix_category_title' );


// Adds a thumbnail size for portraits that has a specific height and width
add_image_size( 'portrait-thumbnail', 625, 875, array( 'center', 'top' ) );
add_image_size( 'square-thumbnail', 600, 600, array( 'center', 'top' ) );
add_image_size( 'squaresm-thumbnail', 400, 400, array( 'center', 'top' ) );
add_image_size( 'news-thumbnail', 800, 600, array( 'center', 'center' ) );
add_image_size( 'wide-thumbnail', 800, 300, array( 'center', 'center' ) );
add_image_size( 'news-one-thumbnail', 700, 445, array( 'center', 'center' ) );
add_image_size( 'news-three-thumbnail', 500, 300, array( 'center', 'center' ) );

add_filter( 'image_size_names_choose', 'jc7_custom_sizes' );
function jc7_custom_sizes( $sizes ) {
	return array_merge( $sizes, array(
		'portrait-thumbnail' => __( 'Portrait Thumbnail' ),
		'square-thumbnail' => __( 'Square Thumbnail' ),
		'news-thumbnail' => __( 'News Thumbnail' ),
		'wide-thumbnail' => __( 'News Thumbnail 2' ),
		'news-one-thumbnail' => __( 'News Thumbnail 1' ),
		'news-three-thumbnail' => __( 'News Thumbnail 3' ),
	) );
}

// Custom category templatws
function get_custom_cat_template($single_template) {
   global $post;
   if ( in_category( 'publications' )) {
      $single_template = dirname( __FILE__ ) . '/single-publications.php';
   }
   elseif ( in_category( 'political-director-report' )) {
      $single_template = dirname( __FILE__ ) . '/single-political-director-report.php';
   }
   elseif ( in_category( 'member-profiles' )) {
      $single_template = dirname( __FILE__ ) . '/single-member-profiles.php';
   }
   elseif ( in_category( 'presidents-report' )) {
      $single_template = dirname( __FILE__ ) . '/single-presidents-report.php';
   }
   return $single_template;
} 
add_filter( "single_template", "get_custom_cat_template" ) ;

// Customize excerpts
function wpse_allowedtags() {
    // Add custom tags to this string
        return '<br>,<em>,<i>,<ul>,<ol>,<li>,<p>'; 
    }

if ( ! function_exists( 'wpse_custom_wp_trim_excerpt' ) ) : 

    function wpse_custom_wp_trim_excerpt($wpse_excerpt) {
    $raw_excerpt = $wpse_excerpt;
        if ( '' == $wpse_excerpt ) {

            $wpse_excerpt = get_the_content('');
            $wpse_excerpt = strip_shortcodes( $wpse_excerpt );
            $wpse_excerpt = apply_filters('the_content', $wpse_excerpt);
            $wpse_excerpt = str_replace(']]>', ']]&gt;', $wpse_excerpt);
            $wpse_excerpt = strip_tags($wpse_excerpt, wpse_allowedtags()); /*IF you need to allow just certain tags. Delete if all tags are allowed */

            //Set the excerpt word count and only break after sentence is complete.
                $excerpt_word_count = 75;
                $excerpt_length = apply_filters('excerpt_length', $excerpt_word_count); 
                $tokens = array();
                $excerptOutput = '';
                $count = 0;

                // Divide the string into tokens; HTML tags, or words, followed by any whitespace
                preg_match_all('/(<[^>]+>|[^<>\s]+)\s*/u', $wpse_excerpt, $tokens);

                foreach ($tokens[0] as $token) { 

                    if ($count >= $excerpt_length && preg_match('/[\,\;\?\.\!]\s*$/uS', $token)) { 
                    // Limit reached, continue until , ; ? . or ! occur at the end
                        $excerptOutput .= trim($token);
                        break;
                    }

                    // Add words to complete sentence
                    $count++;

                    // Append what's left of the token
                    $excerptOutput .= $token;
                }

            $wpse_excerpt = trim(force_balance_tags($excerptOutput));

                $excerpt_end = ' <a href="'. esc_url( get_permalink() ) . '">' . '&nbsp;&raquo;&nbsp;' . sprintf(__( 'Read more about: %s &nbsp;&raquo;', 'wpse' ), get_the_title()) . '</a>'; 
                $excerpt_more = apply_filters('excerpt_more', ' ' . $excerpt_end); 

                //$pos = strrpos($wpse_excerpt, '</');
                //if ($pos !== false)
                // Inside last HTML tag
                //$wpse_excerpt = substr_replace($wpse_excerpt, $excerpt_end, $pos, 0); /* Add read more next to last word */
                //else
                // After the content
                $wpse_excerpt .= $excerpt_more; /*Add read more in new paragraph */

            return $wpse_excerpt;   

        }
        return apply_filters('wpse_custom_wp_trim_excerpt', $wpse_excerpt, $raw_excerpt);
    }

endif; 

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'wpse_custom_wp_trim_excerpt'); 

// Get Post Thumbnail URL (1)
function jc7_post_thumbnail_url_1( $post = null, $size ) {
    $post_thumbnail_id = get_post_thumbnail_id( $post );
    if ( ! $post_thumbnail_id ) {
        return false;
    }
    return wp_get_attachment_image_url( $post_thumbnail_id, $size = 'news-one-thumbnail' );
}

// Get Post Thumbnail URL (Wide, 2)
function jc7_wide_post_thumbnail_url( $post = null, $size ) {
    $post_thumbnail_id = get_post_thumbnail_id( $post );
    if ( ! $post_thumbnail_id ) {
        return false;
    }
    return wp_get_attachment_image_url( $post_thumbnail_id, $size = 'wide-thumbnail' );
}

// Get Post Thumbnail URL (3)
function jc7_post_thumbnail_url_3( $post = null, $size ) {
    $post_thumbnail_id = get_post_thumbnail_id( $post );
    if ( ! $post_thumbnail_id ) {
        return false;
    }
    return wp_get_attachment_image_url( $post_thumbnail_id, $size = 'news-three-thumbnail' );
}

// Get Post Thumbnail URL (4)
function jc7_post_thumbnail_url_4( $post = null, $size ) {
    $post_thumbnail_id = get_post_thumbnail_id( $post );
    if ( ! $post_thumbnail_id ) {
        return false;
    }
    return wp_get_attachment_image_url( $post_thumbnail_id, $size = 'portrait-thumbnail' );
}

// Get Post Categories
function jc7_post_categories() {
    $categories = the_category(', '); 
    if (has_category('',$post->ID)) {
    	return $categories;
    }
    else echo "";
}

// Get Post Categories
function nolink_categories() {
  if (has_category('',$post->ID)) {
    $cat_names = wp_list_pluck( get_the_category(), 'cat_name');
    echo join( ', ', $cat_names );
  }
    else echo "";
}

// Get Certain post categories
function limit_post_categories() {
    $categories = the_category(', '); 
    if (has_category('',$post->ID)) {
    	return $categories;
    }
    else echo "";
}

// Get current post ID in order to print "Recents" correctly
function member_get_id() {
	global $post;
	$memid = array ($post->ID);
	return $memid;
}

// Use post slug to find category slug
function local_category_slug($args, $filter) {
	global $post;
	$page_slug = $post->post_name;
    $args = array(
	    'category_name' => $page_slug
	);
	return $args;
}



