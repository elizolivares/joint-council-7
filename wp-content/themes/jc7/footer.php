<?php
/**
 * The template for displaying the footer. 
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */			
 ?>
					
				<footer class="footer" role="contentinfo">
					
					<div id="inner-footer" class="row">
						
						<div class="small-12 medium-12 large-4 columns">
	    					<div class="small-12 medium-6 large-12 float-left">
	    						<?php dynamic_sidebar( 'footerleft' ); ?>
	    					</div>
	    				</div>
						<div class="small-12 medium-12 large-8 columns">
								<?php dynamic_sidebar( 'footerright' ); ?>
						</div>
					
					</div> <!-- end #inner-footer -->
					<div class="bottom-bar small-12"></div>
				
				</footer> <!-- end .footer -->
			
			</div>  <!-- end .off-canvas-content -->
					
		</div> <!-- end .off-canvas-wrapper -->
		
		<?php wp_footer(); ?>
		
	</body>
	
</html> <!-- end page -->