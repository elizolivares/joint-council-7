<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						
					
    <section class="entry-content" itemprop="articleBody">
		<div class="content-wrapper">
			<div class="float-left small-12 large-7 columns">
				<header class="article-header">
					<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
			    </header> <!-- end article header -->
				<?php the_content(); ?>
				<?php if ( get_post_meta( $post->ID, 'membership_meetings', true ) ) { ?>
						<div class="data meetings"><h3><?php the_title(); ?> Membership Meetings</h3><?php the_field('membership_meetings'); ?></div>
					<?php } else { 
			
							echo '<div class="empty"></div>';
			    		}
					?>
				<div class="local-news">
					<h3>Recent <?php the_title(); ?> News</h3>
					<div class="small-12">
						<?php echo do_shortcode('[query slug="local-news"]'); ?>
					</div>
				</div>
			</div>
			<div class="float-right small-12 large-5 columns local-info">
				<div class="local-image">
					<?php the_post_thumbnail('large'); ?>
				</div>
				<div class="local-sidebar">
					<?php if ( get_post_meta( $post->ID, 'leadership', true ) ) { ?>
						<div class="data leadership"><?php the_field('leadership'); ?></div>
					<?php } else { 
			
							echo '<div class="empty"></div>';
			    		}
					?>
					<?php if ( get_post_meta( $post->ID, 'address_and_phone', true ) ) { ?>
						<div class="data address"><?php the_field('address_and_phone'); ?> </div>
					<?php } else { 
			
							echo '<div class="empty"></div>';
			    		}
					?>
					<?php if ( get_post_meta( $post->ID, 'website', true ) ) { ?>
						<div class="data website"><a href="<?php the_field('website'); ?>">Visit Website</a></div>
					<?php } else { 
			
							echo '<div class="empty"></div>';
			    		}
					?>
				</div>
			</div>
		</div>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jointswp' ), 'after'  => '</div>' ) ); ?>
		<p class="tags"><?php the_tags('<span class="tags-title">' . __( 'Tags:', 'jointswp' ) . '</span> ', ', ', ''); ?></p>	
	</footer> <!-- end article footer -->
						
	<?php comments_template(); ?>	
													
</article> <!-- end article -->