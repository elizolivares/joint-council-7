<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>
<div class="social-bar small-12">
    	<?php if ( is_active_sidebar( 'minibar' ) ) : ?>
		    <?php dynamic_sidebar( 'minibar' ); ?>
		<?php endif; ?>
</div>
<div class="top-bar" id="top-bar-menu">
	<div class="small-12 row">
		<div class="small-2 medium-2 float-left">
			<ul class="menu logo">
				<li><a href="<?php echo home_url(); ?>"><img src="<?php echo home_url(); ?>/wp-content/themes/jc7/images/JC7-logo.svg"></a></li>
			</ul>
		</div>
		<div class="small-10 medium-10 float-left columns">		
			<h1 class="site-title"><a href="<?php echo home_url(); ?>">Teamsters</a></h1>
			<h3 class="site-subtitle"><a href="<?php echo home_url(); ?>">Joint Council 7</a></h3>
		</div>
	</div>
	<div class="small-12 navigation">
		<div class="small-12 row">
			<div class="small-2 medium-2 float-left trans">.</div>
			<div class="top-nav show-for-large float-left small-8">
				<?php joints_top_nav(); ?>
			</div>
			<div class="top-search float-left small-6 medium-5 large-2 columns">
				<?php get_search_form(); ?>
			</div>
			<div class="top-bar-right float-right small-2 hide-for-large">
				<ul class="menu float-right">
					<!-- <li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li> -->
					<li><a data-toggle="off-canvas" class="hamburger"><?php _e( 'Menu', 'jointswp' ); ?></a></li>
				</ul>
			</div>
		</div>
	</div>
	
</div>