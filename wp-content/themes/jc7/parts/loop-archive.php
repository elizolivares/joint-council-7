<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">					
	<header class="article-header">
		<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
		<div class="post_date"><?php the_date(); ?></div>
		<div class="categories"><?php echo limit_post_categories() ?></div>
		<?php if ( has_post_thumbnail() ) { ?>
		<div class="featured">
			<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('large'); ?></a>
		</div>
		<?php } ?>
		<?php //get_template_part( 'parts/content', 'byline' ); ?>
	</header> <!-- end article header -->
					
	<section class="entry-content" itemprop="articleBody">
		<?php the_excerpt(); ?>
		<a class="button sparse" href="<?php the_permalink(); ?>">Keep reading<i class="fas fa-chevron-circle-right"></i></a>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
    	<p class="tags"><?php the_tags('<span class="tags-title">' . __('Tags:', 'jointstheme') . '</span> ', ', ', ''); ?></p>
	</footer> <!-- end article footer -->				    						
</article> <!-- end article -->