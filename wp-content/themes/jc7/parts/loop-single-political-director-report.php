<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
								
    <section class="entry-content float-left small-12 large-8" itemprop="articleBody">
	    <header class="article-header">
			<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
			<?php get_template_part( 'parts/content', 'byline' ); ?>
			<span class="entry-date"><?php echo get_the_date(); ?></span>
			<div class="share"><span class="share-text">Share: </span><?php echo do_shortcode('[feather_share]'); ?></div>
	    </header> <!-- end article header -->
		<?php the_content(); ?>
	</section> <!-- end article section -->
	<div id="politicaldirector" class="small-8 large-4 float-right political-director-sidebar columns">
		<?php if ( is_active_sidebar( 'politicaldirector' ) ) : ?>
		    <?php dynamic_sidebar( 'politicaldirector' ); ?>
		<?php endif; ?>
	</div>			
	<footer class="article-footer">
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jointswp' ), 'after'  => '</div>' ) ); ?>
		<p class="tags"><?php the_tags('<span class="tags-title">' . __( 'Tags:', 'jointswp' ) . '</span> ', ', ', ''); ?></p>	
	</footer> <!-- end article footer -->
						
	<?php comments_template(); ?>	
													
</article> <!-- end article -->